#ifndef MSABBINARYHANDLER_H
#define MSABBINARYHANDLER_H

#include <QString>
#include <Utility/BinaryHandler.h>
#include <QList>

namespace MSAB {

	static class BinaryHandler
	{
	public:	
		static QList<double> StringToDoubleArray(QString inString, int expectedValues, bool doublePrecision = true, bool reverse = false);
		
	private:
		static QVector<unsigned char> StringToByteArray(QString inString);
		static QByteArray UncompressByteArray(QByteArray inArray, int expectedLength, bool doublePrecision = true);
		static bool is_base64(char c);

	};
}

#endif //MSABBINARYHANDLER_H