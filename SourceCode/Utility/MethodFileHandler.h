#ifndef MSABMETHODFILEHANDLER_H
#define MSABMETHODFILEHANDLER_H

#include <QMainWindow>
#include <QLabel>
#include <QProgressBar>
#include <QString>
#include <Utility/DataContainer/MethodTableModel.h>
#include <Utility/DataContainer/Chemestry.h>
#include <QList>

namespace MSAB {

	static class MethodFileHandler
	{

	public:
		static QList<MassSpecMethodModel> GetModelsFromFile(QString filePath);
	

	};
}

#endif //MSABMETHODFILEHANDLER_H