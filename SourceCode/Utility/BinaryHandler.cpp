#include <Utility/BinaryHandler.h>
#include <QtZlib/zlib.h>

using namespace std;

namespace MSAB
{
	QList<double> BinaryHandler::StringToDoubleArray(QString inString, int expectedValues, bool doublePrecision, bool reverse)
	{
		auto test = inString.length();
		//auto ret = StringToByteArray(inString);
		auto data = QByteArray::fromBase64(inString.toUtf8());
		const std::size_t count = data.size();
		auto doublePointer = reinterpret_cast<const double*>(data.data());
		
		if (data.size() != expectedValues * 8 && data.size() != expectedValues * 4)
			data = UncompressByteArray(data, expectedValues, doublePrecision);

		std::vector<unsigned char> ret(data.begin(), data.end());
		QList<double> doubleList;
		if (data.size() == expectedValues * 4)
		{
			for (int x = 0; x < ret.size(); x += 4)
			{
				union {
					float f;
					char bytes[sizeof(float)];
				} u;
				if (reverse)
				{
					u.bytes[0] = ret[x + 3];
					u.bytes[1] = ret[x + 2];
					u.bytes[2] = ret[x + 1];
					u.bytes[3] = ret[x];
				}
				else
				{
					u.bytes[0] = ret[x];
					u.bytes[1] = ret[x + 1];
					u.bytes[2] = ret[x + 2];
					u.bytes[3] = ret[x + 3];
				}
				
				doubleList.push_back(u.f);
			}
		}
		else if (data.size() == expectedValues * 8)
		{
			for (int x = 0; x < ret.size(); x += 8)
			{
				union {
					double d;
					char bytes[sizeof(double)];
				} u;
				if (reverse)
				{
					u.bytes[0] = ret[x + 7];
					u.bytes[1] = ret[x + 6];
					u.bytes[2] = ret[x + 5];
					u.bytes[3] = ret[x + 4];
					u.bytes[4] = ret[x + 3];
					u.bytes[5] = ret[x + 2];
					u.bytes[6] = ret[x + 1];
					u.bytes[7] = ret[x];
				}
				else
				{
					u.bytes[0] = ret[x];
					u.bytes[1] = ret[x + 1];
					u.bytes[2] = ret[x + 2];
					u.bytes[3] = ret[x + 3];
					u.bytes[4] = ret[x + 4];
					u.bytes[5] = ret[x + 5];
					u.bytes[6] = ret[x + 6];
					u.bytes[7] = ret[x + 7];
				}
				doubleList.push_back(u.d);
			}
		}
		else
			return QList<double>();
		
		return doubleList;
	}
	QVector<unsigned char> BinaryHandler::StringToByteArray(QString inString)
	{
		//const std::string base64_chars =
		//	"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		//	"abcdefghijklmnopqrstuvwxyz"
		//	"0123456789+/";

		//std::string binaryData = inString.toStdString();
		//int in_len = binaryData.size();
		//int i = 0;
		//int j = 0;
		//int in_ = 0;
		//char char_array_4[4], char_array_3[3];
		//QList<unsigned char> ret;

		//while (in_len-- && (binaryData[in_] != '=') && is_base64(binaryData[in_])) {
		//	char_array_4[i++] = binaryData[in_]; in_++;
		//	if (i == 4) {
		//		for (i = 0; i < 4; i++)
		//			char_array_4[i] = base64_chars.find(char_array_4[i]);

		//		char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
		//		char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
		//		char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

		//		for (i = 0; (i < 3); i++)
		//			ret.push_back(char_array_3[i]);
		//		i = 0;
		//	}
		//}

		//if (i) {
		//	for (j = i; j < 4; j++)
		//		char_array_4[j] = 0;

		//	for (j = 0; j < 4; j++)
		//		char_array_4[j] = base64_chars.find(char_array_4[j]);

		//	char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
		//	char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
		//	char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

		//	for (j = 0; (j < i - 1); j++) ret.push_back(char_array_3[j]);
		//}

		auto data = QByteArray::fromBase64(inString.toUtf8());
		std::vector<unsigned char> bufferToCompress(data.begin(), data.end());
		return QVector<unsigned char>::fromStdVector(bufferToCompress);
	}
	QByteArray BinaryHandler::UncompressByteArray(QByteArray inArray, int expectedLength, bool doublePrecision)
	{
		auto CompressedDataLen = inArray.size();
		auto RawDataLen = expectedLength * (doublePrecision ? 8 : 4);

		unsigned char *compressBuf = new unsigned char[CompressedDataLen];
		memcpy(compressBuf, inArray, CompressedDataLen);
		unsigned char *uncompressBuf = new unsigned char[RawDataLen];
		unsigned long int compressBufLength = CompressedDataLen;
		unsigned long int uncompressLength = RawDataLen;
		int uncompressValue = uncompress(uncompressBuf, &uncompressLength, compressBuf, compressBufLength);
		if (uncompressValue != Z_OK)
		{
			//OutErrorMessage("Uncompression error code " + QString::number(uncompressValue), false);
		}
		return QByteArray::fromRawData(reinterpret_cast<char*>(uncompressBuf), uncompressLength);
	}
	bool BinaryHandler::is_base64(char c)
	{
		return (isalnum(c) || (c == '+') || (c == '/'));
	}
}