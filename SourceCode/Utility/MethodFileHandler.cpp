#include <Utility/DataContainer/MSABExperiment.h>
#include <Utility/MethodFileHandler.h>
#include <netcdf.h>
#include <vector>
#include <QRegularExpression>

using namespace std;

namespace MSAB
{
	QList<MassSpecMethodModel> MethodFileHandler::GetModelsFromFile(QString filePath)
	{
		QFile inFile(filePath);
		if (!inFile.open(QIODevice::ReadOnly | QIODevice::Text))
			return *(new QList<MassSpecMethodModel>);

		QList<MassSpecMethodModel> returnVector;
		QRegularExpression nameRx("\\.Name\\s*=\\s*'(\\w+)';", QRegularExpression::CaseInsensitiveOption);
		QRegularExpression rtRx("\\.RetTime\\s*=\\s*([\\d\\.]+);", QRegularExpression::CaseInsensitiveOption);
		QRegularExpression relIntOuterRx("\\.MainIonsRelInt\\s*=\\s*\\[(.*)\\];", QRegularExpression::CaseInsensitiveOption);
		QRegularExpression formulaOuterRx("\\.SelectedIonsFormula\\s*=\\s*\{(.*)}", QRegularExpression::CaseInsensitiveOption);
		QRegularExpression labelsOuterRx("\\.SelectedIonsLabeled\\s*=\\s*\{(.*)}", QRegularExpression::CaseInsensitiveOption);
		QRegularExpression innerStringRx("'(\\w+)'", QRegularExpression::CaseInsensitiveOption);
		QRegularExpression innerIntRx("(\\d+)", QRegularExpression::CaseInsensitiveOption);
		QRegularExpression labelMassRx("^\\[([\\d\\.]+)\\]", QRegularExpression::CaseInsensitiveOption);
		QRegularExpression labelElementRx("^([A-Za-z]+)", QRegularExpression::CaseInsensitiveOption);
		QRegularExpression labelHitsRx("^(\\d+)", QRegularExpression::CaseInsensitiveOption);
		QRegularExpression breakRx("^\\s*$", QRegularExpression::CaseInsensitiveOption);

		auto tempModel = new MassSpecMethodModel;
		auto relIntensities = new QList<int>;
		auto formulae = new QList<QString>;
		auto labels = new QList<QList<IonLabel>>;

		QTextStream reader(&inFile);
		while (!reader.atEnd())
		{
			QString line = reader.readLine();
			auto nameMatch = nameRx.match(line);
			auto rtMatch = rtRx.match(line);
			auto relIntMatch = relIntOuterRx.match(line);
			auto formulaOuterMatch = formulaOuterRx.match(line);
			auto labelsOuterMatch = labelsOuterRx.match(line);
			auto breakMatch = breakRx.match(line);

			if (nameMatch.hasMatch())
				tempModel->methodName = nameMatch.captured(1);
			else if (rtMatch.hasMatch())
				tempModel->expectedRT = rtMatch.captured(1).toDouble();
			else if (relIntMatch.hasMatch())
			{
				auto innerInts = innerIntRx.globalMatch(relIntMatch.captured(1));
				while (innerInts.hasNext()) 
				{
					QRegularExpressionMatch match = innerInts.next();
					relIntensities->append(match.captured(1).toInt());
				}

			}
			else if (formulaOuterMatch.hasMatch())
			{
				auto spacelessFormula = formulaOuterMatch.captured(1).replace(" ","");
				auto innerStrings = innerStringRx.globalMatch(spacelessFormula);
				while (innerStrings.hasNext()) 
				{
					QRegularExpressionMatch match = innerStrings.next();
					formulae->append(match.captured(1));
				}
			}
			else if (labelsOuterMatch.hasMatch())
			{
				auto innerStrings = innerStringRx.globalMatch(labelsOuterMatch.captured(1));
				while (innerStrings.hasNext()) 
				{
					auto matchString = innerStrings.next().captured(1);
					QList<IonLabel> innerLabelList;
					while (matchString.length() > 0)
					{
						double newMass = -1;
						QString element;
						int numHits = 1;
						auto newMassMatch = labelMassRx.match(matchString);
						if (newMassMatch.hasMatch())
						{
							newMass = newMassMatch.captured(1).toDouble();
							matchString.replace(labelMassRx, "");
						}
						auto elementMatch = labelElementRx.match(matchString);
						if (elementMatch.hasMatch())
						{
							element = elementMatch.captured(1);
							matchString.replace(labelElementRx, "");
							if (newMass < 0)
							{
								if (element == "C")
									newMass = 13.003355;
								else if (element == "N")
									newMass = 15.000109;
								else if (element == "H")
									newMass = 2.0141025;
							}
						}
						auto hitsMatch = labelHitsRx.match(matchString);
						if (hitsMatch.hasMatch())
						{
							numHits = hitsMatch.captured(1).toInt();
							matchString.replace(labelHitsRx, "");
						}
						if (newMass >= 0 && element.length() > 0 && numHits > 0)
						{
							IonLabel newLabel(element,newMass, numHits);
							innerLabelList.append(newLabel);
						}
						else
							break;
					}
					labels->append(innerLabelList);
				}
			}
			else if (breakMatch.hasMatch() || reader.atEnd())
			{
				if (tempModel->methodName.length() > 0 
					&& relIntensities->count() > 0
					&& relIntensities->count() == formulae->count()
					&& formulae->count() == labels->count())
				{
					for (auto x = 0; x < relIntensities->count(); x++)
					{
						MassSpecMethodIon newIon((*formulae)[x], (*relIntensities)[x], false);
						auto theseLabels = (*labels)[x];
						for (auto y = 0; y < theseLabels.count(); y++)
							newIon.ionLabels.append(theseLabels[y]);
						tempModel->append(newIon);
					}
					returnVector.append(*tempModel);
				}
				delete tempModel;
				delete relIntensities;
				delete formulae;
				delete labels;
				tempModel = new MassSpecMethodModel;
				relIntensities = new QList<int>;
				formulae = new QList<QString>;
				labels = new QList<QList<IonLabel>>;
			}
		}
			   
		
		//MassSpecMethodIon tempIon("C8H7N5O8", 100,true);
		//MassSpecMethodIon tempIon2("C8H8N5O8", 50);

		////MassSpecMethodIon tempIon("C8H13O4",1,100,"H3");
		////MassSpecMethodIon tempIon2("C12H19O6",1,50,"H4");
		////C8H7N5O8

		////tempIon2.formula -= tempIon.formula;
		////OpenMS::String tmpstr = tempIon2.formula.toString();
		////double baseMass = tempIon2.formula.getMonoWeight();
		////tempIon.ionRelMass = baseMass * 0.00000001;
		//tempModel.append(tempIon);
		//tempModel.append(tempIon2);
		//returnVector.push_back(tempModel);
		return returnVector;
	}
}