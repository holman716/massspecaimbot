#include <Utility/DataContainer/Spectra.h>

namespace MSAB
{
	MSABPeak::MSABPeak(double mz, double intensity)
	{
		_mz = mz;
		_intensity = intensity;
	}
	MSABPeak::~MSABPeak()
	{
	}

	const QString MSABSpectrum::TIC = "__TIC";
	const QString MSABSpectrum::MAX_MZ = "__MAX_MZ";
	const QString MSABSpectrum::MIN_MZ = "__MIN_MZ";

	MSABSpectrum::MSABSpectrum(double RT, int MSLevel, double precursorMZ, double providedTIC, QString Name)
	{
		_rt = RT;
		_msLevel = MSLevel;
		_precursorMZ = precursorMZ;
		_reportedTIC = providedTIC;
		_name = Name.isEmpty() ? QString::number(RT, 'g', 4) : Name;
		nextLevelDown = NULL;
	}
	MSABSpectrum::~MSABSpectrum()
	{
		
	}
	void MSABSpectrum::ClearBranches()
	{
		if (nextLevelDown != NULL)
		{
			nextLevelDown->ClearBranches();
			delete nextLevelDown;
		}

	}
	
	void MSABSpectrum::SetPeaks(QList<double> mz, QList<double> intensity)
	{
		if (mz.count() != intensity.count())
			throw new std::exception("Unequal mz/intensity lists");
		for (int x = 0; x < mz.count(); x++)
		{
			SetPeak(mz[x], intensity[x]);
		}
		SortPeaks();
	}
	void MSABSpectrum::SetPeak(double mz, double intensity, bool noSort)
	{
		QString keyString = QString::number(mz, 'g', 8);
		if (_presentMZs.contains(keyString))
		{
			for (int x = 0; x < _peaks.count(); x++)
			{
				if (_peaks[x].SameMZ(mz))
				{
					if (_peaks[x].SameIntensity(intensity))
						break;
					ClearCache();
					_peaks[x].setIntensity(intensity);
					break;
				}
			}
		}
		else
		{
			ClearCache();
			MSABPeak newPeak(mz, intensity);
			_peaks.push_back(newPeak);
			_presentMZs.insert(keyString);
			if (!noSort)
				SortPeaks();
		}
	}
	void MSABSpectrum::SortPeaks()
	{
		std::sort(_peaks.begin(), _peaks.end(), MSAB::MSABPeak::MzSortComparer);
	}
	void MSABSpectrum::RemovePeak(double mz)
	{
		QString keyString = QString::number(mz, 'g', 8);
		if (_presentMZs.contains(keyString))
		{
			for (int x = 0; x < _peaks.count(); x++)
			{
				if (_peaks[x].SameMZ(mz))
				{
					ClearCache();
					_presentMZs.remove(keyString);
					_peaks.removeAt(x);
					break;
				}
			}
		}
	}
	void MSABSpectrum::ClearPeaks()
	{
		_presentMZs.clear();
		_peaks.clear();
	}
	double MSABSpectrum::GetIntensity(double minMZ, double maxMZ)
	{
		QString minString = QString::number(minMZ, 'g', 8);
		QString maxString = QString::number(maxMZ, 'g', 8);
		QString keyString = minString + "-" + maxString;
		if (_aggregateCache.contains(keyString))
			return _aggregateCache[keyString];

		double runningTotal = 0;
		for (int x = 0; x < _peaks.count(); x++)
		{
			double mz = _peaks[x].getMZ();
			if (mz >= minMZ && mz <= maxMZ)			
				runningTotal += _peaks[x].getIntensity();			
		}
		_aggregateCache[keyString] = runningTotal;
		return runningTotal;
	}
	double MSABSpectrum::GetTIC()
	{
		if (!_aggregateCache.contains(MSABSpectrum::TIC))
			SetBaseAggregates();

		return _aggregateCache[MSABSpectrum::TIC];
	}
	double MSABSpectrum::GetMaxMZ()
	{
		if (!_aggregateCache.contains(MSABSpectrum::MAX_MZ))
			SetBaseAggregates();

		return _aggregateCache[MSABSpectrum::MAX_MZ];
	}
	double MSABSpectrum::GetMinMZ()
	{
		if (!_aggregateCache.contains(MSABSpectrum::MIN_MZ))
			SetBaseAggregates();

		return _aggregateCache[MSABSpectrum::MIN_MZ];
	}
	void MSABSpectrum::SetBaseAggregates()
	{
		double minMZ = std::numeric_limits<double>::max();
		double maxMZ = std::numeric_limits<double>::min();

		double runningTotal = 0;
		for (int x = 0; x < _peaks.count(); x++)
		{
			double mz = _peaks[x].getMZ();
			if (mz < minMZ)
				minMZ = mz;
			if (mz > maxMZ)
				maxMZ = mz;
			runningTotal += _peaks[x].getIntensity();
		}

		_aggregateCache[MSABSpectrum::TIC] = _reportedTIC >= 0 ? _reportedTIC : runningTotal;
		_aggregateCache[MSABSpectrum::MIN_MZ] = minMZ;
		_aggregateCache[MSABSpectrum::MAX_MZ] = maxMZ;
	}
}


