#include "MethodTableModel.h"
#include <Utility/DataContainer/Chemestry.h>

namespace MSAB
{
	MassSpecMethodIon::MassSpecMethodIon(QString formulaOrMZ, int msLevel, double precursorMZ, double relInt, bool ionTrace)
	{
		Name = "";
		try
		{
			this->formula.SetFormula(formulaOrMZ);
			this->ionMass = this->formula.TotalMass();
		}
		catch (std::invalid_argument parseError)
		{
			bool conversionOK;
			double mz = formulaOrMZ.toDouble(&conversionOK);

			if (conversionOK)
			{
				ChemFormula customFormula(mz);
				this->formula = customFormula;
			}			
			this->ionMass = mz;
		}	
		
		this->msLevel = msLevel;
		this->precursorMZ = precursorMZ;
		this->relIntensity = relInt;
		this->showIonTrace = ionTrace;
	}

	MassSpecMethodIon::MassSpecMethodIon(double thisMZ, int msLevel, double precursorMZ, double relInt, bool ionTrace)
	{
		ChemFormula customFormula(thisMZ);
		this->formula = customFormula;
		this->ionMass = thisMZ;
		this->msLevel = msLevel;
		this->precursorMZ = precursorMZ;
		this->relIntensity = relInt;
		this->showIonTrace = ionTrace;
	}

	MassSpecMethodIon::MassSpecMethodIon(const MassSpecMethodIon & source)
	{
		Name = source.Name;
		ionMass =source.ionMass;
		ionRelMass= source.ionRelMass;
		msLevel = source.msLevel;
		precursorMZ = source.precursorMZ;
		relIntensity= source.relIntensity;
		formula = source.formula;
		relativeFormula = source.relativeFormula;
		showIonTrace=source.showIonTrace;
		ionLabels = QList<IonLabel>(source.ionLabels);
	}

	MassSpecMethodIon::~MassSpecMethodIon()
	{
	}

	QString MassSpecMethodIon::GetLabel() const
	{
		QStringList LabelList;
		for (auto currentLabel = ionLabels.begin(); currentLabel != ionLabels.end(); currentLabel++)
		{
			QString newBaseName = currentLabel->Name;
			if (currentLabel->maxNumReplacement > 1)
			{
				newBaseName += "(x";
				newBaseName += QString::number(currentLabel->maxNumReplacement);
				newBaseName += ")";
			}
			LabelList.append(newBaseName);
		}

		return LabelList.join("; ");
	}

	QMap<QString, double> MassSpecMethodIon::GetAllIsotopomers()
	{
		QList<IonLabel> rawComboList;
		IonLabel blankIon("", 0);
		rawComboList.append(blankIon);
		for (auto currentLabel : ionLabels)
		{
			//going through the labels one at a time, keep track of which one we're working on
			QString newBaseName = currentLabel.Name;
			//we're going to be updating the rawcombo list so let's keep a snapshot of what it looks like now
			QList<IonLabel> savedComboList(rawComboList);

			//combine the current label with each item in the raw combo list (up to max replacements)
			for (IonLabel nextBase : savedComboList)
			{
				for (int currentDepth = 1; currentDepth <= currentLabel.maxNumReplacement; currentDepth++)
				{
					IonLabel newIon(nextBase); //next item in the raw combo list
					ChemFormula multipliedFormula = ((currentLabel.Formula) * currentDepth);
					newIon.Formula = (newIon.Formula + multipliedFormula);
					QString newName(newBaseName); //this is what the current label looks like
					if (currentDepth > 1)
						newName += QString("(x%1)").arg(QString::number(currentDepth));
						//newName.insert(0, QString::number(currentDepth) + "x"); //...at the current depth
					if (newIon.ionReplacementMass > 0) //add in the rest of the raw ion name
					{
						newName.insert(0, "&");
						newName.insert(0, newIon.Name);
					}
						
					newIon.Name = newName;
					newIon.ionReplacementMass += (currentLabel.ionReplacementMass*currentDepth);
					if (formula.contains(newIon.Formula) || formula.IsCustom())
						rawComboList.append(newIon);
				}
			}
		}
		QMap<QString, double> finalComboList;
		for (auto rawIon : rawComboList)
		{
			if (rawIon.ionReplacementMass == 0)
				continue;
			auto newMass = rawIon.ReplaceMass(formula.TotalMass());
			finalComboList[rawIon.Name] = newMass;
		}

		return finalComboList;
	}

	QVariant MassSpecMethodModel::data(const QModelIndex &index, int role) const 
	{
		if (role == Qt::CheckStateRole && index.column() == 2)
		{
			return _methodItemList[index.row()].showIonTrace ? Qt::Checked : Qt::Unchecked;
		}
		if (role != Qt::DisplayRole && role != Qt::EditRole) return {};
		const auto & ion = _methodItemList[index.row()];
		//auto ion = _methodItemList[index.row()];
		switch (index.column()) {
		case 0: return ion.Name;
		case 1: return ion.ionMass;
		//case 2:
		//	if (role == Qt::CheckStateRole)
		//		return ion.showIonTrace ? Qt::Checked : Qt::Unchecked;
		case 3: return ion.relIntensity;
		//case 4: return ion.integrationRange;
		case 4: return ion.formula.FormulaString();
		case 5: return (ion.msLevel > 1)
			? (ion.precursorMZ >= 0
				? QString::number(ion.precursorMZ, 'g', 4)
				: "{Error}")
			: "N/A";
		case 6: return ion.GetLabel();
		default: return {};
		};
	}

	QVariant MassSpecMethodModel::headerData(int section, Qt::Orientation orientation, int role) const 
	{
		if (orientation != Qt::Horizontal || role != Qt::DisplayRole) return {};
		switch (section) {
		case 0: return "Name";
		case 1: return "Mass";
		case 2: return "ShowIonTrace";
		case 3: return "Relative Intensity";
		//case 4: return "Integration Range";
		case 4: return "Formula";
		case 5:return "Precursor";
		case 6: return "Label";
		default: return {};
		}
	}

	Qt::ItemFlags MassSpecMethodModel::flags(const QModelIndex & index) const
	{
		if (index.column() < 2 || index.column() == 5 || index.column() == 6)
			return QAbstractTableModel::flags(index) | Qt::ItemIsEnabled;
		if (index.column() == 2)
			return QAbstractTableModel::flags(index) | Qt::ItemIsEditable | Qt::ItemIsEnabled |  Qt::ItemIsUserCheckable;
		else if (index.isValid())
			return QAbstractTableModel::flags(index) | Qt::ItemIsEditable | Qt::ItemIsEnabled | Qt::ItemIsSelectable;
		return Qt::ItemFlags();
	}

	bool MassSpecMethodModel::setData(const QModelIndex & index, const QVariant & value, int role)
	{		
		if (!index.isValid())
			return false;
		MassSpecMethodIon& MSMI = _methodItemList[index.row()];
		bool rebaseNeeded = false;

		if (index.column() == 2 && role == Qt::CheckStateRole) //case 2: return "ShowIonTrace";
		{
			MSMI.showIonTrace = (value == Qt::Checked);
			auto test = QObject::receivers(SIGNAL(dataChanged()));
			emit dataChanged(index, index, { role });
		}
		else if (role == Qt::EditRole) {

			//if (index.column() == 0) //case 0: return "Name";
			//	MSMI.Name = value.toString();
			//else if (index.column() == 1) //case 1: return "Mass";
			//{
			//	bool ok;
			//	auto tmpDouble = value.toDouble(&ok);
			//	if (ok)
			//		MSMI.ionMass = tmpDouble;
			//	else
			//		return false;
			//	rebaseNeeded = true;
			//}
			if (index.column() == 3) //case 3: return "Relative Intensity";
			{
				bool ok;
				auto tmpDouble = value.toDouble(&ok);
				if (ok)
					MSMI.relIntensity = tmpDouble;
				else
					return false;
				rebaseNeeded = true;
			}
			//else if (index.column() == 4) //case 4: return "Integration Range";
			//{
			//	bool ok;
			//	auto tmpDouble = value.toDouble(&ok);
			//	if (ok)
			//		MSMI.integrationRange = tmpDouble;
			//	else
			//		return false;
			//}
			else if (index.column() == 4) //case 4: return "Formula";
			{
				try
				{
					MSMI.formula.SetFormula(value.toString());
					MSMI.ionMass = MSMI.formula.TotalMass();
					rebaseNeeded = true;
				}
				catch (std::invalid_argument parseError)
				{
					return false;
				}
			}
			//else if (index.column() == 5) //case 5: return "Label"; 
			//	MSMI.label = value.toString();
			else
				return false;

			_methodItemList.replace(index.row(), MSMI);
			emit dataChanged(index, index, { role });

			if (rebaseNeeded)
				GuessBaseMass();

			return true;
		}

		return false;
	}

	void MassSpecMethodModel::append(const MassSpecMethodIon & ion, int position) 
	{
		beginInsertRows({}, _methodItemList.count(), _methodItemList.count());
		if (position <0 || position>_methodItemList.count())
			_methodItemList.append(ion);
		else
			_methodItemList.insert(position,ion);
		endInsertRows();
		GuessBaseMass();
	}
	void MassSpecMethodModel::remove(int row)
	{
		if (row < 0 || row >= _methodItemList.count())
			return;
		beginRemoveRows({}, row, row);
		_methodItemList.removeAt(row);
		removeRow(row);
		endRemoveRows();

		GuessBaseMass();
	}
	void MassSpecMethodModel::setRT(double newRT)
	{
		if (newRT>=0)
			expectedRT = newRT;
	}
	bool MassSpecMethodModel::singleShown()
	{
		int shown = 0;
		for (auto ion : _methodItemList)
		{
			if (ion.showIonTrace)
				shown++;
		}
		return shown == 1;
	}
	MassSpecMethodModel::MassSpecMethodModel() : QAbstractTableModel()
	{
	}
	MassSpecMethodModel::MassSpecMethodModel(const MassSpecMethodModel & source) : QAbstractTableModel()
	{
		methodName = source.methodName;
		expectedRT = source.expectedRT;
		for (auto it = source._methodItemList.begin(); it != source._methodItemList.end(); it++)
		{
			MassSpecMethodIon tempIon(*it);
			_methodItemList.push_back(tempIon);
		}
	}
	void MassSpecMethodModel::GuessBaseMass()
	{
		double lowestMass = 999999999999;
		double bestRelInt = -1;
		int bestRow = -1;
		//OpenMS::EmpiricalFormula* currentFormula;
		for (auto row = 0; row < _methodItemList.count(); row++)
		{
			MassSpecMethodIon* itemIT = &_methodItemList[row];
			if (itemIT->relIntensity > bestRelInt)
			{
				bestRelInt = itemIT->relIntensity;
				bestRow = row;
				lowestMass = itemIT->ionMass;
			}
			else if (itemIT->relIntensity == bestRelInt && itemIT->ionMass<lowestMass)
			{
				bestRelInt = itemIT->relIntensity;
				bestRow = row;
				lowestMass = itemIT->ionMass;
			}
		}
		if (bestRow < 0)
			return;
		baseMSMIRow = bestRow;
		MassSpecMethodIon* baseItem = &_methodItemList[bestRow];
		baseItem->Name = "Base";
		baseItem->ionRelMass = 0;
		baseItem->relativeFormula.SetFormula("");
		for (auto row = 0; row < _methodItemList.count(); row++)
		{
			if (row == bestRow)
				continue;
			MassSpecMethodIon* thisItem = &_methodItemList[row];
			if (thisItem->formula.IsCustom() || baseItem->formula.IsCustom())
			{
				thisItem->ionRelMass = thisItem->ionMass - baseItem->ionMass;
				thisItem->Name = "  & " + QString::number(thisItem->ionRelMass);
			}
			else
			{
				ChemFormula tmpFormula(thisItem->formula);
				tmpFormula -= baseItem->formula.FormulaString();
				thisItem->relativeFormula = tmpFormula;
				thisItem->ionRelMass = thisItem->ionMass - baseItem->ionMass;
				thisItem->Name = "  & " + tmpFormula.FormulaString();
			}
			
		}

	}
	double IonLabel::ReplaceMass(double mass)
	{
		return (mass - Formula.TotalMass() + ionReplacementMass);
	}
}
