#include <Utility/DataContainer/MSABExperiment.h>

namespace MSAB
{
	MSABExperiment::MSABExperiment()
	{
		isLoaded = false;
		fileListRow = -1;
	}
	MSABExperiment::MSABExperiment(QString location, int row)
	{
		isLoaded = false;
		fileListRow = row;
		FileLocation = location;
	}

	MSABExperiment::MSABExperiment(const MSABExperiment & other)
	{
		isLoaded = other.isLoaded;
		FileLocation = other.FileLocation;
		fileListRow = other.fileListRow;

		_minRT = other._minRT;
		_maxRT = other._maxRT;
		_minMZ = other._minMZ;
		_maxMZ = other._maxMZ;
		_spectra = other._spectra;
		_presentRTs = other._presentRTs;
	}

	MSABExperiment::~MSABExperiment()
	{
		
	}

	void MSABExperiment::ResetMZRT()
	{
		_minRT = _spectra.isEmpty() ? 0 : std::numeric_limits<double>::max();
		_maxRT = _spectra.isEmpty() ? 1 : std::numeric_limits<double>::min();
		_minMZ = _spectra.isEmpty() ? 0 : std::numeric_limits<double>::max();
		_maxMZ = _spectra.isEmpty() ? 1 : std::numeric_limits<double>::min();

		for (int x = 0; x < _spectra.count(); x++)
		{
			auto spectrum = _spectra[x];
			double rt = spectrum.GetRT();
			double spectrumMaxMZ = spectrum.GetMaxMZ();
			double spectrumMinMZ = spectrum.GetMinMZ();
			if (rt < _minRT)
				_minRT = rt;
			if (rt > _maxRT)
				_maxRT = rt;
			if (_maxMZ < spectrumMaxMZ)
				_maxMZ = spectrumMaxMZ;
			if (_minMZ > spectrumMinMZ)
				_minMZ = spectrumMinMZ;
		}
	}

	void MSAB::MSABExperiment::AddSpectra(QList<MSABSpectrum> newSprectra)
	{
		foreach(auto spectrum, newSprectra)
			AddSpectrum(spectrum);
		SortSpectra();
	}

	void MSABExperiment::AddSpectrum(MSABSpectrum newSprectrum,bool noSort)
	{
		auto keyString = QString::number(newSprectrum.GetRT(), 'g', 8);
		if (_presentRTs.contains(keyString))
		{
			bool duplicateFound = false;
			for (int x = 0; x < _spectra.count(); x++)
			{
				if (!_spectra[x].SameRT(newSprectrum.GetRT()))
					continue;
				duplicateFound = true;
				if (_spectra[x].GetLevel() != newSprectrum.GetLevel())
				{
					if (_spectra[x].GetLevel() > newSprectrum.GetLevel())
					{
						if (newSprectrum.nextLevelDown == NULL)
							newSprectrum.nextLevelDown = new MSABSpectrum(_spectra[x]);
						_spectra[x] = newSprectrum;
					}
					else
					{
						auto compareSpectra = &_spectra[x];
						while (true)
						{
							if (compareSpectra->nextLevelDown == NULL)
							{
								compareSpectra->nextLevelDown = &newSprectrum;
								break;
							}
							else
							{
								if (compareSpectra->nextLevelDown->GetLevel() == newSprectrum.GetLevel())
								{
									if (compareSpectra->nextLevelDown->nextLevelDown != NULL &&
										newSprectrum.nextLevelDown == NULL)
										newSprectrum.nextLevelDown = compareSpectra->nextLevelDown->nextLevelDown;
									compareSpectra->nextLevelDown = &newSprectrum;
								}
								else if (compareSpectra->nextLevelDown->GetLevel() > newSprectrum.GetLevel())
								{
									if (newSprectrum.nextLevelDown == NULL)
										newSprectrum.nextLevelDown = compareSpectra->nextLevelDown;
									compareSpectra->nextLevelDown = &newSprectrum;
								}
								else //gonna need to go deeper
								{
									compareSpectra = compareSpectra->nextLevelDown;
								}
							}
						}
					}
				}

			}
			if (!duplicateFound)
				throw new std::exception("Duplicate RT indicated but not detected. Is the QList getting cleared properly?");
		}
		else
		{
			_spectra.push_back(newSprectrum);
			if (!noSort)
				SortSpectra();
			_presentRTs.insert(keyString);
		}


	}

	void MSAB::MSABExperiment::SortSpectra()
	{
		std::sort(_spectra.begin(), _spectra.end(), MSAB::MSABSpectrum::RtSortComparer);
	}

	void MSABExperiment::RemoveSpectrum(double RT, int mzLevel)
	{
		auto keyString = QString::number(RT, 'g', 8);
		if (!_presentRTs.contains(keyString))
			throw new std::exception("RT not found in key list when trying to remove");
		for (int x = 0; x < _spectra.count(); x++)
		{
			if (_spectra[x].SameRT(RT))
			{
				if (_spectra[x].GetLevel() != mzLevel)
				{
					auto currentLevel = &_spectra[x];
					while (true)
					{
						if (currentLevel->nextLevelDown == NULL)
							throw new std::exception("RT not found in data when trying to remove");
						else if (currentLevel->nextLevelDown->GetLevel() == mzLevel)
						{
							auto toDelete = currentLevel->nextLevelDown;
							currentLevel->nextLevelDown = toDelete->nextLevelDown;
							delete toDelete;
							break;
						}
						else
							currentLevel = currentLevel->nextLevelDown;
					}
					_presentRTs.remove(keyString);
				}
			}
		}
	}

	void MSABExperiment::ClearSpectra()
	{
		for (int x = 0; x < _spectra.count(); x++)		
			_spectra[x].ClearBranches();
		_spectra.clear();
		_presentRTs.clear();
	}
	int MSABExperiment::GetIndexClosestToRT(double RT) const
	{
		if (_spectra.count() == 0)
			return -1;
		double closestMatch = std::numeric_limits<double>::max();
		int closestMatchIndex = 0;
		for (int x = 0; x < _spectra.count(); x++)
		{
			double distance = abs(RT - _spectra[x].GetRT());
			if (distance < closestMatch)
			{
				closestMatch = distance;
				closestMatchIndex = x;
			}
		}
		return closestMatchIndex;
	}
}


