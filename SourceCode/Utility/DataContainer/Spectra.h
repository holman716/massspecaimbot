#ifndef MSABSPECTRA_H
#define MSABSPECTRA_H

#include <QList>

using namespace std;

namespace MSAB
{
	class MSABPeak
	{
	public:
		MSABPeak(double mz, double intensity);
		~MSABPeak();

		double getMZ() const { return _mz; }
		void setIntensity(double newInt) { _intensity = newInt; }
		double getIntensity() const { return _intensity; }
		
		bool SameMZ(const double otherMZ) const
		{
			return std::abs(this->_mz - otherMZ) < DBL_EPSILON;
		}
		bool SameIntensity(const double otherIntensity) const
		{
			return std::abs(this->_intensity - otherIntensity) < DBL_EPSILON;
		}
		static bool MzSortComparer(const MSABPeak& first, const MSABPeak& second)
		{
			return first._mz < second._mz;
		}
	private:
		double _mz;
		double _intensity;
	};

	class MSABSpectrum
	{	
	public:
		MSABSpectrum(double RT, int MSLevel, double precursorMZ = -1, double tic=-1, QString Name = "");
		~MSABSpectrum();

		QList<MSABPeak> GetPeaks() const { return _peaks; };
		int GetLevel() const { return _msLevel; }
		double GetPrecursorMZ() const { return _precursorMZ; }
		double GetRT() const { return _rt; }
		QString GetName() const { return _name; }

		//cannot apply const due to cache interaction
		double GetIntensity(double minMZ, double maxMZ); //"0.12345678-9.87654321" format conversion
		double GetTIC(); //Each of these aggreagates will go ahead and cache the others
		double GetMaxMZ();
		double GetMinMZ();
        
		void ClearBranches();
		MSABSpectrum* nextLevelDown;
		//QList<MSABPeak> GetPeaks() { return _peaks; };
		void SetPeaks(QList<double> mz, QList<double> intensity); //go ahead and set aggregates with this one
		void SetPeak(double mz, double intensity, bool noSort = true);
		void SortPeaks();
		//void OverlayPeak(double mz, double intensity);
		void RemovePeak(double mz);
		void ClearPeaks();

		bool SameRT(const double otherRT)
		{
			return std::abs(this->_rt - otherRT) < DBL_EPSILON;
		}
		static bool RtSortComparer(const MSABSpectrum& first, const MSABSpectrum& second)
		{
			return first._rt < second._rt;
		}

		static const QString TIC;
		static const QString MAX_MZ;
		static const QString MIN_MZ;
        
	private:
		QString _name;
		int _msLevel;
		double _precursorMZ;
		double _rt;
		double _reportedTIC;
		QList<MSABPeak> _peaks;
		QSet<QString> _presentMZs; //used to quickly check if we need to overwrite
		QMap<QString, double> _aggregateCache; //cnvert double to string to make it mappable

		void SetBaseAggregates();
		void ClearCache() { _aggregateCache.clear(); };
	};
}

#endif // MSABSPECTRA_H