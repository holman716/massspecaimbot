set(currentDirectory ${base_source_directory}/SourceCode/Utility/DataContainer)

set(sources_list
Chemestry.cpp
MethodTableModel.cpp
MSABExperiment.cpp
Spectra.cpp
)

set(sources_list_h
Chemestry.h
MethodTableModel.h
MSABExperiment.h
Spectra.h
)

set(sources)
foreach(i ${sources_list})
	list(APPEND sources ${currentDirectory}/${i})
endforeach(i)

set(sources_h)
foreach(i ${sources_list_h})
	list(APPEND sources_h ${currentDirectory}/${i})
endforeach(i)

### pass source file list to the upper instance
set(MSAB_sources ${MSAB_sources} ${sources} ${sources_h})
set(MSAB_sources_h ${MSAB_sources_h} ${sources_h})

### source group definition
#source_group("utility" FILES ${sources})
source_group("Source Files\\Utility\\DataContainer" FILES ${sources})
source_group("Header Files\\Utility\\DataContainer" FILES ${sources_h})