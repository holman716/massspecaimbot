#ifndef MSABEXPERIMENT_H
#define MSABEXPERIMENT_H

//#include <OpenMS/KERNEL/MSExperiment.h>
#include <QTime>
#include <QList>
#include <Utility/DataContainer/Spectra.h>
using namespace std;

namespace MSAB
{

	class MSABExperiment //: public OpenMS::MSExperiment
	{	
	public:
		MSABExperiment();
		MSABExperiment(QString location, int row);
		MSABExperiment(const MSABExperiment&);
		//MSABExperiment(MSABExperiment&&) = default;
		~MSABExperiment();
        
		QString FileLocation;
		bool isLoaded;
		int fileListRow;
		QTime LastAccessed;
        void ResetMZRT();
		void AddSpectra(QList<MSABSpectrum> newSprectra);
		void AddSpectrum(MSABSpectrum newSprectra, bool noSort = true);
		void SortSpectra();
		void RemoveSpectrum(double RT, int mzLevel = 1);
		void ClearSpectra();

		int GetIndexClosestToRT(double RT) const;
		MSABSpectrum GetSpectrumAtIndex(int index) const { return _spectra[index]; };
		QList<MSABSpectrum> GetSpectraList() const { return _spectra; }
		int GetSpectraCount() const { return _spectra.count(); }
        double getMinRT() const {return _minRT;}
        double getMaxRT() const {return _maxRT;}
        double getMinMZ() const {return _minMZ;}
        double getMaxMZ() const {return _maxMZ;}
        
	private:		
		QList<MSABSpectrum> _spectra;
		QSet<QString> _presentRTs; //used to quickly check if we need to overwrite
		double _minRT;
		double _maxRT;
        double _minMZ;
        double _maxMZ;
	};
}

#endif // MSABEXPERIMENT_H