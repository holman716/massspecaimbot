#ifndef MSABCHEMESTRY_H
#define MSABCHEMESTRY_H

#include <QString>
#include <QMap>
#include <QList>


using namespace std;

namespace MSAB
{
	static class ChemElementData
	{
		//data from https://www.sisweb.com/referenc/source/exactmas.htm 
		//based on Carbon 12 standard
	public:
		static QMap<QString, QList<QString>> IsotopeChart;
		static QMap<QString, double> IsotopeMassChart;
		static QMap<QString, double> IsotopeRatioChart;
		
		static bool ElementIsValid(QString element);
		static double GetMonoisotopicMass(QString element);
		static double GetAverageMass(QString element);

	private:
		static QMap<QString, QString> MonoIsotopicFormChart;
	};

	class ChemElement
	{
	public:
		ChemElement(QString symbol, int count = 1, bool monoIsotopic = false);
		ChemElement(QString symbol, double mass, int count = 1);
		~ChemElement();

		QString getSingleSymbol() const;
		QString getFullSymbol() const;
		bool IsValid() const { return _isValid; }
		bool IsCustom() const { return _isCustom; }
		int getCount() const { return _count; }
		double getMass() const { return _singleMass * _count; }

		void setCount(int newCount);
		static QList<ChemElement> TranslateFormulaString(QString inputString, QSet<QString> monoIsotopicElements = QSet<QString>());
		static QList<ChemElement> TranslateFormulaString(QString inputString, bool allMonoIso);
		//static bool HillSystemComparer(const ChemElement& first, const ChemElement& second);

		friend bool operator==(const ChemElement& lhs, const ChemElement& rhs)
		{
			if (lhs._symbol != rhs._symbol || lhs._isCustom != rhs._isCustom)
				return false;
			if (lhs._isCustom && abs(lhs._singleMass - rhs._singleMass) > DBL_EPSILON)
				return false;
			return true;
		}
		friend bool operator!=(const ChemElement& lhs, const ChemElement& rhs) { return !(lhs == rhs); }
		friend bool operator<(const ChemElement& lhs, const ChemElement& rhs)
		{
			bool firstIsotope = lhs._symbol.startsWith("_");
			bool secondIsotope = rhs._symbol.startsWith("_");
			QString firstBase = lhs._symbol;
			if (firstIsotope)
			{
				while (firstBase.length() > 0 && (firstBase[0] == '_' || firstBase[0].isNumber()))
					firstBase.remove(0, 1);
			}
			QString secondBase = rhs._symbol;
			if (secondIsotope)
			{
				while (secondBase.length() > 0 && (secondBase[0] == '_' || secondBase[0].isNumber()))
					secondBase.remove(0, 1);
			}
			//NOTE: Exceptions to hill system are not taken inot account at this time
			if (firstBase == "C" && secondBase != "C")
				return true;
			else if (secondBase == "C" && firstBase != "C")
				return false;
			else if (firstBase == "H" && secondBase != "H")
				return true;
			else if (secondBase == "H" && firstBase != "H")
				return false;
			else if (firstBase == secondBase)
			{
				if (firstIsotope && !secondIsotope)
					return false;
				else
					return true;
			}
			else
				return firstBase < secondBase;
		};

	private:
		bool _isValid;
		bool _isCustom;
		QString _symbol;
		int _count; 
		double _singleMass;
	};

	class ChemFormula
	{	
	public:
		ChemFormula(QString InitialFormula = "", QSet<QString> monoIsotopes = QSet<QString>()); //should be able to throw std::invalid_argument
		ChemFormula(double mass);
		~ChemFormula();

		ChemFormula(ChemFormula& other); //copy Constructor
		void SetFormula(QString InitialFormula, QSet<QString> monoIsotopes = QSet<QString>());
		void SetFormula(QString InitialFormula, bool allMonoIso);
        		
		QString FormulaString() const;
		double TotalMass() const;
		bool IsCustom() const;
		bool contains(const QString& formulaString) const;
		bool contains(const ChemFormula& formula) const;
		bool contains(const QList<ChemElement>& elements) const;

		friend bool operator==(const ChemFormula& thisFormula, const ChemFormula& otherFormula);
		friend bool operator!=(const ChemFormula& thisFormula, const ChemFormula& otherFormula);
		ChemFormula& operator+=(const QString& inString);
		ChemFormula& operator-=(const QString& inString);
		ChemFormula& operator+(const ChemFormula& otherFormula);
		ChemFormula& operator-(const ChemFormula& otherFormula);
		ChemFormula& operator*(const int factor);
        
	private:
		QList<ChemElement> _formulaList;
	};
}

#endif // MSABCHEMESTRY_H