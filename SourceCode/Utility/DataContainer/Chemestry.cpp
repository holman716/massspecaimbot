#include <Utility/DataContainer/Chemestry.h>
#include <QRegularExpression>

namespace MSAB
{
	bool ChemElementData::ElementIsValid(QString element)
	{
		return IsotopeChart.contains(element);
	}
	double ChemElementData::GetMonoisotopicMass(QString element)
	{
		if (!ElementIsValid(element))
			return 0.0;
		return IsotopeMassChart[MonoIsotopicFormChart[element]];
	}
	double ChemElementData::GetAverageMass(QString element)
	{
		//TODO: calculate these all out and replace with a map
		if (!ElementIsValid(element))
			return 0.0;

		auto forms = IsotopeChart[element];
		double runningMass = 0;
		for (int x = 0; x < forms.count(); x++)
			runningMass += (IsotopeMassChart[forms[x]] * IsotopeRatioChart[forms[x]]);
		return runningMass;
	}

	ChemElement::ChemElement(QString symbol, int count, bool monoIsotiopic)
	{
		if (!ChemElementData::ElementIsValid(symbol))
		{
			_isValid = false;
			_isCustom = false;
			_symbol = "";
			_count = 0;
			_singleMass = 0;
		}
		else
		{
			_isValid = true;
			_isCustom = false;
			_symbol = symbol;
			_count = count;
			if (monoIsotiopic)
				_singleMass = ChemElementData::GetMonoisotopicMass(symbol);
			else
				_singleMass = ChemElementData::GetAverageMass(symbol);
		}
	}
	ChemElement::ChemElement(QString symbol, double mass, int count)
	{
		_isValid = true;
		_isCustom = true;
		_symbol = symbol;
		_count = count;
		_singleMass = mass;
	}
	ChemElement::~ChemElement()
	{
	}
	QString ChemElement::getSingleSymbol() const
	{
		return _symbol;
	}
	QString ChemElement::getFullSymbol() const
	{
		if (_count == 0)
			return "";
		if (_count == 1)
			return _symbol;
		return _symbol + QString::number(_count);
	}
	void ChemElement::setCount(int newCount)
	{
		_count = newCount;
	}
	QList<ChemElement> ChemElement::TranslateFormulaString(QString inputString, QSet<QString> monoIsotopicElements)
	{
		QRegularExpression symbolRx("^((?:_\\d+)?[A-Z][a-z]*)([-\\d]*)");

		QList<ChemElement> returnList;
		QString currentSymbol = "";
		QString currentCount = "";
		int x = 0;
		while (inputString.length() > 0) //cycles for each element
		{
			auto symbolMatch = symbolRx.match(inputString);
			while (!symbolMatch.hasMatch())
			{
				inputString.remove(0, 1);
				if (inputString.length() == 0)
					break;
				symbolMatch = symbolRx.match(inputString);
			}
			if (!symbolMatch.hasMatch())
				throw std::invalid_argument("Bad symbol in input formula");
			inputString.remove(0,symbolMatch.captured(0).length());
			bool curentIsValid = true;
			bool isMono = monoIsotopicElements.contains(symbolMatch.captured(1));
			int symbolCount = symbolMatch.captured(2).length() == 0 ? 1
				: symbolMatch.captured(2).toInt(&curentIsValid);
			if (curentIsValid)
			{
				ChemElement newElement(symbolMatch.captured(1), symbolCount, isMono);
				returnList.append(newElement);
			}			
		}
		std::sort(returnList.begin(), returnList.end());
		return returnList;
	}
	QList<ChemElement> ChemElement::TranslateFormulaString(QString inputString, bool allMonoIso)
	{
		QRegularExpression symbolRx("^((?:_\\d+)?[A-Z][a-z]*)([-\\d]*)");

		QList<ChemElement> returnList;
		QString currentSymbol = "";
		QString currentCount = "";
		int x = 0;
		while (inputString.length() > 0) //cycles for each element
		{
			auto symbolMatch = symbolRx.match(inputString);
			while (!symbolMatch.hasMatch())
			{
				inputString.remove(0, 1);
				if (inputString.length() == 0)
					break;
				symbolMatch = symbolRx.match(inputString);
			}
			if (!symbolMatch.hasMatch())
				throw std::invalid_argument("Bad symbol in input formula");
			inputString.remove(0, symbolMatch.captured(0).length());
			bool curentIsValid = true;
			bool isMono = allMonoIso;
			int symbolCount = symbolMatch.captured(2).length() == 0 ? 1
				: symbolMatch.captured(2).toInt(&curentIsValid);
			if (curentIsValid)
			{
				ChemElement newElement(symbolMatch.captured(1), symbolCount, isMono);
				returnList.append(newElement);
			}
		}
		std::sort(returnList.begin(), returnList.end());
		return returnList;
	}

	ChemFormula::ChemFormula(QString InitialFormula, QSet<QString> monoIsotopes)
	{
		SetFormula(InitialFormula, monoIsotopes);
	}
	ChemFormula::ChemFormula(double mass)
	{
		ChemElement customElement("?",mass);
		_formulaList.push_back(customElement);
	}
	ChemFormula::~ChemFormula()
	{
	}
	ChemFormula::ChemFormula(ChemFormula & other)// : ChemFormula(other.FormulaString())
	{
		_formulaList = other._formulaList;
	}
	void ChemFormula::SetFormula(QString InitialFormula, QSet<QString> monoIsotopes)
	{
		_formulaList = ChemElement::TranslateFormulaString(InitialFormula, monoIsotopes);
	}
	void ChemFormula::SetFormula(QString InitialFormula, bool allMonoIso)
	{
		_formulaList = ChemElement::TranslateFormulaString(InitialFormula, true);
	}
	QString ChemFormula::FormulaString() const
	{
		QString returnString = "";
		for (int x = 0; x < _formulaList.count(); x++)
		{
			returnString += _formulaList[x].getFullSymbol();
		}
		return returnString;
	}
	double ChemFormula::TotalMass() const
	{
		double returnMass = 0;
		for (int x = 0; x < _formulaList.count(); x++)
		{
			returnMass += _formulaList[x].getMass();
		}
		return returnMass;
	}
	bool ChemFormula::IsCustom() const
	{
		for (auto element : _formulaList)
			if (element.IsCustom())
				return true;
		return false;
	}
	bool ChemFormula::contains(const QString& formulaString) const
	{
		return contains(ChemElement::TranslateFormulaString(formulaString));
	}
	bool ChemFormula::contains(const ChemFormula& formula) const
	{
		return contains(formula._formulaList);
	}
	bool ChemFormula::contains(const QList<ChemElement>& elements) const
	{
		for (const ChemElement outsideElement: elements)
		{
			bool found = false;
			for (ChemElement presentElement: _formulaList)
			{
				if (presentElement != outsideElement)
					continue;
				found = true;
				if (presentElement.getCount() < outsideElement.getCount())
					return false;
			}
			if (!found)
				return false;
		}
		return true;
	}

	bool operator==(const ChemFormula& thisFormula, const ChemFormula& otherFormula)
	{
		for (const ChemElement outsideElement : otherFormula._formulaList)
		{
			bool found = false;
			for (ChemElement presentElement : thisFormula._formulaList)
			{
				if (presentElement != outsideElement)
					continue;
				found = true;
				if (presentElement.getCount() != outsideElement.getCount())
					return false;
			}
			if (!found)
				return false;
		}
		return true;
	}
	bool operator!=(const ChemFormula& thisFormula, const ChemFormula& otherFormula)
	{
		return !(thisFormula== otherFormula);
	}
	ChemFormula & ChemFormula::operator+=(const QString& inString)
	{
		auto otherElements = ChemElement::TranslateFormulaString(inString);
		for (const ChemElement outsideElement : otherElements)
		{
			bool found = false;
			for (ChemElement& presentElement : _formulaList)
			{
				if (presentElement != outsideElement)
					continue;
				found = true;
				presentElement.setCount(presentElement.getCount() + outsideElement.getCount());
			}
			if (!found)
				_formulaList.append(outsideElement);
		}
		std::sort(_formulaList.begin(), _formulaList.end());
		return *this;
	}
	ChemFormula & ChemFormula::operator-=(const QString& inString)
	{
		auto otherElements = ChemElement::TranslateFormulaString(inString);
		for (const ChemElement outsideElement : otherElements)
		{
			bool remove = false;
			bool found = false;
			for (ChemElement& presentElement : _formulaList)
			{
				if (presentElement != outsideElement)
					continue;
				found = true;
				presentElement.setCount(presentElement.getCount() - outsideElement.getCount());
				//NOTE: it is currently possible to have negative elements
				//right now this is by design
				if (presentElement.getCount() == 0)
					remove = true;
			}
			if (remove)
				_formulaList.removeOne(outsideElement);
			else if (!found)
			{
				ChemElement negativeElement(outsideElement);
				negativeElement.setCount(outsideElement.getCount()*-1);
				_formulaList.append(negativeElement);
			}
				
		}
		std::sort(_formulaList.begin(), _formulaList.end());
		return *this;
	}
	ChemFormula & ChemFormula::operator+(const ChemFormula& otherFormula)
	{
		ChemFormula* returnFormula = new ChemFormula(FormulaString());
		*returnFormula += otherFormula.FormulaString();

		std::sort(returnFormula->_formulaList.begin(), returnFormula->_formulaList.end());
		return *returnFormula;
	}
	ChemFormula & ChemFormula::operator-(const ChemFormula& otherFormula)
	{
		ChemFormula* returnFormula = new ChemFormula(FormulaString());
		*returnFormula -= otherFormula.FormulaString();

		std::sort(returnFormula->_formulaList.begin(), returnFormula->_formulaList.end());
		return *returnFormula;
	}
	ChemFormula & ChemFormula::operator*(const int factor)
	{
		if (factor == 0)
			return ChemFormula("");
		ChemFormula* returnFormula = new ChemFormula(FormulaString());
		for (ChemElement& presentElement : returnFormula->_formulaList)
		{
			presentElement.setCount(presentElement.getCount() * factor);
		}
		return *returnFormula;
	}

	QMap<QString, QList<QString>> ChemElementData::IsotopeChart
	{
		{"H",{"_1H","_2H"}},
		{"He",{"_3He","_4He"}},
		{"Li",{"_6Li","_7Li"}},
		{"Be",{"_9Be"}},
		{"B",{"_10B","_11B"}},
		{"C",{"_12C","_13C"}},
		{"N",{"_14N","_15N"}},
		{"O",{"_16O","_17O","_18O"}},
		{"F",{"_19F"}},
		{"Ne",{"_20Ne","_21Ne","_22Ne"}},
		{"Na",{"_23Na"}},
		{"Mg",{"_24Mg","_25Mg","_26Mg"}},
		{"Al",{"_27Al"}},
		{"Si",{"_28Si","_29Si","_30Si"}},
		{"P",{"_31P"}},
		{"S",{"_32S","_33S","_34S"}},
		{"Cl",{"_35Cl"}},
		{"S",{"_36S"}},
		{"Ar",{"_36Ar"}},
		{"Cl",{"_37Cl"}},
		{"Ar",{"_38Ar"}},
		{"K",{"_39K"}},
		{"Ar",{"_40Ar"}},
		{"Ca",{"_40Ca"}},
		{"K",{"_40K","_41K"}},
		{"Ca",{"_42Ca","_43Ca","_44Ca"}},
		{"Sc",{"_45Sc"}},
		{"Ti",{"_46Ti"}},
		{"Ca",{"_46Ca"}},
		{"Ti",{"_47Ti","_48Ti"}},
		{"Ca",{"_48Ca"}},
		{"Ti",{"_49Ti","_50Ti"}},
		{"Cr",{"_50Cr"}},
		{"V",{"_50V","_51V"}},
		{"Cr",{"_52Cr","_53Cr","_54Cr"}},
		{"Fe",{"_54Fe"}},
		{"Mn",{"_55Mn"}},
		{"Fe",{"_56Fe","_57Fe","_58Fe"}},
		{"Ni",{"_58Ni"}},
		{"Co",{"_59Co"}},
		{"Ni",{"_60Ni","_61Ni","_62Ni"}},
		{"Cu",{"_63Cu"}},
		{"Ni",{"_64Ni"}},
		{"Zn",{"_64Zn"}},
		{"Cu",{"_65Cu"}},
		{"Zn",{"_66Zn","_67Zn","_68Zn"}},
		{"Ga",{"_69Ga"}},
		{"Ge",{"_70Ge"}},
		{"Zn",{"_70Zn"}},
		{"Ga",{"_71Ga"}},
		{"Ge",{"_72Ge","_73Ge","_74Ge"}},
		{"Se",{"_74Se"}},
		{"As",{"_75As"}},
		{"Se",{"_76Se"}},
		{"Ge",{"_76Ge"}},
		{"Se",{"_77Se","_78Se"}},
		{"Kr",{"_78Kr"}},
		{"Br",{"_79Br"}},
		{"Kr",{"_80Kr"}},
		{"Se",{"_80Se"}},
		{"Br",{"_81Br"}},
		{"Kr",{"_82Kr"}},
		{"Se",{"_82Se"}},
		{"Kr",{"_83Kr","_84Kr"}},
		{"Sr",{"_84Sr"}},
		{"Rb",{"_85Rb"}},
		{"Sr",{"_86Sr"}},
		{"Kr",{"_86Kr"}},
		{"Sr",{"_87Sr"}},
		{"Rb",{"_87Rb"}},
		{"Sr",{"_88Sr"}},
		{"Y",{"_89Y"}},
		{"Zr",{"_90Zr","_91Zr","_92Zr"}},
		{"Mo",{"_92Mo"}},
		{"Nb",{"_93Nb"}},
		{"Mo",{"_94Mo"}},
		{"Zr",{"_94Zr"}},
		{"Mo",{"_95Mo","_96Mo"}},
		{"Ru",{"_96Ru"}},
		{"Zr",{"_96Zr"}},
		{"Mo",{"_97Mo"}},
		{"Ru",{"_98Ru"}},
		{"Mo",{"_98Mo"}},
		{"Ru",{"_99Ru","_100Ru"}},
		{"Mo",{"_100Mo"}},
		{"Ru",{"_101Ru","_102Ru"}},
		{"Pd",{"_102Pd"}},
		{"Rh",{"_103Rh"}},
		{"Pd",{"_104Pd"}},
		{"Ru",{"_104Ru"}},
		{"Pd",{"_105Pd","_106Pd"}},
		{"Cd",{"_106Cd"}},
		{"Ag",{"_107Ag"}},
		{"Pd",{"_108Pd"}},
		{"Cd",{"_108Cd"}},
		{"Ag",{"_109Ag"}},
		{"Cd",{"_110Cd"}},
		{"Pd",{"_110Pd"}},
		{"Cd",{"_111Cd","_112Cd"}},
		{"Sn",{"_112Sn"}},
		{"In",{"_113In"}},
		{"Cd",{"_113Cd"}},
		{"Sn",{"_114Sn"}},
		{"Cd",{"_114Cd"}},
		{"Sn",{"_115Sn"}},
		{"In",{"_115In"}},
		{"Sn",{"_116Sn"}},
		{"Cd",{"_116Cd"}},
		{"Sn",{"_117Sn","_118Sn"}},
		{"Sn",{"_119Sn","_120Sn"}},
		{"Te",{"_120Te"}},
		{"Sb",{"_121Sb"}},
		{"Te",{"_122Te"}},
		{"Sn",{"_122Sn"}},
		{"Sb",{"_123Sb"}},
		{"Te",{"_123Te","_124Te"}},
		{"Sn",{"_124Sn"}},
		{"Xe",{"_124Xe"}},
		{"Te",{"_125Te","_126Te"}},
		{"X",{"_126X"}},
		{"I",{"_127I"}},
		{"Xe",{"_128Xe"}},
		{"Te",{"_128Te"}},
		{"Xe",{"_129Xe","_130Xe"}},
		{"Te",{"_130Te"}},
		{"Ba",{"_130Ba"}},
		{"Xe",{"_131Xe","_132Xe"}},
		{"Ba",{"_132Ba"}},
		{"Cs",{"_133Cs"}},
		{"Ba",{"_134Ba"}},
		{"Xe",{"_134Xe"}},
		{"Ba",{"_135Ba","_136Ba"}},
		{"Ce",{"_136Ce"}},
		{"Xe",{"_136Xe"}},
		{"Ba",{"_137Ba","_138Ba"}},
		{"Ce",{"_138Ce"}},
		{"La",{"_138La","_139La"}},
		{"Ce",{"_140Ce"}},
		{"Pr",{"_141Pr"}},
		{"Nd",{"_142Nd"}},
		{"Ce",{"_142Ce"}},
		{"Nd",{"_143Nd","_144Nd"}},
		{"Sm",{"_144Sm"}},
		{"Nd",{"_145Nd","_146Nd"}},
		{"Sm",{"_147Sm","_148Sm"}},
		{"Nd",{"_148Nd"}},
		{"Sm",{"_149Sm","_150Sm"}},
		{"Nd",{"_150Nd"}},
		{"Eu",{"_151Eu"}},
		{"Sm",{"_152Sm"}},
		{"Gd",{"_152Gd"}},
		{"Eu",{"_153Eu"}},
		{"Gd",{"_154Gd"}},
		{"Sm",{"_154Sm"}},
		{"Gd",{"_155Gd","_156Gd"}},
		{"Dy",{"_156Dy"}},
		{"Gd",{"_157Gd","_158Gd"}},
		{"Dy",{"_158Dy"}},
		{"Tb",{"_159Tb"}},
		{"Dy",{"_160Dy"}},
		{"Gd",{"_160Gd"}},
		{"Dy",{"_161Dy","_162Dy"}},
		{"Er",{"_162Er"}},
		{"Dy",{"_163Dy","_164Dy"}},
		{"Er",{"_164Er"}},
		{"Ho",{"_165Ho"}},
		{"Er",{"_166Er","_167Er","_168Er"}},
		{"Yb",{"_168Yb"}},
		{"Tm",{"_169Tm"}},
		{"Yb",{"_170Yb"}},
		{"Er",{"_170Er"}},
		{"Yb",{"_171Yb","_172Yb"}},
		{"Yb",{"_173Yb","_174Yb"}},
		{"Hf",{"_174Hf"}},
		{"Lu",{"_175Lu"}},
		{"Hf",{"_176Hf"}},
		{"Yb",{"_176Yb"}},
		{"Lu",{"_176Lu"}},
		{"Hf",{"_177Hf","_178Hf"}},
		{"Hf",{"_179Hf","_180Hf"}},
		{"W",{"_180W"}},
		{"Ta",{"_180Ta","_181Ta"}},
		{"W",{"_182W","_183W","_184W"}},
		{"Os",{"_184Os"}},
		{"Re",{"_185Re"}},
		{"Os",{"_186Os"}},
		{"W",{"_186W"}},
		{"Os",{"_187Os"}},
		{"Re",{"_187Re"}},
		{"Os",{"_188Os","_189Os","_190Os"}},
		{"Pt",{"_190Pt"}},
		{"Ir",{"_191Ir"}},
		{"Pt",{"_192Pt"}},
		{"Os",{"_192Os"}},
		{"Ir",{"_193Ir"}},
		{"Pt",{"_194Pt","_195Pt","_196Pt"}},
		{"Hg",{"_196Hg"}},
		{"Au",{"_197Au"}},
		{"Hg",{"_198Hg"}},
		{"Pt",{"_198Pt"}},
		{"Hg",{"_199Hg","_200Hg"}},
		{"Hg",{"_201Hg","_202Hg"}},
		{"Tl",{"_203Tl"}},
		{"Pb",{"_204Pb"}},
		{"Hg",{"_204Hg"}},
		{"Tl",{"_205Tl"}},
		{"Pb",{"_206Pb","_207Pb","_208Pb"}},
		{"Bi",{"_209Bi"}},
		{"Th",{"_232Th"}},
		{"U",{"_234U","_235U"}},
		{"U",{"_238U"}}
	};
	QMap<QString, double> ChemElementData::IsotopeMassChart
	{
		{"_1H",1.007825},
		{"_2H",2.014102},
		{"_3He",3.016029},
		{"_4He",4.002603},
		{"_6Li",6.015123},
		{"_7Li",7.016005},
		{"_9Be",9.012183},
		{"_10B",10.012938},
		{"_11B",11.009305},
		{"_12C",12},
		{"_13C",13.003355},
		{"_14N",14.003074},
		{"_15N",15.000109},
		{"_16O",15.994915},
		{"_17O",16.999131},
		{"_18O",17.999159},
		{"_19F",18.998403},
		{"_20Ne",19.992439},
		{"_21Ne",20.993845},
		{"_22Ne",21.991384},
		{"_23Na",22.98977},
		{"_24Mg",23.985045},
		{"_25Mg",24.985839},
		{"_26Mg",25.982595},
		{"_27Al",26.981541},
		{"_28Si",27.976928},
		{"_29Si",28.976496},
		{"_30Si",29.973772},
		{"_31P",30.973763},
		{"_32S",31.972072},
		{"_33S",32.971459},
		{"_34S",33.967868},
		{"_35Cl",34.968853},
		{"_36S",35.967079},
		{"_36Ar",35.967546},
		{"_37Cl",36.965903},
		{"_38Ar",37.962732},
		{"_39K",38.963708},
		{"_40Ar",39.962383},
		{"_40Ca",39.962591},
		{"_40K",39.963999},
		{"_41K",40.961825},
		{"_42Ca",41.958622},
		{"_43Ca",42.95877},
		{"_44Ca",43.955485},
		{"_45Sc",44.955914},
		{"_46Ti",45.952633},
		{"_46Ca",45.953689},
		{"_47Ti",46.951765},
		{"_48Ti",47.947947},
		{"_48Ca",47.952532},
		{"_49Ti",48.947871},
		{"_50Ti",49.944786},
		{"_50Cr",49.946046},
		{"_50V",49.947161},
		{"_51V",50.943963},
		{"_52Cr",51.94051},
		{"_53Cr",52.940651},
		{"_54Cr",53.938882},
		{"_54Fe",53.939612},
		{"_55Mn",54.938046},
		{"_56Fe",55.934939},
		{"_57Fe",56.935396},
		{"_58Fe",57.933278},
		{"_58Ni",57.935347},
		{"_59Co",58.933198},
		{"_60Ni",59.930789},
		{"_61Ni",60.931059},
		{"_62Ni",61.928346},
		{"_63Cu",62.929599},
		{"_64Ni",63.927968},
		{"_64Zn",63.929145},
		{"_65Cu",64.927792},
		{"_66Zn",65.926035},
		{"_67Zn",66.927129},
		{"_68Zn",67.924846},
		{"_69Ga",68.925581},
		{"_70Ge",69.92425},
		{"_70Zn",69.925325},
		{"_71Ga",70.924701},
		{"_72Ge",71.92208},
		{"_73Ge",72.923464},
		{"_74Ge",73.921179},
		{"_74Se",73.922477},
		{"_75As",74.921596},
		{"_76Se",75.919207},
		{"_76Ge",75.921403},
		{"_77Se",76.919908},
		{"_78Se",77.917304},
		{"_78Kr",77.920397},
		{"_79Br",78.918336},
		{"_80Kr",79.916375},
		{"_80Se",79.916521},
		{"_81Br",80.91629},
		{"_82Kr",81.913483},
		{"_82Se",81.916709},
		{"_83Kr",82.914134},
		{"_84Kr",83.911506},
		{"_84Sr",83.913428},
		{"_85Rb",84.9118},
		{"_86Sr",85.909273},
		{"_86Kr",85.910614},
		{"_87Sr",86.908902},
		{"_87Rb",86.909184},
		{"_88Sr",87.905625},
		{"_89Y",88.905856},
		{"_90Zr",89.904708},
		{"_91Zr",90.905644},
		{"_92Zr",91.905039},
		{"_92Mo",91.906809},
		{"_93Nb",92.906378},
		{"_94Mo",93.905086},
		{"_94Zr",93.906319},
		{"_95Mo",94.905838},
		{"_96Mo",95.904676},
		{"_96Ru",95.907596},
		{"_96Zr",95.908272},
		{"_97Mo",96.906018},
		{"_98Ru",97.905287},
		{"_98Mo",97.905405},
		{"_99Ru",98.905937},
		{"_100Ru",99.904218},
		{"_100Mo",99.907473},
		{"_101Ru",100.905581},
		{"_102Ru",101.904348},
		{"_102Pd",101.905609},
		{"_103Rh",102.905503},
		{"_104Pd",103.904026},
		{"_104Ru",103.905422},
		{"_105Pd",104.905075},
		{"_106Pd",105.903475},
		{"_106Cd",105.906461},
		{"_107Ag",106.905095},
		{"_108Pd",107.903894},
		{"_108Cd",107.904186},
		{"_109Ag",108.904754},
		{"_110Cd",109.903007},
		{"_110Pd",109.905169},
		{"_111Cd",110.904182},
		{"_112Cd",111.902761},
		{"_112Sn",111.904826},
		{"_113In",112.904056},
		{"_113Cd",112.904401},
		{"_114Sn",113.902784},
		{"_114Cd",113.903361},
		{"_115Sn",114.903348},
		{"_115In",114.903875},
		{"_116Sn",115.901744},
		{"_116Cd",115.904758},
		{"_117Sn",116.902954},
		{"_118Sn",117.901607},
		{"_119Sn",118.90331},
		{"_120Sn",119.902199},
		{"_120Te",119.904021},
		{"_121Sb",120.903824},
		{"_122Te",121.903055},
		{"_122Sn",121.90344},
		{"_123Sb",122.904222},
		{"_123Te",122.904278},
		{"_124Te",123.902825},
		{"_124Sn",123.905271},
		{"_124Xe",123.905894},
		{"_125Te",124.904435},
		{"_126Te",125.90331},
		{"_126X",125.904281},
		{"_127I",126.904477},
		{"_128Xe",127.903531},
		{"_128Te",127.904464},
		{"_129Xe",128.90478},
		{"_130Xe",129.90351},
		{"_130Te",129.906229},
		{"_130Ba",129.906277},
		{"_131Xe",130.905076},
		{"_132Xe",131.904148},
		{"_132Ba",131.905042},
		{"_133Cs",132.905433},
		{"_134Ba",133.90449},
		{"_134Xe",133.905395},
		{"_135Ba",134.905668},
		{"_136Ba",135.904556},
		{"_136Ce",135.90714},
		{"_136Xe",135.907219},
		{"_137Ba",136.905816},
		{"_138Ba",137.905236},
		{"_138Ce",137.905996},
		{"_138La",137.907114},
		{"_139La",138.906355},
		{"_140Ce",139.905442},
		{"_141Pr",140.907657},
		{"_142Nd",141.907731},
		{"_142Ce",141.909249},
		{"_143Nd",142.909823},
		{"_144Nd",143.910096},
		{"_144Sm",143.912009},
		{"_145Nd",144.912582},
		{"_146Nd",145.913126},
		{"_147Sm",146.914907},
		{"_148Sm",147.914832},
		{"_148Nd",147.916901},
		{"_149Sm",148.917193},
		{"_150Sm",149.917285},
		{"_150Nd",149.9209},
		{"_151Eu",150.91986},
		{"_152Sm",151.919741},
		{"_152Gd",151.919803},
		{"_153Eu",152.921243},
		{"_154Gd",153.920876},
		{"_154Sm",153.922218},
		{"_155Gd",154.822629},
		{"_156Gd",155.92213},
		{"_156Dy",155.924287},
		{"_157Gd",156.923967},
		{"_158Gd",157.924111},
		{"_158Dy",157.924412},
		{"_159Tb",158.92535},
		{"_160Dy",159.925203},
		{"_160Gd",159.927061},
		{"_161Dy",160.926939},
		{"_162Dy",161.926805},
		{"_162Er",161.928787},
		{"_163Dy",162.928737},
		{"_164Dy",163.929183},
		{"_164Er",163.929211},
		{"_165Ho",164.930332},
		{"_166Er",165.930305},
		{"_167Er",166.932061},
		{"_168Er",167.932383},
		{"_168Yb",167.933908},
		{"_169Tm",168.934225},
		{"_170Yb",169.934774},
		{"_170Er",169.935476},
		{"_171Yb",170.936338},
		{"_172Yb",171.936393},
		{"_173Yb",172.938222},
		{"_174Yb",173.938873},
		{"_174Hf",173.940065},
		{"_175Lu",174.940785},
		{"_176Hf",175.94142},
		{"_176Yb",175.942576},
		{"_176Lu",175.942694},
		{"_177Hf",176.943233},
		{"_178Hf",177.94371},
		{"_179Hf",178.945827},
		{"_180Hf",179.946561},
		{"_180W",179.946727},
		{"_180Ta",179.947489},
		{"_181Ta",180.948014},
		{"_182W",181.948225},
		{"_183W",182.950245},
		{"_184W",183.950953},
		{"_184Os",183.952514},
		{"_185Re",184.952977},
		{"_186Os",185.953852},
		{"_186W",185.954377},
		{"_187Os",186.955762},
		{"_187Re",186.955765},
		{"_188Os",187.95585},
		{"_189Os",188.958156},
		{"_190Os",189.958455},
		{"_190Pt",189.959937},
		{"_191Ir",190.960603},
		{"_192Pt",191.961049},
		{"_192Os",191.961487},
		{"_193Ir",192.962942},
		{"_194Pt",193.962679},
		{"_195Pt",194.964785},
		{"_196Pt",195.964947},
		{"_196Hg",195.965812},
		{"_197Au",196.96656},
		{"_198Hg",197.96676},
		{"_198Pt",197.967879},
		{"_199Hg",198.968269},
		{"_200Hg",199.968316},
		{"_201Hg",200.970293},
		{"_202Hg",201.970632},
		{"_203Tl",202.972336},
		{"_204Pb",203.973037},
		{"_204Hg",203.973481},
		{"_205Tl",204.97441},
		{"_206Pb",205.974455},
		{"_207Pb",206.975885},
		{"_208Pb",207.976641},
		{"_209Bi",208.980388},
		{"_232Th",232.038054},
		{"_234U",234.040947},
		{"_235U",235.043925},
		{"_238U",238.050786}
	};
	QMap<QString, double> ChemElementData::IsotopeRatioChart
	{
		{"_1H",0.9999},
		{"_2H",0.00015},
		{"_3He",0.000001},
		{"_4He",1},
		{"_6Li",0.0742},
		{"_7Li",0.9258},
		{"_9Be",1},
		{"_10B",0.198},
		{"_11B",0.802},
		{"_12C",0.989},
		{"_13C",0.011},
		{"_14N",0.9963},
		{"_15N",0.0037},
		{"_16O",0.9976},
		{"_17O",0.00038},
		{"_18O",0.002},
		{"_19F",1},
		{"_20Ne",0.906},
		{"_21Ne",0.0026},
		{"_22Ne",0.092},
		{"_23Na",1},
		{"_24Mg",0.789},
		{"_25Mg",0.1},
		{"_26Mg",0.111},
		{"_27Al",1},
		{"_28Si",0.9223},
		{"_29Si",0.0467},
		{"_30Si",0.031},
		{"_31P",1},
		{"_32S",0.9502},
		{"_33S",0.0075},
		{"_34S",0.0421},
		{"_35Cl",0.7577},
		{"_36S",0.0002},
		{"_36Ar",0.0034},
		{"_37Cl",0.2423},
		{"_38Ar",0.00063},
		{"_39K",0.932},
		{"_40Ar",0.996},
		{"_40Ca",0.9695},
		{"_40K",0.00012},
		{"_41K",0.0673},
		{"_42Ca",0.0065},
		{"_43Ca",0.0014},
		{"_44Ca",0.02086},
		{"_45Sc",1},
		{"_46Ti",0.08},
		{"_46Ca",0.00004},
		{"_47Ti",0.073},
		{"_48Ti",0.738},
		{"_48Ca",0.0019},
		{"_49Ti",0.055},
		{"_50Ti",0.054},
		{"_50Cr",0.0435},
		{"_50V",0.0025},
		{"_51V",0.9975},
		{"_52Cr",0.8379},
		{"_53Cr",0.095},
		{"_54Cr",0.0236},
		{"_54Fe",0.058},
		{"_55Mn",1},
		{"_56Fe",0.9172},
		{"_57Fe",0.022},
		{"_58Fe",0.0028},
		{"_58Ni",0.6827},
		{"_59Co",1},
		{"_60Ni",0.261},
		{"_61Ni",0.0113},
		{"_62Ni",0.0359},
		{"_63Cu",0.6917},
		{"_64Ni",0.0091},
		{"_64Zn",0.486},
		{"_65Cu",0.3083},
		{"_66Zn",0.279},
		{"_67Zn",0.041},
		{"_68Zn",0.188},
		{"_69Ga",0.601},
		{"_70Ge",0.205},
		{"_70Zn",0.006},
		{"_71Ga",0.399},
		{"_72Ge",0.274},
		{"_73Ge",0.078},
		{"_74Ge",0.365},
		{"_74Se",0.009},
		{"_75As",1},
		{"_76Se",0.09},
		{"_76Ge",0.078},
		{"_77Se",0.076},
		{"_78Se",0.235},
		{"_78Kr",0.0035},
		{"_79Br",0.5069},
		{"_80Kr",0.0225},
		{"_80Se",0.496},
		{"_81Br",0.4931},
		{"_82Kr",0.116},
		{"_82Se",0.094},
		{"_83Kr",0.115},
		{"_84Kr",0.57},
		{"_84Sr",0.0056},
		{"_85Rb",0.7217},
		{"_86Sr",0.0986},
		{"_86Kr",0.173},
		{"_87Sr",0.07},
		{"_87Rb",0.2784},
		{"_88Sr",0.8258},
		{"_89Y",1},
		{"_90Zr",0.5145},
		{"_91Zr",0.1127},
		{"_92Zr",0.1717},
		{"_92Mo",0.1484},
		{"_93Nb",1},
		{"_94Mo",0.0925},
		{"_94Zr",0.1733},
		{"_95Mo",0.1592},
		{"_96Mo",0.1668},
		{"_96Ru",0.0552},
		{"_96Zr",0.0278},
		{"_97Mo",0.0955},
		{"_98Ru",0.0188},
		{"_98Mo",0.2413},
		{"_99Ru",0.127},
		{"_100Ru",0.126},
		{"_100Mo",0.0963},
		{"_101Ru",0.17},
		{"_102Ru",0.316},
		{"_102Pd",0.0102},
		{"_103Rh",1},
		{"_104Pd",0.1114},
		{"_104Ru",0.187},
		{"_105Pd",0.2233},
		{"_106Pd",0.2733},
		{"_106Cd",0.0125},
		{"_107Ag",0.5184},
		{"_108Pd",0.2646},
		{"_108Cd",0.0089},
		{"_109Ag",0.4816},
		{"_110Cd",0.1249},
		{"_110Pd",0.1172},
		{"_111Cd",0.128},
		{"_112Cd",0.2413},
		{"_112Sn",0.0097},
		{"_113In",0.043},
		{"_113Cd",0.1222},
		{"_114Sn",0.0065},
		{"_114Cd",0.2873},
		{"_115Sn",0.0036},
		{"_115In",0.957},
		{"_116Sn",0.147},
		{"_116Cd",0.0749},
		{"_117Sn",0.077},
		{"_118Sn",0.243},
		{"_119Sn",0.086},
		{"_120Sn",0.324},
		{"_120Te",0.00096},
		{"_121Sb",0.573},
		{"_122Te",0.026},
		{"_122Sn",0.046},
		{"_123Sb",0.427},
		{"_123Te",0.0091},
		{"_124Te",0.0482},
		{"_124Sn",0.056},
		{"_124Xe",0.001},
		{"_125Te",0.0714},
		{"_126Te",0.1895},
		{"_126X",0.0009},
		{"_127I",1},
		{"_128Xe",0.0191},
		{"_128Te",0.3169},
		{"_129Xe",0.264},
		{"_130Xe",0.041},
		{"_130Te",0.338},
		{"_130Ba",0.0011},
		{"_131Xe",0.212},
		{"_132Xe",0.269},
		{"_132Ba",0.001},
		{"_133Cs",1},
		{"_134Ba",0.0242},
		{"_134Xe",0.104},
		{"_135Ba",0.0659},
		{"_136Ba",0.0785},
		{"_136Ce",0.0019},
		{"_136Xe",0.089},
		{"_137Ba",0.1123},
		{"_138Ba",0.717},
		{"_138Ce",0.0025},
		{"_138La",0.0009},
		{"_139La",0.9991},
		{"_140Ce",0.8848},
		{"_141Pr",1},
		{"_142Nd",0.2713},
		{"_142Ce",0.1108},
		{"_143Nd",0.1218},
		{"_144Nd",0.238},
		{"_144Sm",0.031},
		{"_145Nd",0.083},
		{"_146Nd",0.1719},
		{"_147Sm",0.15},
		{"_148Sm",0.113},
		{"_148Nd",0.0576},
		{"_149Sm",0.138},
		{"_150Sm",0.074},
		{"_150Nd",0.0564},
		{"_151Eu",0.478},
		{"_152Sm",0.267},
		{"_152Gd",0.002},
		{"_153Eu",0.522},
		{"_154Gd",0.0218},
		{"_154Sm",0.227},
		{"_155Gd",0.148},
		{"_156Gd",0.2047},
		{"_156Dy",0.0006},
		{"_157Gd",0.1565},
		{"_158Gd",0.2484},
		{"_158Dy",0.001},
		{"_159Tb",1},
		{"_160Dy",0.0234},
		{"_160Gd",0.2186},
		{"_161Dy",0.189},
		{"_162Dy",0.255},
		{"_162Er",0.0014},
		{"_163Dy",0.249},
		{"_164Dy",0.282},
		{"_164Er",0.0161},
		{"_165Ho",1},
		{"_166Er",0.336},
		{"_167Er",0.2295},
		{"_168Er",0.268},
		{"_168Yb",0.0013},
		{"_169Tm",1},
		{"_170Yb",0.0305},
		{"_170Er",0.149},
		{"_171Yb",0.143},
		{"_172Yb",0.219},
		{"_173Yb",0.1612},
		{"_174Yb",0.318},
		{"_174Hf",0.0016},
		{"_175Lu",0.974},
		{"_176Hf",0.052},
		{"_176Yb",0.127},
		{"_176Lu",0.026},
		{"_177Hf",0.186},
		{"_178Hf",0.271},
		{"_179Hf",0.1374},
		{"_180Hf",0.352},
		{"_180W",0.0013},
		{"_180Ta",0.00012},
		{"_181Ta",0.9999},
		{"_182W",0.263},
		{"_183W",0.143},
		{"_184W",0.3067},
		{"_184Os",0.0002},
		{"_185Re",0.374},
		{"_186Os",0.0158},
		{"_186W",0.286},
		{"_187Os",0.016},
		{"_187Re",0.626},
		{"_188Os",0.133},
		{"_189Os",0.161},
		{"_190Os",0.264},
		{"_190Pt",0.0001},
		{"_191Ir",0.373},
		{"_192Pt",0.0079},
		{"_192Os",0.41},
		{"_193Ir",0.627},
		{"_194Pt",0.329},
		{"_195Pt",0.338},
		{"_196Pt",0.253},
		{"_196Hg",0.0015},
		{"_197Au",1},
		{"_198Hg",0.101},
		{"_198Pt",0.072},
		{"_199Hg",0.17},
		{"_200Hg",0.231},
		{"_201Hg",0.132},
		{"_202Hg",0.2965},
		{"_203Tl",0.2952},
		{"_204Pb",0.014},
		{"_204Hg",0.068},
		{"_205Tl",0.7048},
		{"_206Pb",0.241},
		{"_207Pb",0.221},
		{"_208Pb",0.524},
		{"_209Bi",1},
		{"_232Th",1},
		{"_234U",0.00006},
		{"_235U",0.0072},
		{"_238U",0.9927}
	};
	QMap<QString, QString> ChemElementData::MonoIsotopicFormChart
	{
		{"H","_1H"},
		{"He","_3He"},
		{"Li","_6Li"},
		{"Be","_9Be"},
		{"B","_10B"},
		{"C","_12C"},
		{"N","_14N"},
		{"O","_16O"},
		{"F","_19F"},
		{"Ne","_20Ne"},
		{"Na","_23Na"},
		{"Mg","_24Mg"},
		{"Al","_27Al"},
		{"Si","_28Si"},
		{"P","_31P"},
		{"S","_32S"},
		{"Cl","_35Cl"},
		{"S","_36S"},
		{"Ar","_36Ar"},
		{"Cl","_37Cl"},
		{"Ar","_38Ar"},
		{"K","_39K"},
		{"Ar","_40Ar"},
		{"Ca","_40Ca"},
		{"K","_40K"},
		{"Ca","_42Ca"},
		{"Sc","_45Sc"},
		{"Ti","_46Ti"},
		{"Ca","_46Ca"},
		{"Ti","_47Ti"},
		{"Ca","_48Ca"},
		{"Ti","_49Ti"},
		{"Cr","_50Cr"},
		{"V","_50V"},
		{"Cr","_52Cr"},
		{"Fe","_54Fe"},
		{"Mn","_55Mn"},
		{"Fe","_56Fe"},
		{"Ni","_58Ni"},
		{"Co","_59Co"},
		{"Ni","_60Ni"},
		{"Cu","_63Cu"},
		{"Ni","_64Ni"},
		{"Zn","_64Zn"},
		{"Cu","_65Cu"},
		{"Zn","_66Zn"},
		{"Ga","_69Ga"},
		{"Ge","_70Ge"},
		{"Zn","_70Zn"},
		{"Ga","_71Ga"},
		{"Ge","_72Ge"},
		{"Se","_74Se"},
		{"As","_75As"},
		{"Se","_76Se"},
		{"Ge","_76Ge"},
		{"Se","_77Se"},
		{"Kr","_78Kr"},
		{"Br","_79Br"},
		{"Kr","_80Kr"},
		{"Se","_80Se"},
		{"Br","_81Br"},
		{"Kr","_82Kr"},
		{"Se","_82Se"},
		{"Kr","_83Kr"},
		{"Sr","_84Sr"},
		{"Rb","_85Rb"},
		{"Sr","_86Sr"},
		{"Kr","_86Kr"},
		{"Sr","_87Sr"},
		{"Rb","_87Rb"},
		{"Sr","_88Sr"},
		{"Y","_89Y"},
		{"Zr","_90Zr"},
		{"Mo","_92Mo"},
		{"Nb","_93Nb"},
		{"Mo","_94Mo"},
		{"Zr","_94Zr"},
		{"Mo","_95Mo"},
		{"Ru","_96Ru"},
		{"Zr","_96Zr"},
		{"Mo","_97Mo"},
		{"Ru","_98Ru"},
		{"Mo","_98Mo"},
		{"Ru","_99Ru"},
		{"Mo","_100Mo"},
		{"Ru","_101Ru"},
		{"Pd","_102Pd"},
		{"Rh","_103Rh"},
		{"Pd","_104Pd"},
		{"Ru","_104Ru"},
		{"Pd","_105Pd"},
		{"Cd","_106Cd"},
		{"Ag","_107Ag"},
		{"Pd","_108Pd"},
		{"Cd","_108Cd"},
		{"Ag","_109Ag"},
		{"Cd","_110Cd"},
		{"Pd","_110Pd"},
		{"Cd","_111Cd"},
		{"Sn","_112Sn"},
		{"In","_113In"},
		{"Cd","_113Cd"},
		{"Sn","_114Sn"},
		{"Cd","_114Cd"},
		{"Sn","_115Sn"},
		{"In","_115In"},
		{"Sn","_116Sn"},
		{"Cd","_116Cd"},
		{"Sn","_117Sn"},
		{"Sn","_119Sn"},
		{"Te","_120Te"},
		{"Sb","_121Sb"},
		{"Te","_122Te"},
		{"Sn","_122Sn"},
		{"Sb","_123Sb"},
		{"Te","_123Te"},
		{"Sn","_124Sn"},
		{"Xe","_124Xe"},
		{"Te","_125Te"},
		{"X","_126X"},
		{"I","_127I"},
		{"Xe","_128Xe"},
		{"Te","_128Te"},
		{"Xe","_129Xe"},
		{"Te","_130Te"},
		{"Ba","_130Ba"},
		{"Xe","_131Xe"},
		{"Ba","_132Ba"},
		{"Cs","_133Cs"},
		{"Ba","_134Ba"},
		{"Xe","_134Xe"},
		{"Ba","_135Ba"},
		{"Ce","_136Ce"},
		{"Xe","_136Xe"},
		{"Ba","_137Ba"},
		{"Ce","_138Ce"},
		{"La","_138La"},
		{"Ce","_140Ce"},
		{"Pr","_141Pr"},
		{"Nd","_142Nd"},
		{"Ce","_142Ce"},
		{"Nd","_143Nd"},
		{"Sm","_144Sm"},
		{"Nd","_145Nd"},
		{"Sm","_147Sm"},
		{"Nd","_148Nd"},
		{"Sm","_149Sm"},
		{"Nd","_150Nd"},
		{"Eu","_151Eu"},
		{"Sm","_152Sm"},
		{"Gd","_152Gd"},
		{"Eu","_153Eu"},
		{"Gd","_154Gd"},
		{"Sm","_154Sm"},
		{"Gd","_155Gd"},
		{"Dy","_156Dy"},
		{"Gd","_157Gd"},
		{"Dy","_158Dy"},
		{"Tb","_159Tb"},
		{"Dy","_160Dy"},
		{"Gd","_160Gd"},
		{"Dy","_161Dy"},
		{"Er","_162Er"},
		{"Dy","_163Dy"},
		{"Er","_164Er"},
		{"Ho","_165Ho"},
		{"Er","_166Er"},
		{"Yb","_168Yb"},
		{"Tm","_169Tm"},
		{"Yb","_170Yb"},
		{"Er","_170Er"},
		{"Yb","_171Yb"},
		{"Yb","_173Yb"},
		{"Hf","_174Hf"},
		{"Lu","_175Lu"},
		{"Hf","_176Hf"},
		{"Yb","_176Yb"},
		{"Lu","_176Lu"},
		{"Hf","_177Hf"},
		{"Hf","_179Hf"},
		{"W","_180W"},
		{"Ta","_180Ta"},
		{"W","_182W"},
		{"Os","_184Os"},
		{"Re","_185Re"},
		{"Os","_186Os"},
		{"W","_186W"},
		{"Os","_187Os"},
		{"Re","_187Re"},
		{"Os","_188Os"},
		{"Pt","_190Pt"},
		{"Ir","_191Ir"},
		{"Pt","_192Pt"},
		{"Os","_192Os"},
		{"Ir","_193Ir"},
		{"Pt","_194Pt"},
		{"Hg","_196Hg"},
		{"Au","_197Au"},
		{"Hg","_198Hg"},
		{"Pt","_198Pt"},
		{"Hg","_199Hg"},
		{"Hg","_201Hg"},
		{"Tl","_203Tl"},
		{"Pb","_204Pb"},
		{"Hg","_204Hg"},
		{"Tl","_205Tl"},
		{"Pb","_206Pb"},
		{"Bi","_209Bi"},
		{"Th","_232Th"},
		{"U","_234U"},
		{"U","_238U"}
	};
}


