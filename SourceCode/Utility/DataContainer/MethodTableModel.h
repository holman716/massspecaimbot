#ifndef MSABMETHODTABLEMODEL_H
#define MSABMETHODTABLEMODEL_H

#include <vector>
#include <QtGUI>
#include <QtWidgets>
#include <Utility/DataContainer/Chemestry.h>
//#include <OpenMS/DATASTRUCTURES/String.h>

using namespace std;

namespace MSAB
{
	struct IonLabel
	{
		QString Name;
		ChemFormula Formula;
		double ionReplacementMass;
		int maxNumReplacement;

		IonLabel(QString element, double mass,int replacement=2)
		{
			Formula.SetFormula(element, true);
			ionReplacementMass = mass;
			maxNumReplacement = replacement;


			QString newBaseName = QString::number(round(mass));
			newBaseName += element;
			//QString newBaseName = tmp.toQString();
			//newBaseName += "[";
			//newBaseName += QString::number(round(mass));
			//newBaseName += "]";
			Name = newBaseName;
		}
		IonLabel(const IonLabel& other)
		{
			Name = other.Name;
			Formula = other.Formula;
			ionReplacementMass = other.ionReplacementMass;
			maxNumReplacement = other.maxNumReplacement;
		}
		double ReplaceMass(double mass);

		~IonLabel()
		{
		}

		bool operator==(IonLabel& other) 
		{
			return (Name == other.Name && Formula != other.Formula && ionReplacementMass == other.ionReplacementMass);
		}

		bool operator!=(IonLabel& other)
		{
			return !(*this == other);
			//return (Name != other.Name || Element!=other.Element || ionReplacementMass != other.ionReplacementMass);
		}
	};

	struct MassSpecMethodIon
	{
		MassSpecMethodIon(QString formulaOrMZ, int msLevel, double precursorMZ, double relInt = 100, bool ionTrace = false);
		MassSpecMethodIon(double thisMZ, int msLevel, double precursorMZ, double relInt = 100, bool ionTrace = false);
		MassSpecMethodIon(const MassSpecMethodIon& source);
		~MassSpecMethodIon();
		QString Name;
		double ionMass;
		double ionRelMass;
		double relIntensity;
		ChemFormula formula;
		ChemFormula relativeFormula;
		int msLevel;
		double precursorMZ;
		QString GetLabel() const;
		bool showIonTrace;
		QList<IonLabel> ionLabels;

		QMap<QString, double> GetAllIsotopomers();

		bool operator ==(MassSpecMethodIon& other)
		{
			if (Name != other.Name
				|| ionMass != other.ionMass
				|| relIntensity != other.relIntensity
				|| formula.FormulaString() != other.formula.FormulaString()
				|| relativeFormula.FormulaString() != other.relativeFormula.FormulaString()
				|| showIonTrace != other.showIonTrace
				|| ionLabels.size() != other.ionLabels.size())
				return false;
			for (int x = 0; x < ionLabels.size(); x++)
				if (ionLabels[x] != other.ionLabels[x])
					return false;
			return true;
		}
		bool operator !=(MassSpecMethodIon& other)
		{
			if (Name != other.Name
				|| ionMass != other.ionMass
				|| relIntensity != other.relIntensity
				|| formula.FormulaString() != other.formula.FormulaString()
				|| relativeFormula.FormulaString() != other.relativeFormula.FormulaString()
				|| showIonTrace != other.showIonTrace
				|| ionLabels.size() != other.ionLabels.size())
				return true;
			for (int x = 0; x < ionLabels.size(); x++)
				if (ionLabels[x] != other.ionLabels[x])
					return true;
			return false;
		}
	};

	class MassSpecMethodModel : public QAbstractTableModel
	{	
	public:
		int rowCount(const QModelIndex &) const override { return _methodItemList.count(); }
		int columnCount(const QModelIndex &) const override { return 7; }
		QVariant data(const QModelIndex &index, int role) const override;
		QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
		Qt::ItemFlags flags(const QModelIndex &index) const override;
		bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;
		//bool insertRows(int position, int rows, const QModelIndex &index = QModelIndex()) override;
		//bool removeRows(int position, int rows, const QModelIndex &index = QModelIndex()) override;
		void append(const MassSpecMethodIon & ion, int position = -1);
		void remove(int row);
		void setRT(double newRT);
		QString methodName;
		double expectedRT;
		int baseMSMIRow;
		void clear() { _methodItemList.clear(); }
		int methodIonCount() { return _methodItemList.size(); }
		bool singleShown();
		MassSpecMethodIon* getRawMethodIon(int row) { return &_methodItemList[row]; }

		MassSpecMethodModel();
		MassSpecMethodModel(const MassSpecMethodModel & source);
	private:
		QList<MassSpecMethodIon> _methodItemList;

		void GuessBaseMass();
	};
}

#endif // MSABMETHODTABLEMODEL_H