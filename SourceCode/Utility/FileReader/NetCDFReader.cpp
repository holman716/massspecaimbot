#include <Utility/FileReader/NetCDFReader.h>
#include <netcdf.h>

namespace MSAB
{
	void NetCDFReader::LoadNetCDFExperiment(MSABExperiment& experiment)
	{
		map<QString, vector<double>> experimentMap;
		{
			vector<QString> neededValues{ "scan_acquisition_time" ,"scan_index" ,"mass_values" ,"intensity_values" };
			experimentMap = GetNetCDFValues(experiment.FileLocation, neededValues);
		}

		int massStackIterator = 0;
		QList<MSABSpectrum> newSpectraList;
		for (int scanNumber = 0; scanNumber < experimentMap["scan_acquisition_time"].size(); scanNumber++)
		{
			if (isCanceled)
				break;

			double RT = experimentMap["scan_acquisition_time"][scanNumber];
			MSABSpectrum newSpectrum(RT, 1);
			//newSpectrum.setNativeID(experimentMap["scan_acquisition_time"][scanNumber]);

			int numberOfHits = 0;
			if (scanNumber == experimentMap["scan_acquisition_time"].size() - 1)
				numberOfHits = experimentMap["mass_values"].size() - experimentMap["scan_index"][scanNumber];
			else
				numberOfHits = experimentMap["scan_index"][scanNumber + 1] - experimentMap["scan_index"][scanNumber];

			float rtTIC = 0;

			//gather all peaks and add at one
			QList<double> peakMZs;
			QList<double> peakIntensities;
			while (numberOfHits > 0)
			{
				if (massStackIterator >= experimentMap["mass_values"].size())
					throw "Iterator error! Tried to access index " + massStackIterator;

				peakMZs.push_back(experimentMap["mass_values"][massStackIterator]);
				peakIntensities.push_back(experimentMap["intensity_values"][massStackIterator]);
				//MSABPeak newPeak(peakMZ, peakIntensity);

				massStackIterator++;
				numberOfHits--;
			}
			newSpectrum.SetPeaks(peakMZs, peakIntensities);
			newSpectraList.push_back(newSpectrum);
		}
		experiment.AddSpectra(newSpectraList);
		//return experiment;
	}

	std::map<QString, std::vector<double>> MSAB::NetCDFReader::GetNetCDFValues(QString filePath, std::vector<QString> varIDStrings)

	{
		/* This will be the netCDF ID for the file and data variable. */
		int ncid, varid;

		//int data_in[6][12];

		/* Loop indexes, and error handling. */
		int numDims, y, retval;

		/* Open the file. NC_NOWRITE tells netCDF we want read-only access
		 * to the file.*/
		if ((retval = nc_open(filePath.toLocal8Bit().data(), NC_NOWRITE, &ncid)))
			throw retval;

		map<QString, vector<double>> returnMap;
		for (auto varIDString = varIDStrings.begin(); varIDString != varIDStrings.end(); ++varIDString)
		{
			if (isCanceled)
				break;

			vector<double> currentList;
			if ((retval = nc_inq_varid(ncid, varIDString->toLocal8Bit().data(), &varid)))
				throw retval;
			if ((retval = nc_inq_varndims(ncid, varid, &numDims)))
				throw retval;
			//ss << "|||\t" << numDims << "\t|||" << std::endl;

			//how big is each dimension?
			int *dimIDs;
			dimIDs = (int*)malloc(sizeof(int)*numDims);
			if ((retval = nc_inq_vardimid(ncid, varid, dimIDs)))
				throw retval;

			size_t dimSize;
			for (int x = 0; x < numDims; x++)
			{
				if ((retval = nc_inq_dimlen(ncid, *(dimIDs + x), &dimSize)))
					throw retval;
				//ss << "\t#\t" << dimSize << std::endl;
			}


			if (numDims != 1)
			{
				returnMap[*varIDString] = currentList;
				continue;
			}
			double *dimValues;
			dimValues = (double*)malloc(sizeof(double)*dimSize);
			if ((retval = nc_get_var_double(ncid, varid, dimValues)))
				throw retval;
			//ss << varIDString << std::endl;
			currentList.reserve(dimSize);
			for (size_t x = 0; x < dimSize; x++)
			{
				currentList.push_back(dimValues[x]);
				//ss << "\t\t\t" << dimValues[x] << std::endl;
			}
			returnMap[*varIDString] = currentList;
		}

		//ss << std::endl;
		nc_close(ncid);
		return returnMap;
	}

	std::vector<double> NetCDFReader::GetNetCDFValue(QString filePath, QString varIDString)
	{
		vector<QString> singleList;
		singleList.push_back(varIDString);
		auto tmp = GetNetCDFValues(filePath, singleList);
		return tmp[varIDString];
	}
}


