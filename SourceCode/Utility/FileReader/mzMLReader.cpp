#include <Utility/FileReader/mzMLReader.h>
#include <QRegularExpression>
#include <QFile>
#include <QTextStream>
#include <Utility/BinaryHandler.h>

namespace MSAB
{
	void mzMLReader::Load_mzMLExperiment(MSABExperiment & experiment)
	{
		QFile inFile(experiment.FileLocation);
		if (!inFile.open(QIODevice::ReadOnly | QIODevice::Text))
			return;

		//initilize blank data points
		_rtInMinutes = -1;
		_msLevel = -1;
		_spectrumName = "";
		_precursorMZ = -1;
		_64Bit = true;
		//_filterMinIntensity = 0;
		_spectraTIC = -1;
		_expectedPeaks = -1;
		_readingMZ = false;
		_readingIntensity = false;

		//set up regex
		//(previous work using XML readers has resulted in the whole file being loaded into memory- I don't want that)
		QRegularExpression spectraStartRx("<spectrum.*id=\"([^\"]+)\".*defaultArrayLength=\"?(\\d+)", QRegularExpression::CaseInsensitiveOption);
		QRegularExpression msLevelRx("<cvParam.*accession=\"?MS:1000511.*value=\"?(\\d+)", QRegularExpression::CaseInsensitiveOption);
		QRegularExpression precursorRx("<cvParam.*accession=\"?MS:1000744.*value=\"([^\"]+)", QRegularExpression::CaseInsensitiveOption);
		QRegularExpression ticRx("<cvParam.*accession=\"?MS:1000285.*value=\"([^\"]+)", QRegularExpression::CaseInsensitiveOption);
		QRegularExpression rtRx("<cvParam.*accession=\"?MS:1000016.*value=\"([^\"]+).*unitAccession=\"([^\"]+)", QRegularExpression::CaseInsensitiveOption);
		QRegularExpression precisionRx("<cvParam.*accession=\"?MS:1000521", QRegularExpression::CaseInsensitiveOption);
		QRegularExpression mzArrayRx("<cvParam.*accession=\"?MS:1000514", QRegularExpression::CaseInsensitiveOption);
		QRegularExpression intensityArrayRx("<cvParam.*accession=\"?MS:1000515", QRegularExpression::CaseInsensitiveOption);
		QRegularExpression binaryArrayRx("<binary>([^<]+)<\\/binary>", QRegularExpression::CaseInsensitiveOption);
		QRegularExpression endRx("<\\/spectrum>", QRegularExpression::CaseInsensitiveOption);
		
		//read one line at a time
		QTextStream reader(&inFile);
		QString line = reader.readLine();
		while (!reader.atEnd() && !_isCanceled)
		{			
			auto spectraStartMatch = spectraStartRx.match(line);
			auto msLevelMatch = msLevelRx.match(line);
			auto precursorMatch = precursorRx.match(line);
			auto ticMatch = ticRx.match(line);
			auto rtMatch = rtRx.match(line);
			auto precisionMatch = precisionRx.match(line);
			auto mzMatch = mzArrayRx.match(line);
			auto intensityMatch = intensityArrayRx.match(line);
			auto binaryMatch = binaryArrayRx.match(line);
			auto endMatch = endRx.match(line);
			
			bool conversionOK;
			if (spectraStartMatch.hasMatch())
			{
				_spectrumName = spectraStartMatch.captured(1);
				_expectedPeaks = spectraStartMatch.captured(2).toInt(&conversionOK);
				if (!conversionOK)
					_expectedPeaks = -1;
			}
			else if (msLevelMatch.hasMatch())
			{
				_msLevel = msLevelMatch.captured(1).toInt(&conversionOK);
				if (!conversionOK)
					_msLevel = -1;
			}
			else if (precursorMatch.hasMatch())
			{
				_precursorMZ = precursorMatch.captured(1).toDouble(&conversionOK);
				if (!conversionOK)
					_precursorMZ = -1;
			}
			else if (ticMatch.hasMatch())
			{
				_spectraTIC = ticMatch.captured(1).toDouble(&conversionOK);
				if (!conversionOK)
					_spectraTIC = -1;
			}
			else if (rtMatch.hasMatch())
			{
				_rtInMinutes = rtMatch.captured(1).toDouble(&conversionOK);
				if (!conversionOK)
					_rtInMinutes = -1;
				else if (rtMatch.captured(2) == "MS:1000039")
					_rtInMinutes /= 60;
			}
			else if (precisionMatch.hasMatch())
			{
				_64Bit = false;
			}
			else if (mzMatch.hasMatch())
				_readingMZ = true;
			else if (intensityMatch.hasMatch())
				_readingIntensity = true;
			else if (binaryMatch.hasMatch() && _expectedPeaks>0)
			{
				auto capture = binaryMatch.captured(1);
				if (_readingMZ)// && _spectrumName == "controllerType=0 controllerNumber=1 scan=1176")
				{
					int test = capture.length();
					_mzList = BinaryHandler::StringToDoubleArray(capture, _expectedPeaks,_64Bit);

					//test for mzML conversion failure
					auto lowestMZ = *std::min_element(_mzList.begin(), _mzList.end());
					if (lowestMZ < 0) //something went wrong
						_mzList.clear();

					_readingMZ = false;
					_64Bit = true;
				}
				else if (_readingIntensity)
				{
					_intensityList = BinaryHandler::StringToDoubleArray(capture, _expectedPeaks, _64Bit);
					_readingIntensity = false;
					_64Bit = true;
				}
			}
			else if (endMatch.hasMatch())
				CompileSpectraInfo(experiment);

			line = reader.readLine();
		}

		if (!_isCanceled)
			experiment.SortSpectra();
	}
	void mzMLReader::CompileSpectraInfo(MSABExperiment& experiment)
	{
		if (_rtInMinutes < 0 || _msLevel < 0 
			|| (_msLevel > 1 && _precursorMZ < 0)
			|| _spectraTIC < 0 || _expectedPeaks < 0
			|| _mzList.count() != _intensityList.count())
		{
			_rtInMinutes = -1;
			_msLevel = -1;
			_spectrumName = "";
			_precursorMZ = -1;
			_64Bit = true;
			//_filterMinIntensity = 0;
			_spectraTIC = -1;
			_expectedPeaks = -1;
			_mzList.clear();
			_intensityList.clear();
			_readingMZ = false;
			_readingIntensity = false;
		}
		MSABSpectrum current(_rtInMinutes * 60, _msLevel, _precursorMZ, _spectraTIC, _spectrumName);

		//filter peaks
		auto highestPeak = *std::max_element(_intensityList.begin(), _intensityList.end());
		auto filterMinIntensity = abs(highestPeak * 0.02);
		if (!_mzList.isEmpty() )
		{
			for (auto x = _mzList.count() - 1; x >= 0; x--)
			{
				if (_intensityList[x] < filterMinIntensity)
				{
					_mzList.removeAt(x);
					_intensityList.removeAt(x);
				}
			}
		}
		current.SetPeaks(_mzList, _intensityList);
		experiment.AddSpectrum(current);
		
		//clean slate afterwards
		_rtInMinutes = -1;
		_msLevel = -1;
		_spectrumName = "";
		_precursorMZ = -1;
		_64Bit = true;
		//_filterMinIntensity = 0;
		_spectraTIC = -1;
		_expectedPeaks = -1;
		_mzList.clear();
		_intensityList.clear();
		_readingMZ = false;
		_readingIntensity = false;
	}
}


