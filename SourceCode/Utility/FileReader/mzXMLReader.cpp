#include <Utility/FileReader/mzXMLReader.h>
#include <QRegularExpression>
#include <QFile>
#include <QTextStream>
#include <Utility/BinaryHandler.h>

namespace MSAB
{
	void mzXMLReader::Load_mzXMLExperiment(MSABExperiment & experiment)
	{
		QFile inFile(experiment.FileLocation);
		if (!inFile.open(QIODevice::ReadOnly | QIODevice::Text))
			return;

		//initilize blank data points
		_rtInSeconds = -1;
		_msLevel = -1;
		_spectrumName = "";
		_precursorMZ = -1;
		_64Bit = true;
		//_filterMinIntensity = 0;
		_spectraTIC = -1;
		_expectedPeaks = -1;

		//set up regex
		//(previous work using XML readers has resulted in the whole file being loaded into memory- I don't want that)
		QRegularExpression spectraStartRx("<scan.*num=\"([^\"]+)\"", QRegularExpression::CaseInsensitiveOption);
		QRegularExpression msLevelRx("msLevel=\"([^\"]+)\"", QRegularExpression::CaseInsensitiveOption);
		QRegularExpression peakCountRx("peaksCount=\"([^\"]+)\"", QRegularExpression::CaseInsensitiveOption);
		QRegularExpression precursorRx("<precursor.*>([^<]+)<\\/precursor", QRegularExpression::CaseInsensitiveOption);
		QRegularExpression ticRx("totIonCurrent=\"([^\"]+)\"", QRegularExpression::CaseInsensitiveOption);
		QRegularExpression rtRx("retentionTime=\"PT([^\"]+)S\"", QRegularExpression::CaseInsensitiveOption);
		QRegularExpression precisionRx("precision=\"([^\"]+)\"", QRegularExpression::CaseInsensitiveOption);
		QRegularExpression binaryArrayRx(">([^<]+)<\\/peaks>", QRegularExpression::CaseInsensitiveOption);
		QRegularExpression endRx("<\\/scan>", QRegularExpression::CaseInsensitiveOption);
		
		//read one line at a time
		QTextStream reader(&inFile);
		QString line = reader.readLine();
		while (!reader.atEnd() && !_isCanceled)
		{			
			auto spectraStartMatch = spectraStartRx.match(line);
			auto msLevelMatch = msLevelRx.match(line);
			auto peakCountMatch = peakCountRx.match(line);
			auto precursorMatch = precursorRx.match(line);
			auto ticMatch = ticRx.match(line);
			auto rtMatch = rtRx.match(line);
			auto precisionMatch = precisionRx.match(line);
			auto binaryMatch = binaryArrayRx.match(line);
			auto endMatch = endRx.match(line);
			
			bool conversionOK;
			if (spectraStartMatch.hasMatch())
			{
				_spectrumName = spectraStartMatch.captured(1);
			}
			else if (msLevelMatch.hasMatch())
			{
				_msLevel = msLevelMatch.captured(1).toInt(&conversionOK);
				if (!conversionOK)
					_msLevel = -1;
			}
			else if (peakCountMatch.hasMatch())
			{
				_expectedPeaks = peakCountMatch.captured(1).toInt(&conversionOK);
				if (!conversionOK)
					_expectedPeaks = -1;
			}
			else if (precursorMatch.hasMatch())
			{
				_precursorMZ = precursorMatch.captured(1).toDouble(&conversionOK);
				if (!conversionOK)
					_precursorMZ = -1;
			}
			else if (ticMatch.hasMatch())
			{
				_spectraTIC = ticMatch.captured(1).toDouble(&conversionOK);
				if (!conversionOK)
					_spectraTIC = -1;
			}
			else if (rtMatch.hasMatch())
			{
				_rtInSeconds = rtMatch.captured(1).toDouble(&conversionOK);
				if (!conversionOK)
					_rtInSeconds = -1;
			}
			else if (precisionMatch.hasMatch())
			{
				_64Bit = precisionMatch.captured(1) == "64";
			}
			else if (binaryMatch.hasMatch() && _expectedPeaks>0)
			{
				auto capture = binaryMatch.captured(1);
				auto combinedList = BinaryHandler::StringToDoubleArray(capture, _expectedPeaks*2, _64Bit,true);
				for (int x = 0; x < combinedList.count() - 1; x += 2)
				{
					_mzList.push_back(combinedList[x]);
					_intensityList.push_back(combinedList[x+1]);
				}
			}
			else if (endMatch.hasMatch())
				CompileSpectraInfo(experiment);

			line = reader.readLine();
		}

		if (!_isCanceled)
			experiment.SortSpectra();
	}
	void mzXMLReader::CompileSpectraInfo(MSABExperiment& experiment)
	{
		if (_rtInSeconds < 0 || _msLevel < 0
			|| (_msLevel > 1 && _precursorMZ < 0)
			|| _spectraTIC < 0 || _expectedPeaks < 0
			|| _mzList.count() != _intensityList.count())
		{
			_rtInSeconds = -1;
			_msLevel = -1;
			_spectrumName = "";
			_precursorMZ = -1;
			_64Bit = true;
			//_filterMinIntensity = 0;
			_spectraTIC = -1;
			_expectedPeaks = -1;
			_mzList.clear();
			_intensityList.clear();
		}
		MSABSpectrum current(_rtInSeconds, _msLevel, _precursorMZ, _spectraTIC, _spectrumName);

		//filter peaks
		auto highestPeak = *std::max_element(_intensityList.begin(), _intensityList.end());
		auto filterMinIntensity = highestPeak * 0.02;
		if (!_mzList.isEmpty() )
		{
			for (auto x = _mzList.count() - 1; x >= 0; x--)
			{
				if (_intensityList[x] < filterMinIntensity)
				{
					_mzList.removeAt(x);
					_intensityList.removeAt(x);
				}
			}
		}
		current.SetPeaks(_mzList, _intensityList);
		experiment.AddSpectrum(current);

		//clean slate afterwards
		_rtInSeconds = -1;
		_msLevel = -1;
		_spectrumName = "";
		_64Bit = true;
		_spectraTIC = -1;
		_expectedPeaks = -1;
		_mzList.clear();
		_intensityList.clear();
	}
}


