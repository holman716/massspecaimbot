#ifndef MSABMZMLREADER_H
#define MSABMZMLREADER_H

#include <QList>
#include <Utility/DataContainer/MSABExperiment.h>
#include <Utility/DataContainer/Spectra.h>

using namespace std;

namespace MSAB
{
	class mzMLReader
	{
	public:
		mzMLReader() { _isCanceled = false; }
		~mzMLReader() {}
		void CancelReader() { _isCanceled = true; }

		void Load_mzMLExperiment(MSABExperiment& experiment);
	private: 
		bool _isCanceled;
		void CompileSpectraInfo(MSABExperiment& experiment);
		QList<MSABSpectrum> _newSpectraMap; //RT * 10,000 key

		double _rtInMinutes;
		int _msLevel;
		double _precursorMZ;
		QString _spectrumName;
		//double _preccursorMZ;
		bool _64Bit;
		//double _filterMinIntensity; //base peak intensity * 0.02
		double _spectraTIC;
		int _expectedPeaks;

		bool _readingMZ;
		bool _readingIntensity;
		QList<double> _mzList;
		QList<double> _intensityList;
	};
	
}

#endif // MSABMZMLREADER_H