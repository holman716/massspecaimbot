#ifndef MSABNETCDFREADER_H
#define MSABNETCDFREADER_H

#include <QList>
#include <Utility/DataContainer/MSABExperiment.h>

using namespace std;

namespace MSAB
{
	class NetCDFReader
	{
	public:
		NetCDFReader() { isCanceled = false; }
		~NetCDFReader() {};

		void CancelReader() { isCanceled = true; }
		void LoadNetCDFExperiment(MSABExperiment& experiment);
	private:
		bool isCanceled;
		std::map<QString, std::vector<double>> GetNetCDFValues(QString filePath, std::vector <QString> varIDStrings);
		std::vector<double> GetNetCDFValue(QString filePath, QString varIDString); //not used right now
	};
	
}

#endif // MSABNETCDFREADER_H