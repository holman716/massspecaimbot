#ifndef MSABMZXMLREADER_H
#define MSABMZXMLREADER_H

#include <QList>
#include <Utility/DataContainer/MSABExperiment.h>
#include <Utility/DataContainer/Spectra.h>

using namespace std;

namespace MSAB
{
	class mzXMLReader
	{
	public:
		mzXMLReader() { _isCanceled = false; }
		~mzXMLReader() {}
		void CancelReader() { _isCanceled = true; }

		void Load_mzXMLExperiment(MSABExperiment& experiment);
	private: 
		bool _isCanceled;
		void CompileSpectraInfo(MSABExperiment& experiment);
		QList<MSABSpectrum> _newSpectraMap; //RT * 10,000 key

		double _rtInSeconds;
		int _msLevel;
		QString _spectrumName;
		double _precursorMZ;
		bool _64Bit;
		//double _filterMinIntensity; //base peak intensity * 0.02
		double _spectraTIC;
		int _expectedPeaks;

		QList<double> _mzList;
		QList<double> _intensityList;
	};
	
}

#endif // MSABMZXMLREADER_H