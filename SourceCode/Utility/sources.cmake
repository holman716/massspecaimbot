set(currentDirectory ${base_source_directory}/SourceCode/Utility)

set(sources_list
SpectraFileHandler.cpp
MethodFileHandler.cpp
BinaryHandler.cpp
)

set(sources_list_h
SpectraFileHandler.h
MethodFileHandler.h
BinaryHandler.h
)

set(sources)
foreach(i ${sources_list})
	list(APPEND sources ${currentDirectory}/${i})
endforeach(i)

set(sources_h)
foreach(i ${sources_list_h})
	list(APPEND sources_h ${currentDirectory}/${i})
endforeach(i)

### pass source file list to the upper instance
set(MSAB_sources ${MSAB_sources} ${sources} ${sources_h})
set(MSAB_sources_h ${MSAB_sources_h} ${sources_h})

### source group definition
#source_group("utility" FILES ${sources})
source_group("Source Files\\Utility" FILES ${sources})
source_group("Header Files\\Utility" FILES ${sources_h})

include(${base_source_directory}/SourceCode/Utility/DataContainer/sources.cmake)
include(${base_source_directory}/SourceCode/Utility/FileReader/sources.cmake)