#ifndef MSABSPECTRAFILEHANDLER_H
#define MSABSPECTRAFILEHANDLER_H

#include <QMainWindow>
#include <QLabel>
#include <QGridLayout>
#include <QPushButton>
#include <QListWidget>
#include <QProgressBar>
#include <QString>
#include <vector>
#include <Utility/DataContainer/MSABExperiment.h>
#include <Utility/FileReader/NetCDFReader.h>
#include <Utility/FileReader/mzMLReader.h>
#include <Utility/FileReader/mzXMLReader.h>
#include <QThread>
#include <mutex>

namespace MSAB {

	struct SpectraFileType
	{
		static const QString NETCDF;// = "NetCDF_File";
		static const QString MZML;// = "mzML_File";
		static const QString MZXML;// = "mzXML_File";
	};

	class SpectraFileWorker : public QObject
	{
		Q_OBJECT
			
	public:
		SpectraFileWorker();
		~SpectraFileWorker();
		void CancelOperation();
		bool getWorking() { return isWorking; };
		bool getCurrentId() { return currentRowID; };
		bool getCanceled();
	public slots:
		void LoadExperiment(MSABExperiment* targetExperiment, int rowID); //DoWork
		void ClearExperiment(MSABExperiment* targetExperiment, int rowID);
	public slots:
		void DoWork_Load();
		void DoWork_Clear();
	signals:
		void resultReady(int rowID);		
	private:
		bool isCanceled;
		QString _currentExperementType;
		NetCDFReader _netCDFReader;
		mzMLReader _mzMLReader;
		mzXMLReader _mzXMLReader;

		MSABExperiment* currentExperiment;
		int currentRowID;
		bool isWorking;
		
	};

	class SpectraFileHandler : public QWidget
	{
		Q_OBJECT

	public:
		SpectraFileHandler(QWidget *parent = nullptr);

		bool ExperimentIsLoaded(int rowID);
		MSABExperiment* GetExperiment(int rowID);

	public slots:
		void ShowFileLoadDialog();
		void clearDataFiles();
		void RequestExperiment(int rowID);
		void workerResultReady(int rowID);
		void rowSelected();
	signals:
		//void dep_ProcessComplete(std::vector<MSABExperiment>&);
		void ExperimentSelected(int rowID);
	private:
		QGridLayout* _fileAreaBoxLayout;
		QPushButton* _newFileButton;
		QPushButton* _clearFileButton;
		QListWidget* _fileListBox;
		QProgressBar* _pb;

		SpectraFileWorker* _fw;
		int _numFilesLoaded;
		int _experimentNeeded;

		QMap<int, MSABExperiment> _fileMap;

	};
}

#endif //MSABSPECTRAFILEHANDLER_H