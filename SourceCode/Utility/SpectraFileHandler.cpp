#include <Utility/DataContainer/MSABExperiment.h>
//#include <OpenMS/METADATA/Precursor.h>
//#include <OpenMS/FORMAT/MzMLFile.h>
//#include <OpenMS/CONCEPT/Exception.h>
#include <Utility/SpectraFileHandler.h>
#include <vector>
#include<QGridLayout>
#include <QThread>
#include <QFileDialog>
#include <QMessageBox>

using namespace std;

namespace MSAB
{
	const QString SpectraFileType::NETCDF = "NetCDF_File";
	const QString SpectraFileType::MZML = "mzML_File";
	const QString SpectraFileType::MZXML = "mzXML_File";

#pragma region SpectraFileHandler
	//std::mutex _readLock, _processLock;
	SpectraFileHandler::SpectraFileHandler(QWidget *parent) : QWidget(parent)
	{
		auto _fileAreaBoxLayout = new QGridLayout(this);
		_numFilesLoaded = 0;
		_experimentNeeded = -1;

		_newFileButton = new QPushButton;
		_newFileButton->setText("New");
		connect(_newFileButton, SIGNAL(clicked()), this, SLOT(ShowFileLoadDialog()));
		_fileAreaBoxLayout->addWidget(_newFileButton, 0, 0, Qt::AlignRight);
		_clearFileButton = new QPushButton;
		_clearFileButton->setText("Clear");
		connect(_clearFileButton, SIGNAL(clicked()), this, SLOT(clearDataFiles()));
		_fileAreaBoxLayout->addWidget(_clearFileButton, 0, 1, Qt::AlignLeft);
		_fileListBox = new QListWidget;
		_fileListBox->setMinimumWidth(200);
		connect(_fileListBox, SIGNAL(itemSelectionChanged()), this, SLOT(rowSelected()));
		_fileAreaBoxLayout->addWidget(_fileListBox, 1, 0, 1, 2);
		_pb = new QProgressBar;
		_pb->setMinimum(0);
		_pb->setMaximum(0);
		_pb->setAlignment(Qt::AlignCenter);
		_pb->setVisible(false);
		_fileAreaBoxLayout->addWidget(_pb, 2, 0, 1, 2);

		_fw = new SpectraFileWorker;
	}

	bool SpectraFileHandler::ExperimentIsLoaded(int rowID)
	{
		if (_fileMap.contains(rowID))
			return _fileMap[rowID].isLoaded;
		return false;

	}

	MSABExperiment* SpectraFileHandler::GetExperiment(int rowID)
	{
		MSABExperiment emptyData;
		if (!_fileMap.contains(rowID))
			return &emptyData;

		return &_fileMap[rowID];
	}

	void SpectraFileHandler::ShowFileLoadDialog()
	{
		QFileDialog dialog(this);
		dialog.setFileMode(QFileDialog::FileMode::ExistingFiles);
		dialog.setNameFilter(tr("MS Files (*.cdf *.mzml *.mzxml)"));
		dialog.setViewMode(QFileDialog::Detail);
		if (!dialog.exec())
			return;
		auto fileNames = dialog.selectedFiles();
		QStringList validFiles;
		foreach(auto file, fileNames)
		{
			bool valid = true;
			for(auto it = _fileMap.constBegin(); it != _fileMap.constEnd(); it++)
			{
				auto location = it->FileLocation;
				if (QString::compare(location,file,Qt::CaseInsensitive) == 0)
					valid = false;
			}
			if (valid)
				validFiles.push_back(file);
		}
		if (validFiles.count() == 0)
		{
			QMessageBox mb;
			mb.setText("All files have already been loaded.");
			QMessageBox::question(this, "No valid files", "All files have already been loaded.", QMessageBox::Ok);
			return;
		}
		int firstRow = -1;
		foreach(auto file, validFiles)
		{
			int rowNumber = _fileListBox->count();
			if (firstRow == -1)
				firstRow = rowNumber;
			_fileListBox->addItem(file);
			_fileListBox->item(rowNumber)->setTextColor(QColor::fromHsv(100, 0, 180));
			MSABExperiment newExp(file,rowNumber);
			_fileMap[rowNumber] = newExp;
		}
		if (validFiles.count()>0) //(_numFilesLoaded == 0 && _fileMap.count() > 0)
		{
			RequestExperiment(firstRow);
		}
	}

	void SpectraFileHandler::clearDataFiles()
	{
		auto reply = QMessageBox::question(this, "Clear data", "Clear data files from list?",
			QMessageBox::Yes | QMessageBox::No);
		if (reply == QMessageBox::Yes)
		{
			_fw->CancelOperation();
			_fileListBox->clear();
			_fileMap.clear();
			
			emit ExperimentSelected(-1);
		}
	}

	void SpectraFileHandler::RequestExperiment(int rowID)
	{
		if (!_fileMap.contains(rowID))
			return;
		if (_fw->getWorking())
		{
			if (_fw->getCurrentId() == rowID)
				return;
			else
				_fw->CancelOperation();
		}
		else
			_fw->deleteLater();
		_pb->setVisible(true);
		_fw = new SpectraFileWorker();
		connect(_fw, &SpectraFileWorker::resultReady, this, &SpectraFileHandler::workerResultReady);
		auto pointer = &_fileMap[rowID];
		_fw->LoadExperiment(pointer, rowID);
	}

	void SpectraFileHandler::workerResultReady(int rowID)
	{
		if (!_fileMap[rowID].isLoaded)
			return;
		_numFilesLoaded++;
		_fileListBox->item(rowID)->setTextColor(QColor::fromHsv(100, 0, 0));
		_pb->setVisible(false);
		if (_experimentNeeded == rowID)
		{
			_experimentNeeded = -1;
			emit ExperimentSelected(rowID);
		}
		if (_fileMap.count() == _numFilesLoaded)
			return;
		for (int x = rowID + 1; x != rowID; x++)
		{
			if (x >= _fileMap.count())
			{
				x = -1;
				continue;
			}
			if (_fileMap.constFind(x) == _fileMap.constEnd())
				throw new exception("Gap found in filemap data- unintended result");//continue; // why would there be a gap in the data?
			if (_fileMap[x].isLoaded)
				continue;
			RequestExperiment(x);
			return;
		}		
	}

	void MSAB::SpectraFileHandler::rowSelected()
	{
		auto selectedRow = _fileListBox->currentRow();
		if (_fileMap.find(selectedRow) == _fileMap.end())
			emit ExperimentSelected(-1);
		emit ExperimentSelected(selectedRow);
		if (!_fileMap[selectedRow].isLoaded)
		{
			_experimentNeeded = selectedRow;
			RequestExperiment(selectedRow);
		}			
	}

#pragma endregion SpectraFileHandler GUI functions here

#pragma region SpectraFileWorker

	SpectraFileWorker::SpectraFileWorker()
	{
		isWorking = false;
		currentRowID = -1;
		isCanceled = false;
	}
	SpectraFileWorker::~SpectraFileWorker()
	{

	}

	void SpectraFileWorker::LoadExperiment(MSABExperiment* targetExperiment, int rowID)
	{
		isWorking = true;
		isCanceled = false;
		currentExperiment = targetExperiment;
		currentRowID = rowID;
		QThread* workerThread = new QThread;
		moveToThread(workerThread);
		connect(workerThread, SIGNAL(finished()), this, SLOT(deleteLater()));
		connect(workerThread, SIGNAL(finished()), workerThread, SLOT(deleteLater()));
		//connect(this, &SpectraFileWorker::resultReady, this, &SpectraFileHandler::QueueFile);
		connect(workerThread, &QThread::started, this, &SpectraFileWorker::DoWork_Load);

		workerThread->start();
	}

	void SpectraFileWorker::DoWork_Load()
	{
		if (currentExperiment->FileLocation.endsWith("cdf"))
		{
			_currentExperementType = SpectraFileType::NETCDF;
			_netCDFReader.LoadNetCDFExperiment(*currentExperiment);
		}
		else if (currentExperiment->FileLocation.endsWith("mzML"))
		{
			_currentExperementType = SpectraFileType::MZML;
			_mzMLReader.Load_mzMLExperiment(*currentExperiment);
		}
		else if (currentExperiment->FileLocation.endsWith("mzXML"))
		{
			_currentExperementType = SpectraFileType::MZXML;
			_mzXMLReader.Load_mzXMLExperiment(*currentExperiment);
		}

		currentExperiment->ResetMZRT();
		currentExperiment->isLoaded = !isCanceled;
		if (isCanceled)
			DoWork_Clear();
		else
			emit resultReady(currentRowID);
		isWorking = false;
	}

	void SpectraFileWorker::ClearExperiment(MSABExperiment* targetExperiment, int rowID)
	{
		isWorking = true;
		isCanceled = false;
		currentExperiment = targetExperiment;
		currentRowID = rowID;
		QThread* workerThread = new QThread;
		connect(workerThread, SIGNAL(finished()), this, SLOT(deleteLater()));
		connect(workerThread, SIGNAL(finished()), workerThread, SLOT(deleteLater()));
		//connect(this, &SpectraFileWorker::resultReady, this, &SpectraFileHandler::QueueFile);
		connect(workerThread, &QThread::started, this, &SpectraFileWorker::DoWork_Load);

		workerThread->start();
	}

	void SpectraFileWorker::DoWork_Clear()
	{
		currentExperiment->isLoaded = false;
		currentExperiment->ClearSpectra();
		isWorking = false;
	}
	
	void MSAB::SpectraFileWorker::CancelOperation()
	{
		if (_currentExperementType == SpectraFileType::NETCDF)
			_netCDFReader.CancelReader();
		else if (_currentExperementType == SpectraFileType::MZML)
			_mzMLReader.CancelReader();
		else if (_currentExperementType == SpectraFileType::MZXML)
			_mzXMLReader.CancelReader();
		
		isCanceled = true;
	}

	bool MSAB::SpectraFileWorker::getCanceled()
	{
		return isCanceled;
	}

	
	
#pragma endregion SpectraFileWorker functions here

}