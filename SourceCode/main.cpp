#include <Dialog/IntegrationWindow.h>
#include <Dialog/BlankWindow.h>
#include <QApplication>

//int main(int argc, char *argv[])
//{
//    QApplication a(argc, argv);
//    OpenMS::BlankWindow w;
//    w.show();	
//
//    return a.exec();
//}

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, char*, int nShowCmd)
{
	int argc = 0;
	QApplication a(argc, 0);
	MSAB::IntegrationWindow w;
	//MSAB::BlankWindow w;
	w.showMaximized();
	//w.show();

	return a.exec();
}
