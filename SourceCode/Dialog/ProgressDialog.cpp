#include <Dialog/ProgressDialog.h>
#include <QGridLayout>

namespace MSAB
{
	ProgressDialog::ProgressDialog(QWidget *parent) :
		QMainWindow(parent)
	{
		QWidget* dummy_cw = new QWidget(this);
		setCentralWidget(dummy_cw);
		auto main_layout = new QGridLayout(dummy_cw);
		_mainLabel = new QLabel;
		_mainLabel->setText("Initializing");
		main_layout->addWidget(_mainLabel);
		_mainBar = new QProgressBar;
		_mainBar->setMinimum(0);
		_mainBar->setMaximum(0);
		_mainBar->setAlignment(Qt::AlignCenter);
		main_layout->addWidget(_mainBar);
	}

	void ProgressDialog::UpdateDialog(QString text, int current, int max)
	{
		if (!text.isEmpty())
			_mainLabel->setText(text);
		if (max >= 0)
			_mainBar->setMaximum(max);
		if (current <= max)
			_mainBar->setValue(current);
	}
}