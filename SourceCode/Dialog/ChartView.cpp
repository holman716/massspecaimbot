#include <Dialog/ChartView.h>
#include <QLineSeries>
#include <QValueAxis>

namespace MSAB
{
	MSAB::ChartView::ChartView(QtCharts::QChart* chart, QWidget *parent)
		: QChartView(chart, parent)
	{
		setDragMode(QGraphicsView::NoDrag);
		this->setMouseTracking(true);
		_mainChart = chart;

		_scene = new QGraphicsScene(0, 0, 25, height());
		//QPen pen(Qt::red, 2);
		//_scene->addLine(0, 0, 0, height()-2, pen);
		_view = new QGraphicsView(_scene,this);
		_view-> setFrameStyle(QFrame::NoFrame);
		_view->setStyleSheet("background: transparent");
		_view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
		_view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
		_view->setGeometry(0, 0, 2, height());
		_view->setAttribute(Qt::WA_TransparentForMouseEvents);
		//_view->show();

		//_xAxis = xAxis;
		//_yAxis = yAxis;
		_xAxis = (QtCharts::QValueAxis*)(_mainChart->axisX());
		_yAxis = (QtCharts::QValueAxis*)(_mainChart->axisY());
		MaxShownXValue = -1;
		MaxShownXValue = -1;
		
	}


	void MSAB::ChartView::mouseReleaseEvent(QMouseEvent *event)
	{
		if (event->button() != Qt::RightButton)
		{
			QChartView::mouseReleaseEvent(event);
		}
		handleReleaseEvent(event->button() == Qt::RightButton);
	}

	void ChartView::mousePressEvent(QMouseEvent *event)
	{
		bool eventTrigger = true;
		if (event->button() == Qt::MiddleButton)
		{
			QApplication::setOverrideCursor(QCursor(Qt::SizeAllCursor));
			m_lastMousePos = event->pos();
			event->accept();
		}
		else if (event->button() == Qt::LeftButton)
		{
			auto xval = _mainChart->mapToValue(event->pos()).x();
			auto inScene = _mainChart->plotArea();
			auto inChart = _mainChart->mapFromScene(inScene);
			auto inChartRect = inChart.boundingRect();
			auto min = _mainChart->mapToValue(inChartRect.topLeft()).x();
			auto max = _mainChart->mapToValue(inChartRect.bottomRight()).x();
			if (xval > min || xval < max)
				emit ScanTimeSelected(xval*60);
		}

		if (eventTrigger)
			QChartView::mousePressEvent(event);
	}
	
	void ChartView::mouseMoveEvent(QMouseEvent *event)
	{
		if (event->buttons() & Qt::MiddleButton)
		{
			auto dPos = event->pos() - m_lastMousePos;
			chart()->scroll(-dPos.x(), 0);

			m_lastMousePos = event->pos();
			event->accept();

			QApplication::restoreOverrideCursor();
			auto xval = _mainChart->mapToValue(event->pos()).x();
			auto inScene = _mainChart->plotArea();
			auto inChart = _mainChart->mapFromScene(inScene);
			auto inChartRect = inChart.boundingRect();
			auto min = _mainChart->mapToValue(inChartRect.topLeft()).x();
			auto max = _mainChart->mapToValue(inChartRect.bottomRight()).x();
			RescaleAxis(max, min, MaxShownYValue);
		}
		else if (!event->buttons())
		{
			auto xval = _mainChart->mapToValue(event->pos()).x();
			auto inScene = _mainChart->plotArea();
			auto inChart = _mainChart->mapFromScene(inScene);
			auto inChartRect = inChart.boundingRect();
			auto min = _mainChart->mapToValue(inChartRect.topLeft()).x();
			auto max = _mainChart->mapToValue(inChartRect.bottomRight()).x();
			if (xval == 0 || xval<min || xval>max)
			{
				_view->hide();
			}
			else
			{
				auto testString = QString::number(xval, 'f', 4);
				QPen pen(Qt::red, 2);
				_scene->clear();
				_scene->addLine(0, 0, 0, inChartRect.height(), pen);
				QGraphicsTextItem *text = _scene->addText(testString, QFont("sans", 12));
				text->setPos(5, 25);
				text->setDefaultTextColor(Qt::red);
				_view->setGeometry(event->pos().x(), inChartRect.topLeft().y(), text->boundingRect().width() + 2, inChartRect.height());
				_view->setSceneRect(0, 0, _view->frameSize().width(), _view->frameSize().height());
				_view->show();
			}
		}
		else
		{
			_view->hide();
		}

		QChartView::mouseMoveEvent(event);
	}

	void ChartView::leaveEvent(QEvent * event)
	{
		_view->hide();
	}

	void MSAB::ChartView::handleReleaseEvent(bool rightButton)
	{
		auto xAxis = (QtCharts::QValueAxis*)(_mainChart->axisX());
		xAxis->setTickType(QtCharts::QValueAxis::TickType::TicksFixed);
		auto yAxis = (QtCharts::QValueAxis*)(_mainChart->axisY());
		yAxis->setTickType(QtCharts::QValueAxis::TickType::TicksFixed);
		if (rightButton)
		{			
			chart()->zoomReset();
		}

		qreal maxY = 0;
		_mainChart->axisY()->setRange(0, MaxStoredYValue * 1.1);
		foreach(auto it, _mainChart->series())
		{
			//auto inScene = _mainChart->plotArea();
			//auto inChart = _mainChart->mapFromScene(inScene);
			//auto inChartRect = inChart.boundingRect();
			auto xAxis = (QtCharts::QValueAxis*)(_mainChart->axisX());
			auto xMin = xAxis->min();
			auto xMax = xAxis->max();
			auto const points = ((QtCharts::QLineSeries*)it)->pointsVector();
			foreach(auto point, points)
			{
				if (point.y() == 0 || point.x() < xMin || point.x() > xMax) continue;
				if (point.y() > maxY)
					maxY = point.y();
			}
		}
		//_mainChart->axisY()->setRange(0, maxY*1.1);
		auto inScene = _mainChart->plotArea();
		auto inChart = _mainChart->mapFromScene(inScene);
		auto inChartRect = inChart.boundingRect();
		auto minX = _mainChart->mapToValue(inChartRect.topLeft()).x();
		auto maxX = _mainChart->mapToValue(inChartRect.bottomRight()).x();
		RescaleAxis(maxX, minX, maxY*1.1);
	}

	void MSAB::ChartView::RescaleAxis(qreal maxX, qreal minX, qreal maxY, qreal minY)
	{
		_xAxis->setTickType(QtCharts::QValueAxis::TickType::TicksFixed);
		_yAxis->setTickType(QtCharts::QValueAxis::TickType::TicksFixed);
		MinShownXValue = minX;
		MaxShownXValue = maxX;
		MaxShownYValue = maxY;
		_mainChart->axisY()->setRange(minY, maxY);
		_mainChart->axisX()->setRange(minX, maxX);

		//x axis
		auto range = maxX - minX;
		auto rawLogMagnitude = log10(range);
		auto ceilLogMagnitude = (int)ceil(rawLogMagnitude);
		auto baseInterval = pow(10, ceilLogMagnitude - 1);
		int modifier = 1;
		auto estimatedTicks = floor(range / baseInterval);
		if (estimatedTicks < 5)
		{
			modifier = 2;
			estimatedTicks = floor(range / (baseInterval / modifier));
			if (estimatedTicks < 5)
			{
				modifier = 5;
				estimatedTicks = floor(range / (baseInterval / modifier));
				if (estimatedTicks < 5)
				{
					modifier = 10;
					estimatedTicks = floor(range / (baseInterval / modifier));
				}
			}
		}
		auto tickInterval = baseInterval / modifier;

		auto start = minX / baseInterval;
		start = floor(start);
		start *= baseInterval;
		while (start < minX)
			start += tickInterval;

		_xAxis->setTickType(QtCharts::QValueAxis::TickType::TicksDynamic);
		_xAxis->setTickInterval(tickInterval);
		_xAxis->setTickAnchor(start);

		if (tickInterval < 1)
		{
			auto decimalPlaces = abs(ceilLogMagnitude) + (modifier > 1 ? 2 : 1)
				- (baseInterval == 1 ? 2 : 0);
			_xAxis->setLabelFormat("%." + QString::number(decimalPlaces) + "f");
		}
		else
			_xAxis->setLabelFormat("%i");


		//y axis
		range = maxY - minY;
		rawLogMagnitude = log10(range);
		ceilLogMagnitude = (int)ceil(rawLogMagnitude);
		baseInterval = pow(10, ceilLogMagnitude - 1);
		modifier = 1;
		estimatedTicks = floor(range / baseInterval);
		if (estimatedTicks < 3)
		{
			modifier = 2;
			estimatedTicks = floor(range / (baseInterval / modifier));
			if (estimatedTicks < 3)
			{
				modifier = 5;
				estimatedTicks = floor(range / (baseInterval / modifier));
				if (estimatedTicks < 3)
				{
					modifier = 10;
					estimatedTicks = floor(range / (baseInterval / modifier));
				}
			}
		}
		else if (estimatedTicks > 6)
		{
			modifier = 5;
			baseInterval = pow(10, ceilLogMagnitude);
			estimatedTicks = floor(range / (baseInterval / modifier));
		}
		tickInterval = baseInterval / modifier;

		start = minY / baseInterval;
		start = floor(start);
		start *= baseInterval;
		while (start < minY)
			start += tickInterval;

		_yAxis->setTickType(QtCharts::QValueAxis::TickType::TicksDynamic);
		_yAxis->setTickInterval(tickInterval);
		_yAxis->setTickAnchor(start);

		if (tickInterval < 1)
		{
			auto decimalPlaces = abs(ceilLogMagnitude) + (modifier > 1 ? 2 : 1)
				- (baseInterval == 1 ? 2 : 0);
			_yAxis->setLabelFormat("%." + QString::number(decimalPlaces) + "f");
		}
		else
			_yAxis->setLabelFormat("%.0f");

		//emit ViewChanged(min, max);
	}

	void MSAB::ChartView::ResetZoom()
	{
		handleReleaseEvent(true);
	}

}