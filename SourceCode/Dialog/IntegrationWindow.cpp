#include <Dialog/IntegrationWindow.h>
#include <Dialog/TICWidget.h>
#include <Utility/SpectraFileHandler.h>

#include <vector>
//#include <QtGUI>
//#include <QWidget>

//#include <OpenMS/VISUAL/SpectrumCanvas.h>
//#include <OpenMS/VISUAL/SpectrumWidget.h>
//#include <OpenMS/VISUAL/Spectrum1DWidget.h>
//#include <OpenMS/VISUAL/AxisWidget.h>

namespace MSAB {

	IntegrationWindow::IntegrationWindow(QWidget *parent) :
		QMainWindow(parent)
	{
		//main window portion
		setWindowTitle("Mass Spec Aimbot");
		setMinimumSize(400, 400);
		
		
		//central layout 
		QWidget* dummy_cw = new QWidget(this);
		setCentralWidget(dummy_cw);
		//dummy_cw->hide();
		_main_layout = new QGridLayout(dummy_cw);


		////fileMenu
		//_fileMenu = menuBar()->addMenu("File");
		//_fileMenu_loadData =_fileMenu->addAction("Load data...");
		//connect(_fileMenu_loadData, SIGNAL(triggered()), this, SLOT(loadDataFiles()));
		//_fileMenu_loadMethod = _fileMenu->addAction("Load method...");
		//_fileMenu_saveMethod = _fileMenu->addAction("Save method...");
		//_fileMenu_exportData = _fileMenu->addAction("Export data to workspace");
		//_fileMenu_importData = _fileMenu->addAction("Import data from workspace");


		////optionsMenu
		//_optionsMenu = menuBar()->addMenu("Options");
		//_optionsMenu_peakOptions = _optionsMenu->addAction("Set find peaks options...");
		//_optionsMenu_setPeakParameters = _optionsMenu->addAction("Manually set peak parameters...");
		//_optionsMenu_calculateIon = _optionsMenu->addAction("Calculate baseline for each ion");
		//_optionsMenu_calculateIon->setCheckable(true);
		//_optionsMenu_calculateIon->setChecked(true);
		//_optionMenu_correctionBool = _optionsMenu->addAction("Natural abundance correction");
		//_optionMenu_correctionBool->setCheckable(true);
		//_optionMenu_correctionBool->setChecked(true);
		//_optionsMenuInt = _optionsMenu->addMenu("Integration outputs...");
		//_optionsMenuInt_csv = _optionsMenuInt->addAction(".csv");
		//_optionsMenuInt_mat =_optionsMenuInt->addAction(".mat");


		//File area
		//_experimentDock = new QDockWidget("Spectra Files");
		//_experimentDock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea
		//	| Qt::BottomDockWidgetArea | Qt::TopDockWidgetArea);
		//addDockWidget(Qt::LeftDockWidgetArea, _experimentDock);
		_fhWidget = new SpectraFileHandler();
		connect(_fhWidget, &SpectraFileHandler::ExperimentSelected, this, &IntegrationWindow::showSelectedExperiment);
		_main_layout->addWidget(_fhWidget);
		

		//Spectra view (top right)
		_msDock = new QDockWidget("Experiment View");
		_msDock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea
			| Qt::BottomDockWidgetArea | Qt::TopDockWidgetArea);
		_msDock->setFeatures(_methodDock->DockWidgetMovable);
		

		//scanView (bottom)
		_scanDock = new QDockWidget("Scan View");
		_scanDock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea
			| Qt::BottomDockWidgetArea | Qt::TopDockWidgetArea);
		_scanDock->setFeatures(_methodDock->DockWidgetMovable);
		addDockWidget(Qt::RightDockWidgetArea, _msDock);
		auto oldMinWidth = _msDock->minimumWidth();
		
		//set initial widths, has to be done this way or else it won't stick for some reason
		_msDock->setMinimumWidth(QApplication::desktop()->screenGeometry().width()*0.75);
		_msDock->setMaximumWidth(QApplication::desktop()->screenGeometry().width()*0.9);
		setTabPosition(Qt::RightDockWidgetArea| Qt::BottomDockWidgetArea|Qt::LeftDockWidgetArea, QTabWidget::North);		
		QTimer::singleShot(5000, this, [=]() {_msDock->setMinimumWidth(oldMinWidth); });
		
		//Actually populate the docks
		_spectrumWidget = new TICSpectrumWidget();
		_msDock->setWidget(_spectrumWidget);
		_scanWidget = new TICSpectrumWidget();
		_scanWidget->RefreshData(NULL, false);
		_scanDock->setWidget(_scanWidget);
		connect(_spectrumWidget, &TICSpectrumWidget::SpectraSelected, this, &IntegrationWindow::SetSpectra);
		
		

		//bottom panel
		_methodWidget = new MethodWidget();
		connect(_methodWidget, &MethodWidget::ModelChanged, _spectrumWidget, &TICSpectrumWidget::SetCurrentModel );
		connect(_methodWidget, &MethodWidget::IndicateNewTolerance, _spectrumWidget, &TICSpectrumWidget::SetInstrumentTolerance);
		//connect(_methodWidget, &MethodWidget::CallTableRefresh, _spectrumWidget, &TICSpectrumWidget::RefreshAllSeries );

		_methodDock = new QDockWidget("Methods");
		_methodDock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea
			| Qt::BottomDockWidgetArea | Qt::TopDockWidgetArea);		
		_methodDock->setFeatures(_methodDock->DockWidgetMovable);
		_methodDock->setWidget(_methodWidget);
		addDockWidget(Qt::BottomDockWidgetArea, _methodDock);
		tabifyDockWidget(_methodDock, _scanDock);
		_methodDock->raise();
		resizeDocks({ _methodDock }, { (int)(QApplication::desktop()->screenGeometry().height()*0.35) },Qt::Vertical);

		////other globals
		//_fh = new SpectraFileHandler(this);
		//connect(_fh, &SpectraFileHandler::dep_ProcessComplete,
		//	this, &IntegrationWindow::loadDataFilesFromList);
	}

	IntegrationWindow::~IntegrationWindow()
	{

	}

	void IntegrationWindow::showSelectedExperiment(int rowId)
	{
		if (rowId < 0 || !_fhWidget->ExperimentIsLoaded(rowId))
		{
			//MSABExperiment emptyData;			
			_spectrumWidget->RefreshData(nullptr);
			_scanWidget->RefreshData(nullptr);
			_scanDock->setWindowTitle("Scan View");
			if (!_fhWidget->ExperimentIsLoaded(rowId))
				_msDock->setWindowTitle("Loading...");
			else
				_msDock->setWindowTitle("Experiment View");
			return;
			
		}
		
		_msDock->setWindowTitle("Experiment View");
		auto tmpData = _fhWidget->GetExperiment(rowId);
		_spectrumWidget->RefreshData(tmpData);
		_scanWidget->RefreshData(tmpData, false);
		_scanDock->setWindowTitle("Scan View");

	}

	void IntegrationWindow::SetSpectra(int scanNumber)
	{
		QString scanText = _scanWidget->SetSpectra(scanNumber);
		
		//auto fileLocation = _fileListBox->selectedItems()[0]->text();
		//auto tmpData = _experimentMap[fileLocation];
		//OpenMS::MSSpectrum scan = tmpData->getSpectra()[scanNumber];


		_scanDock->setWindowTitle(scanText);
		//_topRightTabGroup->setTabText(1, QString("Scan: %1 minutes").arg(QString::number(scan.getRT() / 60)));
	}
	  
}