#ifndef MSABMETHODWIDGET_H
#define MSABMETHODWIDGET_H

#include <QtWidgets>
#include <Utility/DataContainer/MethodTableModel.h>
#include <Dialog/NewMethodWindow.h>

namespace MSAB
{
	class MethodWidget : public QWidget
	{
		Q_OBJECT

	public:
		MethodWidget(QWidget * parent = nullptr);
		~MethodWidget() override;

		QMap<QString, MassSpecMethodModel*> _methodMap;

	protected slots:
		void addMethods();
		void editMethod();
		void addIon();
		void editIon();
		void ToleranceChanged();
		void CurrentMethodUpdated();
	signals:
		void ModelChanged(MSAB::MassSpecMethodModel* currentModel);
		//void CallTableRefresh();
		void IndicateNewTolerance(double tol);
	private:
		QSplitter* _mainLayout;
		QGridLayout* _leftLayout;
		QLabel* _methodLabel;
		QComboBox* _methodBox;
		QPushButton* _newMethodButton;
		QPushButton* _editMethodButton;
		double _currentSetTolerance;
		double _lastSeenTolerance;
		QLabel* _toleranceLabel;
		QDoubleSpinBox* _toleranceBox;
		QPushButton* _toleranceButton;

		QGridLayout* _rightLayout;
		QPushButton* _newIonButton;
		QPushButton* _editIonButton;
				
		MassSpecMethodModel* _currentMethodModel;
		QTableView* _methodQTV;

		

		bool double_equals(double a, double b, double epsilon = 0.00001)
		{
			return std::abs(a - b) < epsilon;
		}
	};
}

#endif //MSABMETHODWIDGET_H