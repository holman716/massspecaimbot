#ifndef INTEGRATIONWINDOW_H
#define INTEGRATIONWINDOW_H

#include <QMainWindow>


#include <Utility/DataContainer/MethodTableModel.h>
#include <Dialog/TICWidget.h>
#include <Dialog/MethodWidget.h>
#include <Utility/SpectraFileHandler.h>
#include <Utility/DataContainer/MSABExperiment.h>

namespace MSAB
{

	//namespace Ui {
	//	class MainWindow;
	//}

	class IntegrationWindow : public QMainWindow
	{
		Q_OBJECT

	public:
		explicit IntegrationWindow(QWidget *parent = nullptr);
		~IntegrationWindow();

	protected slots:
		void showSelectedExperiment(int rowId);
		void SetSpectra(int scanNumber);

	protected:
		////Main area
		QGridLayout* _main_layout;

		//menu bar
		QMenu* _fileMenu;
		QAction* _fileMenu_loadData;
		QAction* _fileMenu_loadMethod;
		QAction* _fileMenu_saveMethod;
		QAction* _fileMenu_exportData;
		QAction* _fileMenu_importData;
		QMenu* _optionsMenu;
		QAction* _optionsMenu_peakOptions;
		QAction* _optionsMenu_setPeakParameters;
		QAction* _optionsMenu_calculateIon;
		QAction* _optionMenu_correctionBool;
		QMenu* _optionsMenuInt;
		QAction* _optionsMenuInt_csv;
		QAction* _optionsMenuInt_mat;

		//top left (files)
		QDockWidget* _experimentDock;
		SpectraFileHandler* _fhWidget;

		//top right (chart)
		QDockWidget* _msDock;
		TICSpectrumWidget* _spectrumWidget;
		
		//bottom half
		QDockWidget* _methodDock;
		MethodWidget* _methodWidget;
		QDockWidget* _scanDock;
		TICSpectrumWidget* _scanWidget;

	private:
		map<QString, std::shared_ptr<MSABExperiment>> _experimentMap;

	};
}

#endif // INTEGRATIONWINDOW_H