#pragma once

#include <QtWidgets>
#include <QChart>
#include <QChartView>
#include <QValueAxis>
#include <Utility/DataContainer/MSABExperiment.h>

namespace MSAB
{
	class ChartView : public QtCharts::QChartView
	{
		Q_OBJECT

	public:
		ChartView(QtCharts::QChart* chart, QWidget *parent = 0);
		qreal MaxStoredYValue;
		qreal MaxShownYValue;
		qreal MinShownXValue;
		qreal MaxShownXValue;

		QtCharts::QChart* _mainChart;
		QtCharts::QValueAxis* _xAxis;
		QtCharts::QValueAxis* _yAxis;

		void RescaleAxis(qreal maxX, qreal minX, qreal maxY, qreal minY = 0);
		void ResetZoom();

	
	signals:
		void ScanTimeSelected(qreal scanTime);
		//void ViewChanged(qreal min, qreal max);

	protected:

		virtual void mouseReleaseEvent(QMouseEvent *event) override;
		virtual void mousePressEvent(QMouseEvent *event) override;
		virtual void mouseMoveEvent(QMouseEvent *event) override;
		virtual void leaveEvent(QEvent * event) override;

	private:
		QPointF m_lastMousePos;
		
		QGraphicsView* _view;
		QGraphicsScene* _scene;
		

		void handleReleaseEvent(bool rightButton);
		
	};
}