#ifndef MSABTICWIDGET_H
#define MSABTICWIDGET_H

#include <QtWidgets>
#include <QChart>
#include <QChartView>
#include <QValueAxis>
#include <QLineSeries>
#include <Dialog/ChartView.h>
#include <Utility/DataContainer/MSABExperiment.h>
#include <Utility/DataContainer/MethodTableModel.h>

namespace MSAB
{
	class TICSpectrumWidget : public QWidget
	{
		Q_OBJECT

	public:
		TICSpectrumWidget(QWidget * parent = nullptr, QString xTitle = "RT", QString yTitle = "Intensity");
		~TICSpectrumWidget() override;
		void RefreshData(MSABExperiment* newData, bool show = true);
	public slots:
		QString SetSpectra(int scanNumber, int level = 1);
		void ForwardSelectedSpectra(qreal scanTime);
		//void ViewChanged(qreal min, qreal max);
		//void ViewChanged(qreal min, qreal max,QtCharts::QValueAxis* _axisX);
		void RefreshAllSeries();
		void SetCurrentModel(MSAB::MassSpecMethodModel* newModel);
		void SetInstrumentTolerance(double tol);
		void ShowCompositeSpectra();
	signals:
		void SpectraSelected(int scanNumber);
	private:
		double _tolerance;
		//QMap<QString, MassSpecMethodIon*> _ionMap; //access via chemical symbol
		MSAB::MassSpecMethodModel* _currentModel;
		QtCharts::QLineSeries* GetSpectraSeries(double lowerMass, double upperMass, QString name, int msLevel = 1, double precursorMassMin = -1, double precursorMassMax = -1);
		void RefreshTICChart();
		QList<MSABPeak> GetCompositePeaksInRange(qreal min, qreal max);

		QLabel* _precursorLabel;
		QPushButton* _actionButton;
		QMenu* _actionMenu;
		//QList<QAction*> _actionItems;
		QPushButton* _exportButton;
		QMenu* _exportMenu;
		//QList<QAction*> _exportItems;

		QGridLayout* _mainLayout;
		//QtCharts::QChart* _mainChart;
		//QtCharts::QValueAxis* _axisX;
		//QtCharts::QValueAxis* _axisY;
		ChartView* _chartView;
		MSABExperiment* _dataSource;
		int _currentScanId;
		bool _showTIC;
	};
}

#endif //MSABTICWIDGET_H