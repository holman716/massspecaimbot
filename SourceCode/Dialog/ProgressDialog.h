#ifndef PROGRESSDIALOG_H
#define PROGRESSDIALOG_H

#include <QMainWindow>
#include <QLabel>
#include <QProgressBar>

namespace MSAB
{

	//namespace Ui {
	//	class BlankWindow;
	//}

	class ProgressDialog : public QMainWindow
	{
		Q_OBJECT

	public:
		explicit ProgressDialog(QWidget *parent = nullptr);
		void UpdateDialog(QString text, int current, int max);
		
	private:
		QLabel* _mainLabel;
		QProgressBar* _mainBar;
		//Spectrum1DWidget* _svw;
	};
}

#endif //PROGRESSDIALOG_H