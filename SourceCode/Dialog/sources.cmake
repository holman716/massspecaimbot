set(directory ${base_source_directory}/SourceCode/Dialog)

set(sources_list
IntegrationWindow.cpp
BlankWindow.cpp
TICWidget.cpp
MethodWidget.cpp
NewMethodWindow.cpp
ChartView.cpp
ProgressDialog.cpp
)

set(sources_list_h
IntegrationWindow.h
BlankWindow.h
TICWidget.h
MethodWidget.h
NewMethodWindow.h
ChartView.h
ProgressDialog.h
)

set(sources)
foreach(i ${sources_list})
	list(APPEND sources ${directory}/${i})
endforeach(i)

set(sources_h)
foreach(i ${sources_list_h})
	list(APPEND sources_h ${directory}/${i})
endforeach(i)

### pass source file list to the upper instance
set(MSAB_sources ${MSAB_sources} ${sources} ${sources_h})
set(MSAB_sources_h ${MSAB_sources_h} ${sources_h})

### source group definition
#source_group("dialog" FILES ${sources})
source_group("Source Files\\Dialog" FILES ${sources})
source_group("Header Files\\Dialog" FILES ${sources_h})