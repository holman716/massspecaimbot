#ifndef MSABCALLOUT_H
#define MSABCALLOUT_H

#include <QtWidgets>

namespace MSAB
{

	class Callout : public QGraphicsView
	{
	public:
		Callout(QWidget * parent = (QWidget*)nullptr);

	protected:
		void mousePressEvent(QGraphicsSceneMouseEvent *event);
		void mouseMoveEvent(QGraphicsSceneMouseEvent *event);

	private:
		QGraphicsScene* _scene;
	};
}
#endif // MSABCALLOUT_H
