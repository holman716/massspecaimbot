#include <Dialog/NewMethodWindow.h>
#include <QtGUI>
#include <QWidget>
#include <Utility/DataContainer/Chemestry.h>
#include <Utility/MethodFileHandler.h>


namespace MSAB {

	//class Spectrum1DWidget;

	
	
	NewMethodWindow::NewMethodWindow(WindowMode mode, MassSpecMethodModel* inputModel, int rowToLoad, QWidget *parent) :
		QDialog(parent)
	{
		//main window portion
		_currentMode = mode;
		if (inputModel)
			_oldModel = inputModel;
		switch (mode)
		{
			case WindowMode::NewMethod:
				setWindowTitle("New Method");
				resize(375, 384); //WithPrecursor(375, 410);//Before(375, 365);
				break;
			case WindowMode::EditMethod:
				setWindowTitle("Edit Method");
				resize(375, 185);
				break;
			case WindowMode::NewIon:
				setWindowTitle("New Ion");
				resize(375, 324);
				break;
			case WindowMode::EditIon:
				setWindowTitle("Edit Ion");
				resize(375, 354);
				break;
		}	
				
		//central layout 
		auto main_layout = new QVBoxLayout(this);
		
		//top button
		if (_currentMode == WindowMode::NewMethod)
		{
			auto loadButton = new QPushButton("Load from File");
			loadButton->setFocusPolicy(Qt::NoFocus);
			connect(loadButton, &QPushButton::clicked, this, &NewMethodWindow::LoadFromFile);
			main_layout->addWidget(loadButton, 0, Qt::AlignRight);
		}
		else if (_currentMode == WindowMode::EditMethod || _currentMode == WindowMode::EditIon)
		{
			auto deleteButton = new QPushButton("Delete");
			deleteButton->setFocusPolicy(Qt::NoFocus);
			QString qss = QString("background-color: #FF8082");
			deleteButton->setStyleSheet(qss);
			//QPalette warningPalette;
			//warningPalette.setColor(QPalette::Base, QColor::fromHsv(360, 50, 100));
			connect(deleteButton, &QPushButton::clicked, this, [=]() {
				auto questionResult = QMessageBox::question(this, "Delete?",
					"Are you sure you want to delete this?", QMessageBox::Yes| QMessageBox::No);
				if (questionResult == QMessageBox::Yes)
					this->accept(); 
			});
			main_layout->addWidget(deleteButton, 0, Qt::AlignLeft);
		}
		
		
		//Main section
		auto dummyPanel = new QFrame;
		dummyPanel->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
		auto bottomLayout = new QGridLayout(dummyPanel);
		bottomLayout->setSizeConstraint(QLayout::SetNoConstraint);
		main_layout->addWidget(dummyPanel, 1);

		//method name
		_methodNameBox = new QLineEdit;				
		if (_currentMode == WindowMode::NewMethod || _currentMode == WindowMode::EditMethod)
		{
			auto methodNameLabel = new QLabel("Method Name:");
			bottomLayout->addWidget(methodNameLabel, 0, 0, Qt::AlignRight);

			auto tempSP = _methodNameBox->sizePolicy();
			tempSP.setHorizontalStretch(1);
			_methodNameBox->setSizePolicy(tempSP);
			bottomLayout->addWidget(_methodNameBox, 0, 1);
			bottomLayout->addItem(new QSpacerItem(25, 25), 0, 2);
			if (_currentMode == WindowMode::EditMethod && inputModel != NULL)
				_methodNameBox->setText(inputModel->methodName);
		}
		
		//RT
		_expetedRTBox = new QSpinBox;
		if (_currentMode == WindowMode::NewMethod || _currentMode == WindowMode::EditMethod)
		{
			auto expetedRTLabel = new QLabel("Expected RT (minutes):");
			bottomLayout->addWidget(expetedRTLabel, 1, 0, Qt::AlignRight);

			_expetedRTBox->setMinimum(0);
			bottomLayout->addWidget(_expetedRTBox, 1, 1);
			if (inputModel != NULL)
				_expetedRTBox->setValue(inputModel->expectedRT);
		}

		//ms Level
		_msLevelBox = new QSpinBox;
		_lastMSLevel = 1;
		_msLevelBox->setValue(1);
		_msLevelBox->setMinimum(1);
		_msLevelBox->setMaximum(2); //no MS[n>2] data to test; playing it safe for now
		if (_currentMode != WindowMode::EditMethod)
		{
			auto msLevelLabel = new QLabel("MS Level:");
			bottomLayout->addWidget(msLevelLabel, 2, 0, Qt::AlignRight);

			bottomLayout->addWidget(_msLevelBox, 2, 1);
			if (inputModel != NULL && inputModel->methodIonCount() > 0)
			{
				if (rowToLoad < 1 || rowToLoad >= inputModel->methodIonCount())
					_msLevelBox->setValue(inputModel->getRawMethodIon(0)->msLevel);
				else
					_msLevelBox->setValue(inputModel->getRawMethodIon(rowToLoad)->msLevel);
			}

			connect(_msLevelBox, qOverload<int>(&QSpinBox::valueChanged), this, [=](int x) {
				double sizeChange = (x - _lastMSLevel) * 26;
				double newHeight = this->size().height() + sizeChange;
				_precursorLabel->setVisible(x > 1);
				_precursorBox->setVisible(x > 1);
				this->resize(this->size().width(), newHeight);
				
				_lastMSLevel = x;

			});
		}

		//precursor
		_precursorLabel = new QLabel("Precursor M/Z:");
		_precursorBox = new QLineEdit;
		if (_currentMode != WindowMode::EditMethod)
		{
			bottomLayout->addWidget(_precursorLabel, 3, 0, Qt::AlignRight);
			bottomLayout->addWidget(_precursorBox, 3, 1);
			_precursorLabel->setVisible(_msLevelBox->value() > 1);
			_precursorBox->setVisible(_msLevelBox->value() > 1);
			if (inputModel != NULL && inputModel->methodIonCount() > 0)
			{
				QString newValue = "";
				if (rowToLoad < 1 || rowToLoad >= inputModel->methodIonCount())
					_precursorBox->setText(QString::number(inputModel->getRawMethodIon(0)->precursorMZ));
				else
					_precursorBox->setText(QString::number(inputModel->getRawMethodIon(rowToLoad)->precursorMZ));
			}
		}
		
		//ion formula
		_ionFormulaBox = new QLineEdit;
		if (_currentMode != WindowMode::EditMethod)
		{
			auto ionFormulaLabel = new QLabel("Base Ion Formula:");
			bottomLayout->addWidget(ionFormulaLabel, 4, 0, Qt::AlignRight);

			bottomLayout->addWidget(_ionFormulaBox, 4, 1);
			if (inputModel != NULL && inputModel->methodIonCount() > 0)
			{
				if (rowToLoad < 1 || rowToLoad >= inputModel->methodIonCount())
					_ionFormulaBox->setText(inputModel->getRawMethodIon(0)->formula.IsCustom()?
						QString::number(inputModel->getRawMethodIon(0)->formula.TotalMass()) :
						inputModel->getRawMethodIon(0)->formula.FormulaString());
				else
					_ionFormulaBox->setText(inputModel->getRawMethodIon(rowToLoad)->formula.IsCustom() ?
						QString::number(inputModel->getRawMethodIon(rowToLoad)->formula.TotalMass()) :
						inputModel->getRawMethodIon(rowToLoad)->formula.FormulaString());
			}

			auto ionInfoLabel = new QLabel("(M/Z can be used if the formula is unknown)");
			ionInfoLabel->setStyleSheet("font-size: 9px;");
			bottomLayout->addWidget(ionInfoLabel, 5, 1);
		}
		
		//relative intensity
		_relIntensityBox = new QSpinBox;
		_relIntensityBox->setMinimum(0);
		_relIntensityBox->setMaximum(100);
		if (_currentMode == WindowMode::NewMethod || _currentMode == WindowMode::EditMethod)
			_relIntensityBox->setValue(100);
		else
		{
			auto relIntensityLabel = new QLabel("Reative Intensity:");
			bottomLayout->addWidget(relIntensityLabel, 6, 0, Qt::AlignRight);

			bottomLayout->addWidget(_relIntensityBox, 6, 1);

			if (_currentMode == WindowMode::NewIon)
				_relIntensityBox->setValue(25);
			else if (_currentMode == WindowMode::EditIon && rowToLoad >= 0 && rowToLoad < inputModel->methodIonCount())
				_relIntensityBox->setValue(inputModel->getRawMethodIon(rowToLoad)->relIntensity);
		}

		//isotope labels
		_isotopesBox = new QTableView;
		_isotopesBox->setItemDelegate(new LabelBoxDelegate);
		_isotopesBox->verticalHeader()->hide();
		_isotopesBox->horizontalHeader()->setStretchLastSection(true);
		_isotopesBox->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
		QStandardItemModel* model = new QStandardItemModel;
		model->setHorizontalHeaderItem(0, new QStandardItem("Element"));
		model->setHorizontalHeaderItem(1, new QStandardItem("Isotope Mass"));
		model->setHorizontalHeaderItem(2, new QStandardItem("Max Replacements"));
		if (_currentMode != WindowMode::EditMethod)
		{
			bottomLayout->addItem(new QSpacerItem(10, 25), 7, 0);

			auto isotopesLabel = new QLabel("Isotope Labels:");
			bottomLayout->addWidget(isotopesLabel, 8, 0, Qt::AlignLeft);

			bool cUsed = false;
			bool nUsed = false;
			bool hUsed = false;

			//load Labels
			if (inputModel != NULL && rowToLoad >= 0 && rowToLoad < inputModel->methodIonCount())
			{
				auto rawIon = inputModel->getRawMethodIon(rowToLoad);
				QRegularExpression numberRx("[0-9]", QRegularExpression::CaseInsensitiveOption);
				for (auto label = rawIon->ionLabels.begin(); label != rawIon->ionLabels.end(); label++)
				{
					//this part is probably not needed now. Consider removing
					auto newFormula = label->Formula.FormulaString();
					for (int x = newFormula.length() - 1; x >= 0; x--)
					{
						if (newFormula[x] == '1' && x > 0)
						{
							auto before = newFormula.toStdString()[x - 1];
							if (isdigit(before))
								break;
							if (x < newFormula.length() - 1)
							{
								auto after = newFormula.toStdString()[x +1];
								if (isdigit(after))
									break;
							}
							newFormula.remove(x, 1);
						}
					}
					
					auto dumbRequiredExtraPrecisionNumber = ((int)label->ionReplacementMass)/10;
					model->appendRow({ new QStandardItem(newFormula) ,
					new QStandardItem(QString::number(label->ionReplacementMass,
										'g',7 + dumbRequiredExtraPrecisionNumber)),
					new QStandardItem(QString::number(label->maxNumReplacement)) });
					if (newFormula == "C")
						cUsed = true;
					else if (newFormula == "N")
						nUsed = true;
					else if (newFormula == "H")
						hUsed = true;
				}
			}

			//default labels
			if (!cUsed)
			{
				model->appendRow({ new QStandardItem("C") ,
					new QStandardItem("13.003355"),
					new QStandardItem("0") });
			}
			if (!nUsed)
			{
				model->appendRow({ new QStandardItem("N") ,
					new QStandardItem("15.000109"),
					new QStandardItem("0") });
			}
			if (!hUsed)
			{
				model->appendRow({ new QStandardItem("H") ,
					new QStandardItem("2.014102"),
					new QStandardItem("0") });
			}

			_isotopesBox->setModel(model);
			bottomLayout->addWidget(_isotopesBox, 9, 0, 1, 3);
		}		
		
		auto _okButton = new QPushButton("OK");
		connect(_okButton, &QPushButton::clicked, this, &NewMethodWindow::TryAccept);
		main_layout->addWidget(_okButton, 0, Qt::AlignRight);
		
	}

	NewMethodWindow::~NewMethodWindow()
	{

	}

	void NewMethodWindow::SetUsedMethodNames(QList<QString> usedNames)
	{
		_invalidMethodNames.clear();
		foreach(auto name, usedNames)
		{
			_invalidMethodNames.insert(name);
		}
	}

	bool NewMethodWindow::CheckValidity(bool alsoEmpty)
	{
		bool isValid = true;
		QPalette okPalette;
		okPalette.setColor(QPalette::Base, Qt::white);
		QPalette warningPalette;
		warningPalette.setColor(QPalette::Base,Qt::yellow);
		QPalette errorPalette;
		errorPalette.setColor(QPalette::Base, Qt::magenta);

		if (_currentMode == WindowMode::NewMethod || _currentMode == WindowMode::EditMethod)
		{
			if (_methodNameBox->text().isEmpty() && alsoEmpty)
			{
				isValid = false;
				_methodNameBox->setPalette(errorPalette);
			}
			else if (_invalidMethodNames.contains(_methodNameBox->text()))
			{
				isValid = false;
				_methodNameBox->setPalette(warningPalette);
			}
			else
				_methodNameBox->setPalette(okPalette);
		}
			
		if (_currentMode != WindowMode::EditMethod)
		{
			if (_ionFormulaBox->text().isEmpty() && alsoEmpty)
			{
				isValid = false;
				_ionFormulaBox->setPalette(errorPalette);
			}
			else
			{
				try
				{
					ChemFormula ef(_ionFormulaBox->text());
					if (ef.TotalMass() < 0)
					{
						_ionFormulaBox->setPalette(errorPalette);
						isValid = false;
					}
					else
						_ionFormulaBox->setPalette(okPalette);
				}
				catch (std::invalid_argument e)
				{
					bool conversionOK;
					_ionFormulaBox->text().toDouble(&conversionOK);

					if (conversionOK)
						_ionFormulaBox->setPalette(okPalette);
					else
					{
						_ionFormulaBox->setPalette(errorPalette);
						isValid = false;
					}					
				}

			}
			if (_msLevelBox->value() > 1)
			{
				bool conversionOK;
				_precursorBox->text().toDouble(&conversionOK);

				if (conversionOK)
					_precursorBox->setPalette(okPalette);
				else
				{
					_precursorBox->setPalette(errorPalette);
					isValid = false;
				}
			}
		}

		

		if (isValid && alsoEmpty)
		{
			//load
			QList<MassSpecMethodModel> newModelList;
			_loadedModels = newModelList;

			MassSpecMethodModel* newModel;
			if (_currentMode != WindowMode::NewMethod)
				newModel = new MassSpecMethodModel(*_oldModel);
			else
				newModel = new MassSpecMethodModel();
			if (_currentMode == WindowMode::NewMethod || _currentMode == WindowMode::EditMethod)
			{
				newModel->methodName = _methodNameBox->text();
				if (_methodNameBox->text() == "None")
					newModel->methodName = "None (method)";
				newModel->expectedRT = _expetedRTBox->value();
			}				

			if (_currentMode != WindowMode::EditMethod)
			{
				newModel->clear();
				

				
				int msLevel = _msLevelBox->value();
				double precurorMZ = _precursorBox->text().toDouble();
				QString newFormula = _ionFormulaBox->text();
				double expectedRT = _expetedRTBox->value();
				MassSpecMethodIon newMSMI(newFormula,msLevel,precurorMZ, expectedRT);

				//if (_currentMode == WindowMode::NewIon || _currentMode == WindowMode::EditIon)
					newMSMI.relIntensity = _relIntensityBox->value();

				for (int row = 0; row < _isotopesBox->model()->rowCount(); row++)
				{
					int maxNum = _isotopesBox->model()->data(_isotopesBox->model()->index(row, 2)).toInt();
					if (maxNum > 0)
					{
						QString element = _isotopesBox->model()->data(_isotopesBox->model()->index(row, 0)).toString();
						double newMass = _isotopesBox->model()->data(_isotopesBox->model()->index(row, 1)).toDouble();
						IonLabel newLabel(element, newMass, maxNum);
						newMSMI.ionLabels.append(newLabel);
					}
				}
				newModel->append(newMSMI);
			}
			_loadedModels.append(*newModel);
		}

		return isValid;
	}

	void NewMethodWindow::TryAccept()
	{
		if (CheckValidity(true))
			this->accept();
	}

	void NewMethodWindow::LoadFromFile()
	{
		QFileDialog dialog(this);
		dialog.setFileMode(QFileDialog::FileMode::ExistingFile);
		dialog.setNameFilter(tr("MatLab Method (*.m)"));
		dialog.setViewMode(QFileDialog::Detail);
		if (!dialog.exec() || dialog.selectedFiles().count()<1)
			return;
		_loadedModels = MethodFileHandler::GetModelsFromFile(dialog.selectedFiles()[0]);
		this->accept();
	}

	LabelBoxDelegate::LabelBoxDelegate(QObject *parent)
		: QItemDelegate(parent)
	{
	}

	QWidget *LabelBoxDelegate::createEditor(QWidget *parent,
		const QStyleOptionViewItem &/* option */,
		const QModelIndex & index ) const
	{
		if (index.column() == 0)
		{
			QLineEdit *editor = new QLineEdit(parent);
			return editor;
		}
		if (index.column() == 1)
		{
			QDoubleSpinBox *editor = new QDoubleSpinBox(parent);
			editor->setDecimals(6);
			editor->setMinimum(0);
			editor->setMaximum(1000);

			return editor;
		}
		if (index.column() == 2)
		{
			QSpinBox *editor = new QSpinBox(parent);
			editor->setMinimum(0);
			editor->setMaximum(100);

			return editor;
		}
	}

	void LabelBoxDelegate::setEditorData(QWidget *editor,
		const QModelIndex &index) const
	{
		if (index.column() == 0)
		{
			QString value = index.model()->data(index, Qt::EditRole).toString();

			QLineEdit *editBox = static_cast<QLineEdit*>(editor);
			editBox->setText(value);
		}
		else if (index.column() == 1)
		{
			double value = index.model()->data(index, Qt::EditRole).toDouble();

			QDoubleSpinBox *spinBox = static_cast<QDoubleSpinBox*>(editor);
			spinBox->setValue(value);
		}
		else if (index.column() == 2)
		{
			int value = index.model()->data(index, Qt::EditRole).toInt();

			QSpinBox *spinBox = static_cast<QSpinBox*>(editor);
			spinBox->setValue(value);
		}
	}

	void LabelBoxDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
		const QModelIndex &index) const
	{
		if (index.column() == 0)
		{
			QLineEdit *editBox = static_cast<QLineEdit*>(editor);
			QString value = editBox->text();
			bool isValid = true;

			try
			{
				ChemFormula ef(value);
				if (ef.TotalMass() < 0)
					isValid = false;
			}
			catch (std::invalid_argument parseError)
			{
				isValid = false;
			}

			if (isValid)
				model->setData(index, value, Qt::EditRole);
		}
		else if (index.column() == 1)
		{
			QDoubleSpinBox *spinBox = static_cast<QDoubleSpinBox*>(editor);
			spinBox->interpretText();
			double value = spinBox->value();

			model->setData(index, value, Qt::EditRole);
		}
		else if (index.column() == 2)
		{
			QSpinBox *spinBox = static_cast<QSpinBox*>(editor);
			spinBox->interpretText();
			int value = spinBox->value();

			model->setData(index, value, Qt::EditRole);
		}
	}

	void LabelBoxDelegate::updateEditorGeometry(QWidget *editor,
		const QStyleOptionViewItem &option, const QModelIndex &/* index */) const
	{
		editor->setGeometry(option.rect);
	}

}