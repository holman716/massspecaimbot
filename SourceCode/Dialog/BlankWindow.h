#ifndef BLANKWINDOW_H
#define BLANKWINDOW_H

#include <QMainWindow>
#include <QTextEdit>
#include <QPushButton>

namespace MSAB
{

	//namespace Ui {
	//	class BlankWindow;
	//}

	class BlankWindow : public QMainWindow
	{
		Q_OBJECT

	public:
		explicit BlankWindow(QWidget *parent = nullptr);
		~BlankWindow();

	protected slots:
		void test();

	protected:
		//QTextEdit* _textBox;
		//QPushButton* _startButton;
		//Spectrum1DWidget* _svw;
	};
}

#endif // MAINWINDOW_H