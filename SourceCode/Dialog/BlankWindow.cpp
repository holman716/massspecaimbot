#include "BlankWindow.h"
#include <Utility/SpectraFileHandler.h>
#include <QtGUI>
#include <QWidget>
#include <Dialog/TICWidget.h>

#include <Utility/DataContainer/MethodTableModel.h>
#include <Utility/DataContainer/Chemestry.h>
#include <Utility/BinaryHandler.h>

namespace MSAB {
	
	BlankWindow::BlankWindow(QWidget *parent) :
		QMainWindow(parent)
	{
		//main window portion
		setWindowTitle("TestWindow");
		resize(600, 60);
		
		//central layout 
		QWidget* dummy_cw = new QWidget(this);
		setCentralWidget(dummy_cw);
		auto main_layout = new QGridLayout(dummy_cw);
	
		QFile inFile("D:\\YoungProject\\ExampleData\\TestInput.txt");
		if (!inFile.open(QIODevice::ReadOnly | QIODevice::Text))
			return;

		QTextStream reader(&inFile);
		QString line1 = reader.readLine();
		QString line2 = reader.readLine();
		QString line3 = reader.readLine();

		
		QList<double> ret = BinaryHandler::StringToDoubleArray(line3, line1.toInt()*2,true,true);
		QList<double> ret2;

		for (int x = 0; x < ret.count() - 1; x += 2)
		{
			if (ret[x] > 1 && ret[x] < 3000 && ret[x + 1]>1)
			{
				double mz = ret[x];
				double intensity = ret[x+1];
				ret2.push_back(ret[x]);
				ret2.push_back(ret[x+1]);
			}
		}
			

		auto q = QMessageBox::question(this, "title", "text", QDialogButtonBox::Ok);

	}

	BlankWindow::~BlankWindow()
	{

	}

	void BlankWindow::test()
	{
		//QString newText = QString::fromStdString(FileHandler::GetNetCDFText()); //GetNetCDFText("scan_acquisition_time"));
		//_textBox->setText(newText);
	}
}