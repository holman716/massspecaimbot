#include <Dialog/MethodWidget.h>
#include <Utility/MethodFileHandler.h>
#include <QDoubleSpinBox>

namespace MSAB
{
	MethodWidget::MethodWidget(QWidget * parent) : QWidget(parent)
	{
		auto dummyLayout = new QHBoxLayout(this);
		_mainLayout = new QSplitter;
		dummyLayout->addWidget(_mainLayout);

		auto leftPanel = new QFrame;
		_leftLayout = new QGridLayout(leftPanel);
		_leftLayout->setMargin(25);
		_leftLayout->setContentsMargins(5, 5, 5, 5);
		_leftLayout->setVerticalSpacing(0);

		_methodLabel = new QLabel("Method: ");
		_methodBox = new QComboBox;
		_methodBox->addItem("None", "None");
		_methodBox->setCurrentIndex(_methodBox->findData("None"));
		_methodBox->setMinimumSize(100, 10);
		connect(_methodBox, &QComboBox::currentTextChanged, this, &MethodWidget::CurrentMethodUpdated);
		_newMethodButton = new QPushButton("Add");
		connect(_newMethodButton, &QPushButton::clicked, this, &MethodWidget::addMethods);
		_editMethodButton = new QPushButton("Edit");
		connect(_editMethodButton, &QPushButton::clicked, this, &MethodWidget::editMethod);
		auto spacer = new QSpacerItem(10,25);
		_toleranceLabel = new QLabel("Tolerance:");
		_toleranceBox = new QDoubleSpinBox;
		_toleranceBox->setValue(0.1);
		_lastSeenTolerance = 0.1;
		_currentSetTolerance = 0.1;
		_toleranceBox->setDecimals(4);
		_toleranceBox->setMinimum(0.0001);		
		_toleranceBox->setMaximum(1.5);
		_toleranceBox->setSingleStep(0.0001);
		connect(_toleranceBox, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
			this, &MethodWidget::ToleranceChanged);
		_toleranceButton = new QPushButton("Update");
		connect(_toleranceButton, &QPushButton::clicked, this,
			[=]() {
					_currentSetTolerance = _toleranceBox->value();
					ToleranceChanged();
					emit IndicateNewTolerance(_toleranceBox->value()); 
				  });

		_leftLayout->addWidget(_methodLabel,0,0, Qt::AlignRight);
		_leftLayout->addWidget(_methodBox, 0, 1,1,2);
		_leftLayout->addWidget(_newMethodButton, 1, 1);
		_leftLayout->addWidget(_editMethodButton, 1, 2);
		_leftLayout->addItem(spacer, 2, 2);
		_leftLayout->addWidget(_toleranceLabel, 3, 0, Qt::AlignRight);
		_leftLayout->addWidget(_toleranceBox, 3, 1);
		_leftLayout->addWidget(_toleranceButton, 3, 2);

		_mainLayout->addWidget(leftPanel);

		auto rightPanel = new QFrame;
		_rightLayout = new QGridLayout(rightPanel);
		_rightLayout->setMargin(0);
		_rightLayout->setVerticalSpacing(0);

		_newIonButton = new QPushButton("New");
		connect(_newIonButton, &QPushButton::clicked, this, &MethodWidget::addIon);
		_editIonButton = new QPushButton("Edit");
		connect(_editIonButton, &QPushButton::clicked, this, &MethodWidget::editIon);
		_rightLayout->addWidget(_newIonButton, 0, 0, Qt::AlignLeft);
		_rightLayout->addWidget(_editIonButton, 0, 1, Qt::AlignLeft);
		_rightLayout->setColumnStretch(5, 1);
		_rightLayout->setColumnMinimumWidth(0, 75);
		_rightLayout->setColumnMinimumWidth(2, 100);

		//method table
		_currentMethodModel = new MassSpecMethodModel;
		_methodQTV = new QTableView;
		_methodQTV->setModel(_currentMethodModel);
		_methodQTV->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);

		_rightLayout->addWidget(_methodQTV, 1, 0, 1, 6);
		_mainLayout->addWidget(rightPanel);
		_mainLayout->setCollapsible(0, false);
		_mainLayout->setCollapsible(1, false);
		_mainLayout->setStretchFactor(0, 1);
		_mainLayout->setStretchFactor(1, 4);
	}
	MethodWidget::~MethodWidget()
	{
	}

	
	void MSAB::MethodWidget::addMethods()
	{
		auto nmw = new NewMethodWindow(NewMethodWindow::WindowMode::NewMethod);
		auto result = nmw->exec();
		if (result != QDialog::Accepted)
			return;
		//QMessageBox::question(this, "Title", QString::number(result), QMessageBox::Yes);
		auto loadedMethods = nmw->GetModels();
		//vector<MassSpecMethodModel> tempMethods = MethodFileHandler::GetModelsFromFile("");
		bool anyAdded = false;
		foreach (auto currentMethod,loadedMethods)
		{
			QString label = currentMethod.methodName;
			bool alreadyUsed = false;
			if (_methodMap.find(label) != _methodMap.end())
			{
				_methodMap[label]->deleteLater();
				alreadyUsed = true;
			}
			_methodMap[label] = new MassSpecMethodModel(currentMethod);
			//_methodQTV->setModel(_methodMap[label]);
			connect(_methodMap[label], &MassSpecMethodModel::dataChanged, this, [=](QModelIndex const &topLeft, QModelIndex const &bottomRight)
			{
				//for (int x = 0; x < _currentMethodModel->methodIonCount(); x++) //(int x = topLeft.row(); x <= bottomRight.row(); x++)
				//	emit ModelChanged(*_currentMethodModel->getRawMethodIon(x));
				_methodQTV->resizeColumnsToContents();
				//emit CallTableRefresh();
				CurrentMethodUpdated();
			});

			if (!alreadyUsed)
			{
				_methodBox->addItem(label);
				if (_methodBox->count() == 2)
					_methodBox->setCurrentIndex(1);
			}
				

			for (int x = 0; x < _methodMap[label]->methodIonCount(); x++)
			{
				//emit ModelChanged(*_methodMap[label]->getRawMethodIon(x));
				anyAdded = true;
			}
		}
		if (anyAdded)
			CurrentMethodUpdated();//emit CallTableRefresh();
	}

	void MSAB::MethodWidget::editMethod()
	{
		if (_methodBox->currentText() == "None" || !_methodMap.contains(_methodBox->currentText()))
			return;
		auto nmw = new NewMethodWindow(NewMethodWindow::WindowMode::EditMethod, _methodMap[_methodBox->currentText()], 0);
		auto result = nmw->exec();
		if (result != QDialog::Accepted)
			return;

		auto oldMethod = _methodMap[_methodBox->currentText()];
		auto newMethod = nmw->GetSingleModel();
		if (!newMethod)
		{
			_methodBox->removeItem(_methodBox->currentIndex());
			_methodBox->setCurrentIndex(0);
			_methodMap.remove(oldMethod->methodName);
			delete oldMethod;
			return;			
		}
		_methodMap.remove(oldMethod->methodName);
		delete oldMethod;
		_methodMap[newMethod->methodName] = newMethod;
		connect(_methodMap[newMethod->methodName], &MassSpecMethodModel::dataChanged, this, [=](QModelIndex const &topLeft, QModelIndex const &bottomRight)
		{
			_methodQTV->resizeColumnsToContents();
			CurrentMethodUpdated();
		});
		_methodBox->setItemText(_methodBox->currentIndex(), newMethod->methodName);		
		//MethodChanged();
	
		CurrentMethodUpdated();//emit CallTableRefresh();
		//QMessageBox::question(this, "Title", _methodMap[_methodBox->currentText()]->methodName, QMessageBox::Yes);
	}

	void MSAB::MethodWidget::addIon()
	{
		if (_methodBox->currentText() == "None" || !_methodMap.contains(_methodBox->currentText()))
			return;
		auto nmw = new NewMethodWindow(NewMethodWindow::WindowMode::NewIon,
			_methodMap[_methodBox->currentText()], _methodMap[_methodBox->currentText()]->baseMSMIRow);
		auto result = nmw->exec();
		if (result != QDialog::Accepted)
			return;

		auto newMethod = nmw->GetSingleModel();
		if (!newMethod )		
			return;
		_methodMap[newMethod->methodName]->append(*newMethod->getRawMethodIon(0));
		CurrentMethodUpdated();
	}

	void MSAB::MethodWidget::editIon()
	{
		if (_methodBox->currentText() == "None" || !_methodMap.contains(_methodBox->currentText())
			|| _methodMap[_methodBox->currentText()]->methodIonCount()<2)
			return;

		QItemSelectionModel *selectModel = _methodQTV->selectionModel();
		QSet<int> selectedRows;
		if (selectModel->hasSelection())
		{
			foreach(auto row, selectModel->selectedRows())
				selectedRows.insert(row.row());
			foreach(auto row, selectModel->selectedIndexes())
				selectedRows.insert(row.row());
			if (selectedRows.count() != 1)
				return;
		}
		else
			return;
		int selectedRow = *selectedRows.begin();

		auto nmw = new NewMethodWindow(NewMethodWindow::WindowMode::EditIon,
			_methodMap[_methodBox->currentText()], selectedRow);
		auto result = nmw->exec();
		if (result != QDialog::Accepted)
			return;

		auto newMethod = nmw->GetSingleModel();
		_methodMap[_methodBox->currentText()]->remove(selectedRow);
		if (!newMethod)
		{
			CurrentMethodUpdated();
			return;
		}
		_methodMap[_methodBox->currentText()]->append(*newMethod->getRawMethodIon(0), selectedRow);
	}

	void MethodWidget::ToleranceChanged()
	{
		double current = _toleranceBox->value();
		double diff = current - _lastSeenTolerance;
		if (double_equals(diff , -0.0001)) //going down
		{
			QList<double> brueForceStepMagicList{1.5,1,0.5,0.1,0.05,0.01,0.005,0.001,0.0005,0.0001 };
			foreach(auto magicNumber, brueForceStepMagicList)
			{
				if (magicNumber < current)
				{
					_lastSeenTolerance = magicNumber;
					_toleranceBox->setValue(magicNumber);
					return;
				}
			}
		}
		else if (double_equals(diff, 0.0001)) //going up
		{
			QList<double> brueForceStepMagicList{ 0.0001, 0.0005,0.001,0.005,0.01,0.05,0.1,0.5,1,1.5 };
			foreach(auto magicNumber, brueForceStepMagicList)
			{
				if (magicNumber > current)
				{
					_lastSeenTolerance = magicNumber;
					_toleranceBox->setValue(magicNumber);
					return;
				}
			}
		}
		else
			_lastSeenTolerance = current;

		if (double_equals(_currentSetTolerance, current))
		{
			QString qss = QString("background-color: #ece9d8");
			_toleranceButton->setStyleSheet(qss);
		}
		else
		{
			QString qss = QString("background-color: #58f549");
			_toleranceButton->setStyleSheet(qss);
		}
	}

	void MethodWidget::CurrentMethodUpdated()
	{
		MassSpecMethodModel* tempModel;
		if (_methodMap.find(_methodBox->currentText()) == _methodMap.end())
			tempModel = NULL;
		else
			tempModel = _methodMap[_methodBox->currentText()];
		_methodQTV->setModel(tempModel == NULL ? new MassSpecMethodModel : tempModel);
		emit ModelChanged(tempModel);
	}
}
