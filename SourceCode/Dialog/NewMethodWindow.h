#ifndef NEWMETHODWINDOW_H
#define NEWMETHODWINDOW_H

#include <QDialog>
#include <QLineEdit>
#include <QPushButton>
#include <QGridLayout>
#include <QLabel>
#include <QSpinBox>
#include <QTableView>
#include <QHeaderView>
#include <QValidator>
#include <Utility/DataContainer/MethodTableModel.h>

namespace MSAB
{

	//namespace Ui {
	//	class BlankWindow;
	//}

	class NewMethodWindow : public QDialog
	{
		Q_OBJECT

	public:
		enum WindowMode { NewMethod, EditMethod, NewIon, EditIon };
		explicit NewMethodWindow(WindowMode, MassSpecMethodModel* inputModel = NULL, int rowToLoad = -1, QWidget *parent = nullptr);
		~NewMethodWindow();
		void SetUsedMethodNames(QList<QString> usedNames);
		QList<MassSpecMethodModel> GetModels() { return _loadedModels; } //newMethod
		MassSpecMethodModel* GetSingleModel() { return _loadedModels.isEmpty() ? nullptr : &_loadedModels.first(); } //edit Method & new/edit ion
		void keyPressEvent(QKeyEvent *evt) override
		{
			if (evt->key() == Qt::Key_Enter || evt->key() == Qt::Key_Return)
				return;
			QDialog::keyPressEvent(evt);
		}

	protected slots:
		void TryAccept();
		void LoadFromFile();

	protected:
		QLineEdit* _methodNameBox;
		QSpinBox* _expetedRTBox;
		QSpinBox* _msLevelBox;
		QLabel* _precursorLabel;
		QLineEdit* _precursorBox;
		QLineEdit* _ionFormulaBox;
		QSpinBox* _relIntensityBox;
		QTableView* _isotopesBox;
		WindowMode _currentMode;

		MassSpecMethodModel* _oldModel;
		//QString _oldFormula;
		QSet<QString> _invalidMethodNames;
		QList<MassSpecMethodModel> _loadedModels;

		bool CheckValidity(bool alsoEmpty = false);

	private:
		int _lastMSLevel;

	};

	class LabelBoxDelegate : public QItemDelegate
	{
		Q_OBJECT

	public:
		LabelBoxDelegate(QObject *parent = 0);

		QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
			const QModelIndex &index) const;

		void setEditorData(QWidget *editor, const QModelIndex &index) const;
		void setModelData(QWidget *editor, QAbstractItemModel *model,
			const QModelIndex &index) const;

		void updateEditorGeometry(QWidget *editor,
			const QStyleOptionViewItem &option, const QModelIndex &index) const;
	};
}

#endif // NEWMETHODWINDOW_H