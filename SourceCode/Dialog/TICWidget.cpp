#include <Dialog/TICWidget.h>
#include <QLineSeries>

#include <QLogValueAxis>
#include <QLayout>
//#include <OpenMS/VISUAL/Spectrum1DWidget.h>
#include <Utility/DataContainer/MethodTableModel.h>

using namespace QtCharts;
namespace MSAB
{
	TICSpectrumWidget::TICSpectrumWidget(QWidget * parent, QString xTitle, QString yTitle) : QWidget(parent)
	{
		_currentModel = NULL;
		_showTIC = true;
		_tolerance = 0.1;

		_mainLayout = new QGridLayout(this);		
		_mainLayout->setMargin(0);
		_mainLayout->setVerticalSpacing(0);

		//button area
		auto tmpPanel = new QFrame;
		auto tmpLayout = new QHBoxLayout(tmpPanel);
		tmpLayout->setMargin(5);
		tmpLayout->addStretch(0);
		
		_precursorLabel = new QLabel("Precursor");
		_precursorLabel->setVisible(false);
		tmpLayout->addWidget(_precursorLabel);
		_actionMenu = new QMenu;
		_actionButton = new QPushButton;
		_actionButton->setText("Hide TIC");
		_actionButton->setEnabled(false);
		connect(_actionButton, &QPushButton::clicked, this, [=]() 
		{
			_showTIC = !_showTIC;
			_actionButton->setText(_showTIC?"Hide TIC":"Show TIC");
			RefreshAllSeries();
		});
		//_actionButton->setMenu(_actionMenu);
		tmpLayout->addWidget(_actionButton);
		
		_exportMenu = new QMenu;
		_exportButton = new QPushButton;
		_exportButton->setText("Export");
		_exportButton->setEnabled(false);
		auto compositeViewOption = new QAction("...composite of current view");
		_exportMenu->addAction(compositeViewOption);
		connect(compositeViewOption, &QAction::triggered, this, &TICSpectrumWidget::ShowCompositeSpectra);
		_exportButton->setMenu(_exportMenu);
		tmpLayout->addWidget(_exportButton);
		
		_mainLayout->addWidget(tmpPanel);

		//chart area
		auto mainChart = new QChart;
		auto axisX = new QValueAxis();
		axisX->setTitleText(xTitle);
		axisX->setLabelFormat("%i");
		//_axisX->setTickType(QValueAxis::TickType::TicksDynamic);
		mainChart->addAxis(axisX, Qt::AlignBottom);
		auto axisY = new QValueAxis();
		axisY->setTitleText(yTitle);
		axisY->setLabelFormat("%.0f");
		mainChart->addAxis(axisY, Qt::AlignLeft);
		mainChart->legend()->setAlignment(Qt::AlignRight);
		_chartView = new ChartView(mainChart);
		_chartView->setRubberBand(QChartView::HorizontalRubberBand);
		_chartView->scrollBarWidgets(Qt::AlignBottom);
		connect(_chartView, &ChartView::ScanTimeSelected, this, &TICSpectrumWidget::ForwardSelectedSpectra);
		//connect(_chartView, &ChartView::ViewChanged, this, &TICSpectrumWidget::ViewChanged);
		_mainLayout->addWidget(_chartView);
		_dataSource = new MSABExperiment;
	}
	TICSpectrumWidget::~TICSpectrumWidget()
	{
	}
	
	QString TICSpectrumWidget::SetSpectra(int scanNumber, int targetLevel)
	{
		_chartView->ResetZoom();
		_chartView->_xAxis->setTitleText("MZ");
		_chartView->_mainChart->removeAllSeries();
		_actionButton->setMenu(NULL);
		_actionButton->setEnabled(false);
		_actionMenu->clear();
		_actionButton->setText("MsLevel");
		_actionButton->disconnect();
		_exportMenu->setEnabled(false);		
			
		if (scanNumber  < 0 || _dataSource->GetSpectraCount() < scanNumber)
			return "Scan Not Found";
		_currentScanId = scanNumber;
		MSABSpectrum scan = _dataSource->GetSpectrumAtIndex(scanNumber);
		MSABSpectrum* selectedScan = NULL;
		qreal minX = _dataSource->getMinMZ(); //std::numeric_limits<double>::max();
		qreal maxX = _dataSource->getMaxMZ();
		qreal maxY = 0;

		//handle levels here
		QList<int> availableMsLevels;
		MSABSpectrum* scanIterator = &scan;
		while (true)
		{
			auto currentLevel = scanIterator->GetLevel();
			availableMsLevels.push_back(currentLevel);
			if (currentLevel == targetLevel)
				selectedScan = scanIterator;
			if (scanIterator->nextLevelDown != NULL)
				scanIterator = scanIterator->nextLevelDown;
			else
				break;	
		}
		if (availableMsLevels.count() > 1)
		{
			for (int i : availableMsLevels)
			{
				auto newAction = _actionMenu->addAction(QString::number(i));
				connect(newAction, &QAction::triggered, this, [=]() {SetSpectra(_currentScanId, i); });
			}
			_actionButton->setMenu(_actionMenu);
			_actionButton->setEnabled(true);
		}
			
		if (selectedScan == NULL)
		{
			targetLevel = scan.GetLevel();
			selectedScan = &scan;
		}
		_precursorLabel->setText("Precursor M/Z: " + QString::number(selectedScan->GetPrecursorMZ()));
		_precursorLabel->setVisible(targetLevel > 1);
		_actionButton->setText("MsLevel: " + QString::number(targetLevel));
		

		QLineSeries* series = new QLineSeries();
		series->setName(selectedScan->GetName());
		for (const MSABPeak peak : selectedScan->GetPeaks())
		{
			series->append(peak.getMZ()-0.000000001, 0);
			series->append(peak.getMZ(), peak.getIntensity());
			series->append(peak.getMZ() + 0.000000001, 0);
			if (peak.getIntensity() > maxY)
				maxY = peak.getIntensity();
		}
		series->append(floor(minX * 0.99), 0);
		series->append(ceil(maxX * 1.01), 0);
		_chartView->_mainChart->addSeries(series);
		series->attachAxis(_chartView->_mainChart->axisX());
		series->attachAxis(_chartView->_mainChart->axisY());
		_chartView->_mainChart->legend()->setVisible(false);

		_chartView->MaxStoredYValue = maxY;
		_chartView->RescaleAxis(maxX*1.01, minX* 0.99, maxY*1.1);

		return QString("Scan: %1 minutes").arg(QString::number(selectedScan->GetRT() / 60));

	}
	void TICSpectrumWidget::RefreshData(MSABExperiment* newData, bool show)
	{
		//_dataSource->reset();
		_dataSource = newData;
		_chartView->_yAxis->setTitleText("Intensity");
		
		if (show || !newData)
			RefreshTICChart();
		if (!show)
			SetSpectra(-1, 1);
	}

	void TICSpectrumWidget::RefreshTICChart()
	{
		_chartView->ResetZoom();
		_chartView->_mainChart->removeAllSeries();
		if (!_dataSource || _dataSource->GetSpectraCount() == 0)
			return;

		bool onlyTIC = true;
		if (_currentModel != NULL)
			for (int x = 0; x < _currentModel->methodIonCount(); x++)
			{
				auto ion = _currentModel->getRawMethodIon(x);
				if (ion->showIonTrace)
				{
					onlyTIC = false;
					break;
				}
			}
		if (onlyTIC)
		{
			_showTIC = true;
			_actionButton->setText("Hide TIC");
			_actionButton->setEnabled(false);
		}
		else		
			_actionButton->setEnabled(true);
		_exportButton->setEnabled(true);
		
		
		_chartView->_xAxis->setTitleText("RT");
			
		qreal minX = _dataSource->getMinRT()/60; //std::numeric_limits<double>::max();
		qreal maxX = _dataSource->getMaxRT()/60;
		qreal maxY = 0;
			
		if (_showTIC)
		{
			QLineSeries *ticSeries = new QLineSeries();
			ticSeries->setName("TIC");
			{
				auto spectra = _dataSource->GetSpectraList();
				for (MSABSpectrum spectrum : spectra)
				{
					auto rtInMins = spectrum.GetRT() / 60;
					ticSeries->append(rtInMins, spectrum.GetTIC());
					if (spectrum.GetTIC() > maxY)
						maxY = spectrum.GetTIC();
				}
			}

			_chartView->_mainChart->addSeries(ticSeries);
			ticSeries->attachAxis(_chartView->_mainChart->axisX());
			ticSeries->attachAxis(_chartView->_mainChart->axisY());
		}		
		
		_chartView->MaxStoredYValue = maxY;
		_chartView->RescaleAxis(maxX*1.01, minX*0.99, maxY*1.1);

		if (_currentModel != NULL)
		{
			for (int x = 0; x < _currentModel->methodIonCount(); x++)
			{
				auto ion = _currentModel->getRawMethodIon(x);

				if (!ion->showIonTrace)
					continue;

				QLineSeries *baseSeries = GetSpectraSeries(ion->ionMass - _tolerance, ion->ionMass + _tolerance, ion->Name,ion->msLevel,ion->precursorMZ-_tolerance, ion->precursorMZ + _tolerance);
				_chartView->_mainChart->addSeries(baseSeries);
				baseSeries->attachAxis(_chartView->_mainChart->axisX());
				baseSeries->attachAxis(_chartView->_mainChart->axisY());

				if (_currentModel->singleShown())
				{
					auto finalComboList = ion->GetAllIsotopomers();
					QMap<QString, QLineSeries*> returnMap;
					while (finalComboList.count() > 0)
					{
						QString newName = finalComboList.begin().key();
						double newMass = finalComboList[newName];
						finalComboList.remove(newName);

						double currentMin = newMass - _tolerance;
						double currentMax = newMass + _tolerance;

						QList<QString> merged;
						for (auto otherCombo = finalComboList.begin(); otherCombo != finalComboList.end(); otherCombo++)
						{
							if (otherCombo.value() < newMass && otherCombo.value() + _tolerance > currentMin)
							{
								newName += " | ";
								newName += otherCombo.key();
								currentMin = otherCombo.value() - _tolerance;
								merged.append(otherCombo.key());
							}
							else if (otherCombo.value() > newMass && otherCombo.value() - _tolerance < currentMax)
							{
								newName += " | ";
								newName += otherCombo.key();
								currentMax = otherCombo.value() + _tolerance;
								merged.append(otherCombo.key());
							}
						}
						foreach(auto removed, merged)
							finalComboList.remove(removed);

						QLineSeries *ionSeries = GetSpectraSeries(currentMin, currentMax, "[Iso]" + newName);
						returnMap[newName] = ionSeries;
					}

					for (auto it = returnMap.begin(); it != returnMap.end(); it++)
					{
						_chartView->_mainChart->addSeries(it.value());
						it.value()->attachAxis(_chartView->_mainChart->axisX());
						it.value()->attachAxis(_chartView->_mainChart->axisY());
					}
				}
			}
		}		
	}

	void MSAB::TICSpectrumWidget::RefreshAllSeries()
	{
		RefreshTICChart();
	}

	void MSAB::TICSpectrumWidget::SetCurrentModel(MSAB::MassSpecMethodModel* newModel)
	{
		_currentModel = newModel;
		RefreshTICChart();
	}

	void MSAB::TICSpectrumWidget::SetInstrumentTolerance(double tol)
	{
		_tolerance = tol;
		RefreshTICChart();
	}

	void MSAB::TICSpectrumWidget::ShowCompositeSpectra()
	{
		if (_dataSource == NULL)
			return;

		auto resultWindow = new QWidget;
		auto resultLayout = new QVBoxLayout(resultWindow);

		//title section
		auto infoLabel = new QLabel;
		infoLabel->setText("Showing composite spectra from "
			+ QString::number(_chartView->MinShownXValue, 'f', 4) + " minutes to "
			+ QString::number(_chartView->MaxShownXValue, 'f', 4) + " minutes with a bin size of "
			+ QString::number(_tolerance));
		resultLayout->addWidget(infoLabel);

		//Chart section
		auto testChart = new QChart;
		auto testAxisX = new QValueAxis();
		testAxisX->setTitleText("MZ");
		testAxisX->setLabelFormat("%i");
		//_axisX->setTickType(QValueAxis::TickType::TicksDynamic);
		testChart->addAxis(testAxisX, Qt::AlignBottom);
		auto testAxisY = new QValueAxis();
		testAxisY->setTitleText("Intensity");
		testAxisY->setLabelFormat("%i");
		testChart->addAxis(testAxisY, Qt::AlignLeft);
		testChart->legend()->setAlignment(Qt::AlignRight);
		auto testChartView = new ChartView(testChart);
		testChartView->setRubberBand(QChartView::HorizontalRubberBand);
		testChartView->scrollBarWidgets(Qt::AlignBottom);

		QLineSeries* series = new QLineSeries();
		auto maxY = -1;
		auto minX = numeric_limits<double>::max();
		auto maxX = numeric_limits<double>::min();
		auto peakList = GetCompositePeaksInRange(_chartView->MinShownXValue, _chartView->MaxShownXValue);
		for (const MSABPeak peak : peakList)
		{
			if (peak.getMZ() > maxX)
				maxX = peak.getMZ();
			if (peak.getMZ() < minX)
				minX = peak.getMZ();

			series->append(peak.getMZ() - 0.000000001, 0);
			series->append(peak.getMZ(), peak.getIntensity());
			series->append(peak.getMZ() + 0.000000001, 0);
			if (peak.getIntensity() > maxY)
				maxY = peak.getIntensity();
		}
		series->append(floor(minX * 0.99), 0);
		series->append(ceil(maxX * 1.01), 0);
		testChart->addSeries(series);
		series->attachAxis(testChart->axisX());
		series->attachAxis(testChart->axisY());
		testChart->legend()->setVisible(false);

		testChartView->MaxStoredYValue = maxY;
		testChartView->RescaleAxis(maxX*1.01, minX*.99, maxY*1.1);

		resultLayout->addWidget(testChartView);
		
		resultWindow->show();
		resultWindow->resize(600, 600);
		testChartView->ResetZoom();
		//auto q = QMessageBox::question(this, "title", "text", QDialogButtonBox::Ok);
	}

	QLineSeries* MSAB::TICSpectrumWidget::GetSpectraSeries(double lowerMass, double upperMass, QString name, int msLevel, double precursorMassMin, double precursorMassMax)
	{
		QLineSeries *baseSeries = new QLineSeries();
		baseSeries->setName(name);
		{
			auto spectra = _dataSource->GetSpectraList();
			for (MSABSpectrum spectrum: spectra)
			{
				if (spectrum.GetLevel() != msLevel)
					continue;
				if (msLevel > 1 && (spectrum.GetPrecursorMZ() < 0
					|| spectrum.GetPrecursorMZ() < precursorMassMin
					|| spectrum.GetPrecursorMZ() > precursorMassMax))
					continue;

				double rt = spectrum.GetRT()/60;
				double intensity = 0;
				for (MSABPeak peak: spectrum.GetPeaks())
				{
					if (peak.getMZ() < upperMass && peak.getMZ() > lowerMass)
						intensity += peak.getIntensity();
				}
				baseSeries->append(rt, intensity);
			}
		}
		return baseSeries;
	}

	QList<MSABPeak> MSAB::TICSpectrumWidget::GetCompositePeaksInRange(qreal minRT, qreal maxRT)
	{
		QList<double> mzListRaw;
		QList<double> intensityListRaw;

		auto spectra = _dataSource->GetSpectraList();
		for (MSABSpectrum spectrum : spectra)
		{
			double rt = spectrum.GetRT() / 60;
			if (rt<minRT || rt > maxRT)
				continue;
			for (MSABPeak peak : spectrum.GetPeaks())
			{
				mzListRaw.push_back(peak.getMZ());
				intensityListRaw.push_back(peak.getIntensity());
			}
		}

		QList<double> mzList;
		QList<double> intensityList;
		while (!mzListRaw.isEmpty())
		{
			double maxIntensityValue = std::numeric_limits<double>::min();
			int maxIntensityIndex = -1;
			for (int x = 0; x < intensityListRaw.count(); x++)
			{
				if (intensityListRaw[x] > maxIntensityValue)
				{
					maxIntensityValue = intensityListRaw[x];
					maxIntensityIndex = x;
				}
			}

			double centerMZ = mzListRaw[maxIntensityIndex];
			double minMZ = centerMZ - _tolerance;
			double maxMZ = centerMZ + _tolerance;
			double totalIntensity = 0;

			for (int x = mzListRaw.count() - 1; x >= 0; x--)
			{
				if (mzListRaw[x] > minMZ && mzListRaw[x] < maxMZ)
				{
					totalIntensity += intensityListRaw[x];
					mzListRaw.removeAt(x);
					intensityListRaw.removeAt(x);
				}
			}
			if (totalIntensity < DBL_EPSILON)
				continue;
			mzList.push_back(centerMZ);
			intensityList.push_back(totalIntensity);
		}

		QList<MSABPeak> returnList;
		for (int x = 0; x < mzList.count(); x++)
		{			
			MSABPeak newPeak(mzList[x], intensityList[x]);
			returnList.push_back(newPeak);
		}
		return returnList;


		////TODO: switch to centralized peak strategy

		//auto minMZ = _dataSource->getMinMZ();
		//auto maxMZ = _dataSource->getMaxMZ();

		////set up bins
		//QList<QString> keyStringOrder;
		//QList<double> keyDoubleOrder;
		//QMap<QString, double> binIntensityMap;
		//
		//for (double x = 0; x <= maxMZ; x += _tolerance)
		//{
		//	if (x + _tolerance < minMZ)
		//		continue;
		//	auto key = QString::number(x, 'f', 8);
		//	binIntensityMap[key] = 0;
		//	keyStringOrder.push_back(key);
		//	keyDoubleOrder.push_back(x);
		//}
		//if (keyStringOrder.count() == 0)
		//	return QList<MSABPeak>();


		//auto spectra = _dataSource->GetSpectraList();
		//for (MSABSpectrum spectrum : spectra)
		//{
		//	double rt = spectrum.GetRT() / 60;
		//	if (rt<minRT || rt > maxRT)
		//		continue;
		//	for (MSABPeak peak : spectrum.GetPeaks())
		//	{
		//		int peakCount = spectrum.GetPeaks().count();
		//		for (int x = 0; x < keyStringOrder.count(); x++)
		//		{
		//			if (peak.getMZ() > keyDoubleOrder[x])
		//			{
		//				if (x == keyStringOrder.count() - 1 || peak.getMZ() < keyDoubleOrder[x + 1])
		//				{
		//					binIntensityMap[keyStringOrder[x]] += peak.getIntensity();
		//					break;
		//				}						
		//			}
		//		}
		//	}
		//}
		//QList<MSABPeak> returnList;
		//for (int x = 0; x < keyStringOrder.count(); x++)
		//{
		//	if (binIntensityMap[keyStringOrder[x]] < DBL_EPSILON)
		//		continue;
		//	MSABPeak newPeak(keyDoubleOrder[x], binIntensityMap[keyStringOrder[x]]);
		//	returnList.push_back(newPeak);
		//}
		//return returnList;
	}

	void TICSpectrumWidget::ForwardSelectedSpectra(qreal targetScanTime)
	{
		QList<MSABSpectrum> currentSpectra;
		try
		{
			currentSpectra = _dataSource->GetSpectraList();
		}
		catch (std::exception e)
		{
			return;
		}

		//qreal closest = std::numeric_limits<double>::max();
		int scanNum = _dataSource->GetIndexClosestToRT(targetScanTime);

		////this can be improved, but no need to unless we run into lag issues
		//for (int x = 0; x< currentSpectra.size(); x++)
		//{
		//	double time = (currentSpectra[x]).GetRT();
		//	double diff = abs(time - targetScanTime);
		//	if (diff < closest)
		//	{
		//		closest = diff;
		//		scanNum = x;
		//	}
		//}
		if (scanNum >= 0)
			emit SpectraSelected(scanNum);
			//_dataSource->getSpectra()
	}
	
}