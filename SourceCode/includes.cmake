set(MSAB_sources  CACHE INTERNAL "This variable should hold all OpenMS sources at the end of the config step" )
set(MSAB_sources_h  CACHE INTERNAL "This variable should hold all OpenMS sources at the end of the config step" )
set (currentDirectory ${base_source_directory}/SourceCode)

set(basedir_list
${currentDirectory}/main.cpp
)

include(${currentDirectory}/Dialog/sources.cmake)
include(${currentDirectory}/Utility/sources.cmake)

## merge all headers to sources (for source group view in VS)
list(APPEND MSAB_sources ${MSAB_sources_h})


